package cd.semantic;

import java.util.Iterator;
import java.util.List;

import cd.Main;
import cd.exceptions.SemanticFailure;
import cd.exceptions.ToDoException;
import cd.ir.Ast;
import cd.ir.Ast.Assign;
import cd.ir.Ast.BinaryOp;
import cd.ir.Ast.BooleanConst;
import cd.ir.Ast.BuiltInRead;
import cd.ir.Ast.BuiltInReadFloat;
import cd.ir.Ast.BuiltInWrite;
import cd.ir.Ast.BuiltInWriteFloat;
import cd.ir.Ast.BuiltInWriteln;
import cd.ir.Ast.Cast;
import cd.ir.Ast.ClassDecl;
import cd.ir.Ast.Decl;
import cd.ir.Ast.Expr;
import cd.ir.Ast.Field;
import cd.ir.Ast.FloatConst;
import cd.ir.Ast.IfElse;
import cd.ir.Ast.Index;
import cd.ir.Ast.IntConst;
import cd.ir.Ast.MethodCall;
import cd.ir.Ast.MethodCallExpr;
import cd.ir.Ast.MethodDecl;
import cd.ir.Ast.NewArray;
import cd.ir.Ast.NewObject;
import cd.ir.Ast.Nop;
import cd.ir.Ast.NullConst;
import cd.ir.Ast.ReturnStmt;
import cd.ir.Ast.Seq;
import cd.ir.Ast.Stmt;
import cd.ir.Ast.ThisRef;
import cd.ir.Ast.UnaryOp;
import cd.ir.Ast.Var;
import cd.ir.Ast.VarDecl;
import cd.ir.Ast.WhileLoop;
import cd.ir.AstVisitor;
import cd.ir.Symbol;
import cd.ir.Symbol.ArrayTypeSymbol;
import cd.ir.Symbol.ClassSymbol;
import cd.ir.Symbol.MethodSymbol;
import cd.ir.Symbol.PrimitiveTypeSymbol;
import cd.ir.Symbol.TypeSymbol;
import cd.ir.Symbol.VariableSymbol;



public class SemanticAnalyzer {

	
	public final Main main;
	
	private TypeMapper typeMapper;
	
	private SymbolGenerator symbGen;
	
	private InheritanceChecker inChecker;
	
	protected final SemanticVisitor semVisitor = new SemanticVisitor();
	
	protected final ReturnVisitor retVisitor = new ReturnVisitor();
	
	public SemanticAnalyzer(Main main) {
		this.main = main;
	}
	
	public void check(List<ClassDecl> classDecls) throws SemanticFailure {
		// build Symbols
		typeMapper = new TypeMapper(classDecls);
		symbGen = new SymbolGenerator(typeMapper);
		inChecker = new InheritanceChecker();
		//generate Decl Symbols
		symbGen.generateSymbols(classDecls);
		//check Inheritance
		inChecker.checkForInheritance(classDecls);
		
		for (ClassDecl ast : classDecls) {
			semVisitor.visit(ast, null);
		}
	}

	//checks, whether b is subclass of a
	public boolean isSubclass(TypeSymbol a,TypeSymbol b){
		boolean found=false;
		if(a.equals(b)){
			return true;
		}
		else{
			if((a instanceof ArrayTypeSymbol && !(b instanceof ArrayTypeSymbol))||(!(a instanceof ArrayTypeSymbol) && b instanceof ArrayTypeSymbol)){
				return false;
			}
			else if(a instanceof ArrayTypeSymbol && b instanceof ArrayTypeSymbol){
				//strip the array since we know that both are arrays
				a=((ArrayTypeSymbol) a).elementType;
				b=((ArrayTypeSymbol) b).elementType;
			}
			//now a and b could only be PrimitiveTypeSymbol or ClassSymbol, 
			if(a instanceof PrimitiveTypeSymbol || b instanceof PrimitiveTypeSymbol){
				//return false since a is not equal to b
				return false;
			}
			//now a and b have to be class symbols
			ClassSymbol aClass, bClass;
			aClass = (ClassSymbol) a;
			bClass = (ClassSymbol) b;
			while(!found && !bClass.toString().equals(SemanticUtils.CS_OBJECT)){
				bClass=bClass.superClass;
				if(aClass.equals(bClass)){
					found=true;
				}
			}
		}
		return found; 
	}
	private class SemanticVisitor extends AstVisitor<String,Symbol>{

		@Override
		public String visit(Ast ast, Symbol arg) {
			// TODO Auto-generated method stub
			return super.visit(ast, arg);
		}

		@Override
		public String visitChildren(Ast ast, Symbol arg) {
			// TODO Auto-generated method stub
			return super.visitChildren(ast, arg);
		}

		@Override
		public String assign(Assign ast, Symbol arg) {
			String leftType, rightType;
			
			boolean a,b,c;
			a = ast.left() instanceof Var;
			b = ast.left() instanceof Field;
			c = ast.left() instanceof Index;
			if(!(a || b || c)){
				throw new SemanticFailure(SemanticFailure.Cause.NOT_ASSIGNABLE);
			}
			
			leftType = visit(ast.left(),arg);
			rightType = visit(ast.right(),arg);
			if(ast.right() instanceof NullConst){
				if(leftType.equals(SemanticUtils.PT_INTEGER) || leftType.equals(SemanticUtils.PT_FLOAT) || leftType.equals(SemanticUtils.PT_BOOLEAN)){
					throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
				}
			}
			else if(!isSubclass(typeMapper.getTypeSymbol(leftType),typeMapper.getTypeSymbol(rightType))){
				throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
			}
			
			return null;
		}

		@Override
		public String builtInWrite(BuiltInWrite ast, Symbol arg) {
			if(ast.rwChildren.size() != 1){
				//TODO: is this the right cause?!
				throw new SemanticFailure(SemanticFailure.Cause.WRONG_NUMBER_OF_ARGUMENTS);
			}
			if(!visitChildren(ast,arg).equals(SemanticUtils.PT_INTEGER)){
				throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
			}
			return null;
		}

		@Override
		public String builtInWriteFloat(BuiltInWriteFloat ast, Symbol arg) {
			if(ast.rwChildren.size() != 1){
				//TODO: is this the right cause?!
				throw new SemanticFailure(SemanticFailure.Cause.WRONG_NUMBER_OF_ARGUMENTS);
			}
			if(!visitChildren(ast,arg).equals(SemanticUtils.PT_FLOAT)){
				throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
			}
			return null;
		}

		@Override
		public String builtInWriteln(BuiltInWriteln ast, Symbol arg) {
			// TODO Auto-generated method stub
			return super.builtInWriteln(ast, arg);
		}

		@Override
		public String classDecl(ClassDecl ast, Symbol arg) {
			// TODO Auto-generated method stub
			return super.classDecl(ast, arg);
		}

		@Override
		public String methodDecl(MethodDecl ast, Symbol arg) {
			// TODO Auto-generated method stub
			//check, whether we have a return statement at the end of all possible paths, if the return type is not void
			retVisitor.visit(ast, arg);
			return super.methodDecl(ast, ast.sym);
		}

		@Override
		public String varDecl(VarDecl ast, Symbol arg) {
			// TODO Auto-generated method stub
			return super.varDecl(ast, arg);
		}

		@Override
		public String ifElse(IfElse ast, Symbol arg) {
			String condition = visit(ast.condition(),arg);
			if(!condition.equals(SemanticUtils.PT_BOOLEAN)){
				throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
			}
			visit(ast.then(),arg);
			visit(ast.otherwise(),arg);
			return null;
		}

		@Override
		public String returnStmt(ReturnStmt ast, Symbol arg) {
			String returnType;
			if(ast.arg() != null){
				returnType = visit(ast.arg(),arg);
			}
			else{
				returnType = SemanticUtils.PT_VOID;
			}
			MethodSymbol methodSymbol = (MethodSymbol) arg;
			if(!isSubclass(methodSymbol.returnType,typeMapper.getTypeSymbol(returnType))){
				throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
			}
			return super.returnStmt(ast, arg);
		}

		@Override
		public String methodCall(MethodCall ast, Symbol arg) {
			ClassSymbol cs;
			MethodSymbol ms = null;
			//call this to visit the reciever
			String type = visit(ast.receiver(),arg);
			//call this to visit the arguments
			super.methodCall(ast, arg);
			if(ast.receiver() instanceof ThisRef){
				MethodSymbol sym = (MethodSymbol) arg;
				cs = sym.inClass;
			}
			else{
				if(typeMapper.getTypeSymbol(type) instanceof PrimitiveTypeSymbol || typeMapper.getTypeSymbol(type) instanceof ArrayTypeSymbol){
					throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
				}
				else{
					cs = (ClassSymbol) typeMapper.getTypeSymbol(type);
				}
			}
			ms=cs.methods.get(ast.methodName);
			if(!cs.methods.containsKey(ast.methodName)){
				boolean found=false;
				while(!found && !cs.superClass.toString().equals(SemanticUtils.CS_OBJECT)){
					cs=cs.superClass;
					if(cs.methods.containsKey(ast.methodName)){
						found=true;
						ms=cs.methods.get(ast.methodName);
					}
				}
				if(!found){
					throw new SemanticFailure(SemanticFailure.Cause.NO_SUCH_METHOD);
				}
			}
			//check if the actual arguments are the same number as the formal ones
			if(ms.parameters.size() != ast.argumentsWithoutReceiver().size()){
				throw new SemanticFailure(SemanticFailure.Cause.WRONG_NUMBER_OF_ARGUMENTS);
			}
			//check if the actual parameters are a subclass of the formal parameters
			List forArgs, actArgs;
			forArgs=ms.parameters;
			actArgs=ast.argumentsWithoutReceiver();
			Iterator<VariableSymbol> ifor = forArgs.iterator();
			Iterator<Expr> iact = actArgs.iterator();
			VariableSymbol variableSymbol;
			String actType;
			while(ifor.hasNext()){
				variableSymbol=ifor.next();
				actType = visit(iact.next(),arg);
				if(!isSubclass(variableSymbol.type,typeMapper.getTypeSymbol(actType))){
					throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
				}
			}
			return ms.returnType.toString();
		}

		@Override
		public String nop(Nop ast, Symbol arg) {
			// TODO Auto-generated method stub
			return super.nop(ast, arg);
		}

		@Override
		public String seq(Seq ast, Symbol arg) {
			// TODO Auto-generated method stub
			return super.seq(ast, arg);
		}

		@Override
		public String whileLoop(WhileLoop ast, Symbol arg) {
			String condition = visit(ast.condition(),arg);
			if(!condition.equals(SemanticUtils.PT_BOOLEAN)){
				throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
			}
			visit(ast.body(),arg);
			return null;
		}

		@Override
		public String visit(Expr ast, Symbol arg) {
			// TODO Auto-generated method stub
			return super.visit(ast, arg);
		}

		@Override
		public String visitChildren(Expr ast, Symbol arg) {
			// TODO Auto-generated method stub
			return super.visitChildren(ast, arg);
		}

		@Override
		public String binaryOp(BinaryOp ast, Symbol arg) {
			String leftType = visit(ast.left(),arg);
			String rightType = visit(ast.right(),arg);
			//if true, the left and right type have to be equal
			boolean same=true;
			String type="";
			switch(ast.operator){
				case B_TIMES:
					//we only compare the left expression type because we check afterwards, if left and right are equal
					if(!leftType.equals(SemanticUtils.PT_INTEGER) && !leftType.equals(SemanticUtils.PT_FLOAT)){
						throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
					}
					type=leftType;
					break;
				case B_DIV:
					//we only compare the left expression type because we check afterwards, if left and right are equal
					if(!leftType.equals(SemanticUtils.PT_INTEGER) && !leftType.equals(SemanticUtils.PT_FLOAT)){
						throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
					}
					type=leftType;
					break;
				case B_MOD:
					//we only compare the left expression type because we check afterwards, if left and right are equal
					if(!leftType.equals(SemanticUtils.PT_INTEGER)){
						throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
					}
					type=leftType;
					break;
				case B_PLUS:
					//we only compare the left expression type because we check afterwards, if left and right are equal
					if(!leftType.equals(SemanticUtils.PT_INTEGER) && !leftType.equals(SemanticUtils.PT_FLOAT)){
						throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
					}
					type=leftType;
					break;
				case B_MINUS:
					//we only compare the left expression type because we check afterwards, if left and right are equal
					if(!leftType.equals(SemanticUtils.PT_INTEGER) && !leftType.equals(SemanticUtils.PT_FLOAT)){
						throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
					}
					type=leftType;
					break;
				case B_AND:
					//we only compare the left expression type because we check afterwards, if left and right are equal
					if(!leftType.equals(SemanticUtils.PT_BOOLEAN)){
						throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
					}
					type=SemanticUtils.PT_BOOLEAN;
					break;
				case B_OR:
					//we only compare the left expression type because we check afterwards, if left and right are equal
					if(!leftType.equals(SemanticUtils.PT_BOOLEAN)){
						throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
					}
					type=SemanticUtils.PT_BOOLEAN;
					break;
				case B_EQUAL:
					same=false;
					if(!isSubclass(typeMapper.getTypeSymbol(leftType), typeMapper.getTypeSymbol(rightType)) && !isSubclass(typeMapper.getTypeSymbol(rightType), typeMapper.getTypeSymbol(leftType))){
						throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
					}
					type=SemanticUtils.PT_BOOLEAN;
					break;
				case B_NOT_EQUAL:
					same=false;
					if(!isSubclass(typeMapper.getTypeSymbol(leftType), typeMapper.getTypeSymbol(rightType)) && !isSubclass(typeMapper.getTypeSymbol(rightType), typeMapper.getTypeSymbol(leftType))){
						throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
					}
					type=SemanticUtils.PT_BOOLEAN;
					break;
				case B_LESS_THAN:
					//we only compare the left expression type because we check afterwards, if left and right are equal
					if(!leftType.equals(SemanticUtils.PT_INTEGER) && !leftType.equals(SemanticUtils.PT_FLOAT)){
						throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
					}
					type=SemanticUtils.PT_BOOLEAN;
					break;
				case B_LESS_OR_EQUAL:
					//we only compare the left expression type because we check afterwards, if left and right are equal
					if(!leftType.equals(SemanticUtils.PT_INTEGER) && !leftType.equals(SemanticUtils.PT_FLOAT)){
						throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
					}
					type=SemanticUtils.PT_BOOLEAN;
					break;
				case B_GREATER_THAN:
					//we only compare the left expression type because we check afterwards, if left and right are equal
					if(!leftType.equals(SemanticUtils.PT_INTEGER) && !leftType.equals(SemanticUtils.PT_FLOAT)){
						throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
					}
					type=SemanticUtils.PT_BOOLEAN;
					break;
				case B_GREATER_OR_EQUAL:
					//we only compare the left expression type because we check afterwards, if left and right are equal
					if(!leftType.equals(SemanticUtils.PT_INTEGER) && !leftType.equals(SemanticUtils.PT_FLOAT)){
						throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
					}
					type=SemanticUtils.PT_BOOLEAN;
					break;
			}
			if (same && !leftType.equals(rightType)){
				throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
			}
			ast.type = typeMapper.getTypeSymbol(type);
			return type;
		}

		@Override
		public String booleanConst(BooleanConst ast, Symbol arg) {
			ast.type = typeMapper.getTypeSymbol(SemanticUtils.PT_BOOLEAN);
			return SemanticUtils.PT_BOOLEAN;
		}

		@Override
		public String builtInRead(BuiltInRead ast, Symbol arg) {
			ast.type = typeMapper.getTypeSymbol(SemanticUtils.PT_INTEGER);
			return SemanticUtils.PT_INTEGER;
		}

		@Override
		public String builtInReadFloat(BuiltInReadFloat ast, Symbol arg) {
			ast.type = typeMapper.getTypeSymbol(SemanticUtils.PT_FLOAT);
			return SemanticUtils.PT_FLOAT;
		}

		@Override
		public String cast(Cast ast, Symbol arg) {
			String argType, castType;
			argType = visit(ast.arg(),arg);
			castType = ast.typeName;
			TypeSymbol argSymbol, castSymbol;
			argSymbol = typeMapper.getTypeSymbol(argType);
			castSymbol = typeMapper.getTypeSymbol(castType);
			if(!isSubclass(castSymbol,argSymbol) && !isSubclass(argSymbol,castSymbol)){
				throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
			}
			ast.type = typeMapper.getTypeSymbol(castType);
			return castType;
		}

		@Override
		public String field(Field ast, Symbol arg) {
			ClassSymbol cs;
			VariableSymbol varSymbol = null;
			String type = super.field(ast, arg);
			
			if(ast.arg() instanceof ThisRef){
				MethodSymbol sym = (MethodSymbol) arg;
				cs = sym.inClass;
			}
			else{
				if(typeMapper.getTypeSymbol(type) instanceof PrimitiveTypeSymbol || typeMapper.getTypeSymbol(type) instanceof ArrayTypeSymbol){
					throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
				}
				else{
					cs = (ClassSymbol) typeMapper.getTypeSymbol(type);
				}
			}
			if(!cs.fields.containsKey(ast.fieldName)){
				boolean found=false;
				while(!found && !cs.superClass.toString().equals(SemanticUtils.CS_OBJECT)){
					cs=cs.superClass;
					if(cs.fields.containsKey(ast.fieldName)){
						found=true;
						varSymbol = cs.fields.get(ast.fieldName);
					}
				}
				if(!found){
					throw new SemanticFailure(SemanticFailure.Cause.NO_SUCH_FIELD);
				}
			}
			else{
				varSymbol = cs.fields.get(ast.fieldName);
			}
			ast.sym = varSymbol;
			return varSymbol.type.toString();
		}

		@Override
		public String index(Index ast, Symbol arg) {
			String array,index;
			array = visit(ast.left(),arg);
			index = visit(ast.right(),arg);
			if(!index.equals(SemanticUtils.PT_INTEGER)){
				throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR); 
			}
			if(!(typeMapper.getTypeSymbol(array) instanceof ArrayTypeSymbol)){
				throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
			}
			//return the type of the element type
			ast.type = ((ArrayTypeSymbol)typeMapper.getTypeSymbol(array)).elementType;
			return ((ArrayTypeSymbol)typeMapper.getTypeSymbol(array)).elementType.toString();
		}

		@Override
		public String intConst(IntConst ast, Symbol arg) {
			ast.type = typeMapper.getTypeSymbol(SemanticUtils.PT_INTEGER);
			return SemanticUtils.PT_INTEGER;
		}

		@Override
		public String floatConst(FloatConst ast, Symbol arg) {
			ast.type = typeMapper.getTypeSymbol(SemanticUtils.PT_FLOAT);
			return SemanticUtils.PT_FLOAT;
		}

		@Override
		public String methodCall(MethodCallExpr ast, Symbol arg) {
			ClassSymbol cs;
			MethodSymbol ms = null;
			//call this to visit the reciever
			String type = visit(ast.receiver(),arg);
			//call this to visit the arguments
			super.methodCall(ast, arg);
			if(ast.receiver() instanceof ThisRef){
				MethodSymbol sym = (MethodSymbol) arg;
				cs = sym.inClass;
			}
			else{
				if(typeMapper.getTypeSymbol(type) instanceof PrimitiveTypeSymbol || typeMapper.getTypeSymbol(type) instanceof ArrayTypeSymbol){
					throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
				}
				else{
					cs = (ClassSymbol) typeMapper.getTypeSymbol(type);
				}
			}
			ms=cs.methods.get(ast.methodName);
			if(!cs.methods.containsKey(ast.methodName)){
				boolean found=false;
				while(!found && !cs.superClass.toString().equals(SemanticUtils.CS_OBJECT)){
					cs=cs.superClass;
					if(cs.methods.containsKey(ast.methodName)){
						found=true;
						ms=cs.methods.get(ast.methodName);
					}
				}
				if(!found){
					throw new SemanticFailure(SemanticFailure.Cause.NO_SUCH_METHOD);
				}
			}
			//check if the actual arguments are the same number as the formal ones
			if(ms.parameters.size() != ast.argumentsWithoutReceiver().size()){
				throw new SemanticFailure(SemanticFailure.Cause.WRONG_NUMBER_OF_ARGUMENTS);
			}
			//check if the actual arguments are a subclass of the formal ones
			List forArgs, actArgs;
			forArgs=ms.parameters;
			actArgs=ast.argumentsWithoutReceiver();
			Iterator<VariableSymbol> ifor = forArgs.iterator();
			Iterator<Expr> iact = actArgs.iterator();
			VariableSymbol variableSymbol;
			String actType;
			while(ifor.hasNext()){
				variableSymbol=ifor.next();
				actType = visit(iact.next(),arg);
				if(!isSubclass(variableSymbol.type,typeMapper.getTypeSymbol(actType))){
					throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
				}
			}
			
			
			ast.type = ms.returnType;
			return ms.returnType.toString();
			
		}

		@Override
		public String newObject(NewObject ast, Symbol arg) {	
			typeMapper.getTypeSymbol(ast.typeName);
			ast.type = typeMapper.getTypeSymbol(ast.typeName); 
			return ast.typeName;
		}

		@Override
		public String newArray(NewArray ast, Symbol arg) {
			String capacity = visit(ast.arg(),arg);
			if(!capacity.equals(SemanticUtils.PT_INTEGER)){
				throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
			}
			ast.type = typeMapper.getTypeSymbol(ast.typeName);
			return ast.typeName;
		}

		@Override
		public String nullConst(NullConst ast, Symbol arg) {
			ast.type = typeMapper.getTypeSymbol(SemanticUtils.CS_NULL);
			return SemanticUtils.CS_NULL;
		}

		@Override
		public String thisRef(ThisRef ast, Symbol arg) {
			ClassSymbol cs = ((MethodSymbol) arg).inClass;
			ast.type = cs;
			return cs.name;
		}

		@Override
		public String unaryOp(UnaryOp ast, Symbol arg) {
			String type = super.unaryOp(ast, arg);
			switch (ast.operator) {
			case U_PLUS:
				if(!type.equals(SemanticUtils.PT_INTEGER) && !type.equals(SemanticUtils.PT_FLOAT)){
					throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
				}
				break;

			case U_MINUS:
				if(!type.equals(SemanticUtils.PT_INTEGER) && !type.equals(SemanticUtils.PT_FLOAT)){
					throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
				}
				break;

			case U_BOOL_NOT:
				if(!type.equals(SemanticUtils.PT_BOOLEAN)){
					throw new SemanticFailure(SemanticFailure.Cause.TYPE_ERROR);
				}
				break;
			}
			ast.type = typeMapper.getTypeSymbol(type);
			return type;
		}

		@Override
		public String var(Var ast, Symbol arg) {
			MethodSymbol meth = (MethodSymbol) arg;
			VariableSymbol varSymbol=null;
			//this variable is not in our local scope
			if(!meth.locals.containsKey(ast.name)){
				ClassSymbol superClass = meth.inClass;
				boolean found=false;
				while(found == false && !superClass.toString().equals(SemanticUtils.CS_OBJECT)){
					if(superClass.fields.containsKey(ast.name)){
						found=true;
						varSymbol = superClass.fields.get(ast.name);
						break;
					}
					superClass = superClass.superClass;
				}
				if(!found){
					//check if we are in a methoddecl and if our variable is one of its parameters
					if(arg != null){
						for(VariableSymbol vs:((MethodSymbol)arg).parameters){
							if(vs.name.equals(ast.name)){
								found = true;
								varSymbol = vs;
							}
						}
					}
				}
				if(!found){
					throw new SemanticFailure(SemanticFailure.Cause.NO_SUCH_VARIABLE);
				}
			}
			else{
				varSymbol = meth.locals.get(ast.name);
			}
			ast.sym = varSymbol;
			return varSymbol.type.toString();
		}

		

		
		
	}
}
