package cd.semantic;

import java.util.Iterator;
import java.util.List;

import cd.exceptions.SemanticFailure;
import cd.ir.Ast.ClassDecl;
import cd.ir.Ast.MethodDecl;
import cd.ir.Ast.VarDecl;
import cd.ir.AstVisitor;
import cd.ir.Symbol;
import cd.ir.Symbol.ArrayTypeSymbol;
import cd.ir.Symbol.ClassSymbol;
import cd.ir.Symbol.MethodSymbol;
import cd.ir.Symbol.PrimitiveTypeSymbol;
import cd.ir.Symbol.TypeSymbol;
import cd.ir.Symbol.VariableSymbol;

public class SymbolGenerator extends AstVisitor<Symbol, Symbol> {
	
	private TypeMapper typeMapper;
	
	private boolean hasStartingPoint = false;
	
	public void generateSymbols(List<ClassDecl> classDecls) throws SemanticFailure {
		hasStartingPoint = false;
		
		for(ClassDecl classDecl : classDecls) {
			 visit(classDecl,null);
		}
		
		if(!hasStartingPoint)
			throw new SemanticFailure(SemanticFailure.Cause.INVALID_START_POINT,"NoStartingPoint");
	}
	
	public SymbolGenerator(TypeMapper typeMapper) {
		this.typeMapper = typeMapper;
	}

	@Override
	public Symbol classDecl(ClassDecl ast, Symbol arg) {
		TypeSymbol classSymbol =  typeMapper.getTypeSymbol(ast.name);
		TypeSymbol superClass =  typeMapper.getTypeSymbol(ast.superClass);
		ClassSymbol classSymbolCast;
		ClassSymbol superClassCast;
		
		if(!(classSymbol instanceof ClassSymbol) || !(superClass instanceof ClassSymbol))
			throw new SemanticFailure(SemanticFailure.Cause.OTHER_CAUSE,"No classnames with other types allowed");
		else {
			classSymbolCast =(ClassSymbol) classSymbol;
			superClassCast =(ClassSymbol) superClass;
			classSymbolCast.superClass = superClassCast;
		}
		
		visitChildren(ast,classSymbol);
		
		ast.sym=classSymbolCast;
		return classSymbol;
	}

	@Override
	public Symbol methodDecl(MethodDecl ast, Symbol arg) {
		ClassSymbol cs = (ClassSymbol) arg;
		MethodSymbol methodSymbol = new MethodSymbol(ast,cs);
		methodSymbol.returnType = typeMapper.getTypeSymbol(ast.returnType);
		
		Iterator<String> argNIt = ast.argumentNames.iterator();
		Iterator<String> argTIt = ast.argumentTypes.iterator();
		while(argNIt.hasNext() && argTIt.hasNext()) {
			TypeSymbol type = typeMapper.getTypeSymbol(argTIt.next());
			VariableSymbol varSymbol = new VariableSymbol(argNIt.next(),type, VariableSymbol.Kind.PARAM);
			
			if(checkForName(methodSymbol.parameters,varSymbol))
				throw new SemanticFailure(SemanticFailure.Cause.DOUBLE_DECLARATION,"Double Params in method");
			else
				methodSymbol.parameters.add(varSymbol);
		}
		
		visitChildren(ast,methodSymbol);
		if(cs.methods.containsKey(ast.name))
			throw new SemanticFailure(SemanticFailure.Cause.DOUBLE_DECLARATION,"Double methods");
		else
			cs.methods.put(ast.name, methodSymbol);
		
		//check for startingpoint
		boolean c1,c2,c3,c4;
		 c1 = methodSymbol.name.equals(SemanticUtils.STARTING_POINT_METHOD);
		 c2 = methodSymbol.parameters.isEmpty();
		 c3 = methodSymbol.returnType.name.equals(SemanticUtils.PT_VOID);
		 c4 = cs.name.equals(SemanticUtils.STARTING_POINT_CLASS);
		if(!hasStartingPoint && c1 && c2 && c3 && c4)
			hasStartingPoint=true;
		
		ast.sym=methodSymbol;
		return methodSymbol;
	}
	
	private boolean checkForName(List<VariableSymbol> vsl, VariableSymbol vs) {
		for(VariableSymbol v : vsl) {
			if(v.compareNames(vs)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public Symbol varDecl(VarDecl ast, Symbol arg) {
		
		VariableSymbol varSymbol = null;
		
		TypeSymbol type;

		type = typeMapper.getTypeSymbol(ast.type);
		
		if(arg instanceof ClassSymbol) {
			ClassSymbol classSymbol = (ClassSymbol) arg;
			varSymbol = new VariableSymbol(ast.name,type, VariableSymbol.Kind.FIELD);
			if(classSymbol.fields.containsKey(ast.name))
				throw new SemanticFailure(SemanticFailure.Cause.DOUBLE_DECLARATION,"Double Fields");
			classSymbol.fields.put(ast.name, varSymbol);
			
		} else if(arg instanceof MethodSymbol) {
			MethodSymbol methodSymbol = (MethodSymbol) arg;
			varSymbol = new VariableSymbol(ast.name,type, VariableSymbol.Kind.LOCAL);
			if(methodSymbol.locals.containsKey(ast.name))
				throw new SemanticFailure(SemanticFailure.Cause.DOUBLE_DECLARATION,"Double Locals");
			methodSymbol.locals.put(ast.name, varSymbol);
			
		}
		ast.sym = varSymbol;
		return varSymbol;
	}
	
}
