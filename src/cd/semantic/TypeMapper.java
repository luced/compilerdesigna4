package cd.semantic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cd.exceptions.SemanticFailure;
import cd.ir.Ast.ClassDecl;
import cd.ir.Symbol.ArrayTypeSymbol;
import cd.ir.Symbol.ClassSymbol;
import cd.ir.Symbol.NullTypeSymbol;
import cd.ir.Symbol.PrimitiveTypeSymbol;
import cd.ir.Symbol.TypeSymbol;

public class TypeMapper {
	
	private Map<String,TypeSymbol> typeSymbolMapping = new HashMap<String,TypeSymbol>();
	
	public TypeMapper(List<ClassDecl> classDecls) throws SemanticFailure {
		addDefaultTypeSymbols();
		for(ClassDecl classDecl : classDecls) {
			ClassSymbol cs = new ClassSymbol(classDecl);
			if(classDecl.name.equals(SemanticUtils.CS_OBJECT))
				throw new SemanticFailure(SemanticFailure.Cause.OBJECT_CLASS_DEFINED);
			else if(typeSymbolMapping.containsKey(classDecl.name))
				throw new SemanticFailure(SemanticFailure.Cause.DOUBLE_DECLARATION);
			typeSymbolMapping.put(classDecl.name, cs);
		}
	}
	
	public TypeSymbol getTypeSymbol(String type) throws SemanticFailure {
		TypeSymbol ts = typeSymbolMapping.get(type);
		if(ts == null)
			if(type.contains(SemanticUtils.ARRAY_TYPE_IDENTIFIER)) {
				ts = typeSymbolMapping.get(SemanticUtils.removeArrayIdent(type));
				if(ts == null)
					throw new SemanticFailure(SemanticFailure.Cause.NO_SUCH_TYPE,"ArrayType not found");
				ts = new ArrayTypeSymbol(ts);
				typeSymbolMapping.put(type, ts);
			}
			else {
				throw new SemanticFailure(SemanticFailure.Cause.NO_SUCH_TYPE,"Type not found");
			}
		
		return ts;
	}
	
	public ClassSymbol getClassSymbol(String classString) {
		TypeSymbol ts = getTypeSymbol(classString);
		if(ts instanceof ClassSymbol)
			return (ClassSymbol) ts;
		else 
			throw new RuntimeException("No Classsymbol found");
	}
	
	private void addDefaultTypeSymbols() {
		PrimitiveTypeSymbol bool = new PrimitiveTypeSymbol(SemanticUtils.PT_BOOLEAN);
		PrimitiveTypeSymbol integer = new PrimitiveTypeSymbol(SemanticUtils.PT_INTEGER);
		PrimitiveTypeSymbol floatp = new PrimitiveTypeSymbol(SemanticUtils.PT_FLOAT);
		PrimitiveTypeSymbol voidp = new PrimitiveTypeSymbol(SemanticUtils.PT_VOID);
		NullTypeSymbol nullp = new NullTypeSymbol();
		ClassSymbol object = new ClassSymbol(SemanticUtils.CS_OBJECT);
		typeSymbolMapping.put(SemanticUtils.PT_BOOLEAN, bool);
		typeSymbolMapping.put(SemanticUtils.PT_INTEGER, integer);
		typeSymbolMapping.put(SemanticUtils.PT_FLOAT, floatp);
		typeSymbolMapping.put(SemanticUtils.CS_OBJECT, object);
		typeSymbolMapping.put(SemanticUtils.PT_VOID, voidp);
		typeSymbolMapping.put(SemanticUtils.CS_NULL, nullp);
	}
	
}
