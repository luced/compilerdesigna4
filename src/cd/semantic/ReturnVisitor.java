package cd.semantic;

import cd.exceptions.SemanticFailure;
import cd.ir.Ast.Assign;
import cd.ir.Ast.BuiltInWrite;
import cd.ir.Ast.BuiltInWriteFloat;
import cd.ir.Ast.BuiltInWriteln;
import cd.ir.Ast.ClassDecl;
import cd.ir.Ast.IfElse;
import cd.ir.Ast.MethodCall;
import cd.ir.Ast.MethodDecl;
import cd.ir.Ast.Nop;
import cd.ir.Ast.ReturnStmt;
import cd.ir.Ast.Seq;
import cd.ir.Ast.VarDecl;
import cd.ir.Ast.WhileLoop;
import cd.ir.Ast;
import cd.ir.AstVisitor;
import cd.ir.Symbol;

public class ReturnVisitor extends AstVisitor<String,Symbol>{

	@Override
	public String assign(Assign ast, Symbol arg) {
		// TODO Auto-generated method stub
		return SemanticUtils.BL_FALSE;
	}

	@Override
	public String builtInWrite(BuiltInWrite ast, Symbol arg) {
		// TODO Auto-generated method stub
		return SemanticUtils.BL_FALSE;
	}

	@Override
	public String builtInWriteFloat(BuiltInWriteFloat ast, Symbol arg) {
		// TODO Auto-generated method stub
		return SemanticUtils.BL_FALSE;
	}

	@Override
	public String builtInWriteln(BuiltInWriteln ast, Symbol arg) {
		// TODO Auto-generated method stub
		return SemanticUtils.BL_FALSE;
	}

	@Override
	public String classDecl(ClassDecl ast, Symbol arg) {
		// TODO Auto-generated method stub
		return super.classDecl(ast, arg);
	}

	@Override
	public String methodDecl(MethodDecl ast, Symbol arg) {
		// TODO Auto-generated method stub
		String a = visit(ast.body(),arg);
		if(a.equals(SemanticUtils.BL_FALSE) && !ast.returnType.equals(SemanticUtils.PT_VOID)){
			throw new SemanticFailure(SemanticFailure.Cause.MISSING_RETURN);
		}
			
		return super.methodDecl(ast, arg);
	}

	@Override
	public String varDecl(VarDecl ast, Symbol arg) {
		// TODO Auto-generated method stub
		return SemanticUtils.BL_FALSE;
	}

	@Override
	public String ifElse(IfElse ast, Symbol arg) {
		// TODO Auto-generated method stub
		String a,b;
		a=visit(ast.then(),arg);
		b=visit(ast.otherwise(),arg);
		if(a.equals(SemanticUtils.BL_TRUE) && b.equals(SemanticUtils.BL_TRUE)){
			return SemanticUtils.BL_TRUE;
		}
		else{
			return SemanticUtils.BL_FALSE;
		}
		
	}

	@Override
	public String returnStmt(ReturnStmt ast, Symbol arg) {
		// TODO Auto-generated method stub
		return SemanticUtils.BL_TRUE;
	}

	@Override
	public String methodCall(MethodCall ast, Symbol arg) {
		// TODO Auto-generated method stub
		return SemanticUtils.BL_FALSE;
	}

	@Override
	public String nop(Nop ast, Symbol arg) {
		// TODO Auto-generated method stub
		return SemanticUtils.BL_FALSE;
	}

	@Override
	public String seq(Seq ast, Symbol arg) {
		// TODO Auto-generated method stub
		String a=SemanticUtils.BL_FALSE;
		for (Ast child : ast.children()){
			a = visit(child, arg);
			if(a.equals(SemanticUtils.BL_TRUE)){
				return SemanticUtils.BL_TRUE;
			}
		}
		return SemanticUtils.BL_FALSE;
	}

	@Override
	public String whileLoop(WhileLoop ast, Symbol arg) {
		// TODO Auto-generated method stub
		return SemanticUtils.BL_FALSE;
	}

}
