package cd.semantic;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import cd.exceptions.SemanticFailure;
import cd.ir.Ast.ClassDecl;
import cd.ir.Ast.MethodDecl;
import cd.ir.AstVisitor;
import cd.ir.Symbol;
import cd.ir.Symbol.ClassSymbol;
import cd.ir.Symbol.MethodSymbol;
import cd.ir.Symbol.VariableSymbol;

/**
 * Checks for correct Inheritance and overrides
 * @author lukas
 *
 */
public class InheritanceChecker {
	
	
	public void checkForInheritance(List<ClassDecl> classDecls) throws SemanticFailure {
		for(ClassDecl classDecl : classDecls) {
			ClassSymbol curClassSym = classDecl.sym;
			if(curClassSym==null)
				throw new RuntimeException("No ClassSymbol for class "+classDecl.name);
			checkForCycle(curClassSym);
			checkForOverridesWithSuperClass(curClassSym);
		}
	}

	private void checkForCycle(ClassSymbol curClassSym) throws SemanticFailure {
		HashSet<String> foundNames = new HashSet<String>();
		ClassSymbol superC = curClassSym.superClass;
		foundNames.add(curClassSym.name);
		
		while(!superC.name.equals(SemanticUtils.CS_OBJECT)) {
			if(foundNames.contains(superC.name)) 
				throw new SemanticFailure(SemanticFailure.Cause.CIRCULAR_INHERITANCE,"Circular detected on "+superC.name);
			else {
				foundNames.add(superC.name);
				superC= superC.superClass;
			}
		}
		
	}
	
	private void checkForOverridesWithSuperClass(ClassSymbol cs) throws SemanticFailure {
		ClassSymbol superC =cs.superClass;
	
		if(superC.name.equals(SemanticUtils.CS_OBJECT))
			return;
		
		Collection<MethodSymbol> superMethods = superC.methods.values();
		Collection<MethodSymbol> classMethods = cs.methods.values();
		for(MethodSymbol mSuper : superMethods) {
			MethodSymbol doubleMethod = checkForName(classMethods,mSuper);
			if(!(doubleMethod==null)) {
				if(doubleMethod.parameters.size()!=mSuper.parameters.size()) {
					throw new SemanticFailure(SemanticFailure.Cause.INVALID_OVERRIDE,"ParameterCount override missmach on "+doubleMethod.name);
				}
				
				if(!doubleMethod.returnType.compareNames(mSuper.returnType)) {
					throw new SemanticFailure(SemanticFailure.Cause.INVALID_OVERRIDE,"ReturnType override missmach on "+doubleMethod.name);
				}
				
				Iterator<VariableSymbol> itMetPar= doubleMethod.parameters.iterator();
				Iterator<VariableSymbol> itSupMetPar = mSuper.parameters.iterator();
				
				while(itMetPar.hasNext() && itSupMetPar.hasNext()) {
					if(!itMetPar.next().type.compareNames(itSupMetPar.next().type)) 
						throw new SemanticFailure(SemanticFailure.Cause.INVALID_OVERRIDE,"ParameterType override missmach on "+doubleMethod.name);	
				}
				
			}
		}
		
	}
	
	private MethodSymbol checkForName(Collection<MethodSymbol> vsl, MethodSymbol vs) {
		for(MethodSymbol v : vsl) {
			if(v.compareNames(vs)) {
				return v;
			}
		}
		return null;
	}
	

}
