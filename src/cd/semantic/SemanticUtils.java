package cd.semantic;

public class SemanticUtils {
	
	//primitive Types
	public static String PT_BOOLEAN = "boolean";
	
	public static String PT_INTEGER = "int";
	
	public static String PT_FLOAT = "float";
	
	public static String CS_OBJECT = "Object";
	
	public static String PT_VOID = "void";
	
	public static String CS_NULL = "null";
	
	public static String ARRAY_TYPE_IDENTIFIER = "[]";
	
	public static String STARTING_POINT_METHOD = "main";
	
	public static String STARTING_POINT_CLASS = "Main";
	
	public static String removeArrayIdent(String type) {
		return type.replace(ARRAY_TYPE_IDENTIFIER, "");
	}
	
	// this reference
	public static String RF_THIS = "this";
	//boolean values
	public static String BL_TRUE = "true";
	public static String BL_FALSE = "false";
}
