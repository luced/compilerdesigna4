// $ANTLR 3.5.1 /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g 2014-10-26 15:42:59

package cd.parser;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.tree.*;


/**************************************************************************************************
 *   JAVALI PARSER GRAMMAR
 *   Compiler Construction I
 *
 *   ANTLR TIPS AND TRICKS:
 *   -----------------------
 *
 *   Overview:
 *   ----------
 *
 *   ANTLR is a top-down (LL) parser generator. Given a grammar file
 *   (*.g), ANTLR generates a parser accepting programs written in the
 *   language specified by the grammar. By default, the generated
 *   parser is a Java program.
 *
 *   An ANTLR grammar file (*.g) consists of several sections. In this
 *   grammar file we make use of the following sections:
 *
 *   (1) option declarations
 *   (2) token declarations
 *   (3) action declarations
 *   (4) rule declarations
 *
 *   The rule section must appear at the end. Options (1) alter the
 *   way ANTLR generates code. In this grammar file we use the option
 *   'output=AST', which allows us to apply rewriting rules for AST
 *   construction. The token declaration section (2) declares
 *   tokens. For each declared token, ANTLR generates a corresponding
 *   ANTLR AST node. Rules (4) are either lexer rules (4-a) or parser
 *   rules (4-b).  Lexer rules (4-a) must begin with an upper case
 *   letter and describe tokens that occur in the input stream.
 *   Parser rules (4-b) must begin with a lower case letter and
 *   describe grammar productions.  For each rule in a grammar file,
 *   ANTLR generates a corresponding method in the generated
 *   parser. Using the action declaration section (3), additional
 *   fields and methods can be declared that ANTLR inserts into the
 *   generated parser.
 *
 *   In this assignment, we provide the necessary option (1) and
 *   action (3) declarations, however, you must provide the
 *   appropriate token (2) and rule (4) declarations.
 *
 *   Imaginary tokens:
 *   ---------------
 *
 *   Using the tokens{...} specification, either imaginary tokens can
 *   be defined or aliases for token literals can be defined. An
 *   imaginary token is not associated with a particular input
 *   character (token literal) but is helpful for AST construction. An
 *   imaginary token can be assigned the line and column information
 *   from a token literal appearing in the input. The text of an
 *   imaginary token can be explicitly set to avoid copying the text
 *   from the literal token. E.g., in below example, an imaginary
 *   token ARGUMENTS is created and associated with the actual token
 *   '('. As a result, the imaginary token gets the line and column
 *   number information of the token '(', but hast the text
 *   "ARGUMENTS".
 *
 *   arguments
 *	:	lc='(' expressionlist? ')'
 *		-> ^(ARGUMENTS[$lc,"ARGUMENTS"] expressionlist?)
 *	;
 *
 *   Rules:
 *   ------
 *
 *   ANTLR supports EBNF. The following operators have the following meaning:
 *
 *   x?  x occurs at most once
 *   x*  x occurs zero or several times (left-associative)
 *   x+  x occurs once or several times (left-associative)
 *
 *   Note: Parentheses must be used to apply an operator to a group of tokens!
 *
 *
 *   Left-Associativity:   a op b op c = ( a op b ) op c
 *   Can in principle be achieved  using left recursion:
 *   E ::= E op T | T
 *   However, left-recursive rules are not accepted by ANTLR (see below)! Thus, left
 *   recursion must either be eliminated or, preferably, the EBNF repetition operator
 *   should be used:
 *   E ::= T ( op T)*
 *
 *   Right-Associativity:   a op b op c = a op ( b op c)
 *   Can be achieved using right recursion:
 *   E ::= T op E | T
 *   Above rule might have to be left factored (see below). Preferably, the EBNF '?'
 *   operator should be used:
 *   E ::= T ( op E )?
 *
 *   Left recursion, non-LL(*) decisions, and left-factoring:
 *   ----------------------------------------------
 *
 *   Being a top-down parser generator, ANTLR cannot deal with
 *   left-recursive rules. As a consequence, any left recursion must
 *   be eliminated.
 *
 *   In a top-down parser, a production alternative must be selected
 *   based on the tokens seen next in the input.  In ANTLR, the number
 *   of tokens used for the decision can be explicitly indicated
 *   (LL(k)) or, alternatively, a variable lookahead (LL(*)) can be
 *   used. For this assignment, we use a variable lookahead. The
 *   selection of a production alternative based on a number k of
 *   lookahead tokens can generally be done by a deterministic finite
 *   automaton (DFA) that recognizes strings of length k and that has
 *   accepting states for each alternative in question. A variable
 *   number of tokens lookahead (LL(*)) can be supported by allowing
 *   cyclic DFAs.
 *
 *   Although LL(*) is clearly superior to LL(k) and therefore accepts
 *   grammars that would not be accepted using a fixed number of
 *   lookahead tokens, there may still be cases for which ANTLR
 *   reports that a rule has "non-LL(*) decisions" even if the grammar
 *   is not ambiguous. This can be the case if the lookahead language
 *   is not regular (i.e., cannot be recognized by a DFA) and/or if
 *   ANTLR does not succeed constructing the DFA for the lookahead due
 *   to recursive (i.e., repetitive) constructs. ANTLR is actually
 *   only capable of constructing the DFA for the lookahead language
 *   in case of recursive constructs as long as only one alternative
 *   is recursive and as long as the internal recursion overflow
 *   constant is sufficiently large (see ANTLR book, page 271). The
 *   only remedy for non-LL(*) decisions is grammar left-factoring.
 *
 *   For example, the grammar below uses non-regular constructs
 *   (nested parentheses) and can therefore not be parsed using the
 *   LL(*) option.
 *
 *   se  =  e '%'  |  e '!' ;
 *   e   =  '(' e ')' | ID ;
 *
 *   In below example, the fact that both alternatives in s are
 *   (indirectly) recursive causes troubles.
 *
 *   s  =  label ID '=' expr
 *      |  label 'return' expr  ;
 *
 *   label = ID ':' label  |  ;
 *
 *   If above grammar is rewritten to use the EBNF looping syntax
 *   instead, however, ANTLR is capable of identifying the looping
 *   construct and constructing the cyclic DFA.
 *
 *   s  =  label ID '=' expr
 *      |  label 'return' expr  ;
 *
 *   label = ( ID ':' )*  ;
 *
 *   And in the following example, finally, ANTLR is capable of
 *   constructing the corresponding DFA for the lookahead language as
 *   only one alternative is recursive and as the looping analysis
 *   recurses less often than specified by the recursion overflow
 *   threshold (see ANTLR book, p. 271):
 *
 *   a  = L a R
 *      | L L X  ;
 *
**************************************************************************************************/
@SuppressWarnings("all")
public class JavaliParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "ActualParamList", "ArrayType", 
		"Assignment", "BooleanLiteral", "BuiltInRead", "BuiltInReadFloat", "BuiltInWrite", 
		"BuiltInWriteFloat", "BuiltInWriteln", "COMMENT", "ClassDecl", "ClassName", 
		"DecimalNumber", "DeclList", "DigitNumber", "ElemSelector", "Expr", "Factor", 
		"FieldSelector", "FloatNumber", "FormalParams", "HexDigit", "HexNumber", 
		"HexPrefix", "IdentAccess", "Identifier", "IfElse", "JavaIDDigit", "LINE_COMMENT", 
		"Letter", "MethodBody", "MethodCall", "MethodCallTail", "MethodDecl", 
		"MethodHead", "MethodName", "NewExpr", "NoSignFactor", "Param", "ReturnStmt", 
		"ReturnType", "SelectorSeq", "Seq", "SimpleExpr", "StmtBlock", "SuperClassName", 
		"Term", "Type", "TypeVal", "UNIT", "WS", "WhileStmt", "'!'", "'!='", "'%'", 
		"'&&'", "'('", "')'", "'*'", "'+'", "','", "'-'", "'.'", "'/'", "';'", 
		"'<'", "'<='", "'='", "'=='", "'>'", "'>='", "'['", "']'", "'boolean'", 
		"'class'", "'else'", "'extends'", "'float'", "'if'", "'int'", "'new'", 
		"'null'", "'read'", "'readf'", "'return'", "'this'", "'void'", "'while'", 
		"'write'", "'writef'", "'writeln'", "'{'", "'||'", "'}'"
	};
	public static final int EOF=-1;
	public static final int T__56=56;
	public static final int T__57=57;
	public static final int T__58=58;
	public static final int T__59=59;
	public static final int T__60=60;
	public static final int T__61=61;
	public static final int T__62=62;
	public static final int T__63=63;
	public static final int T__64=64;
	public static final int T__65=65;
	public static final int T__66=66;
	public static final int T__67=67;
	public static final int T__68=68;
	public static final int T__69=69;
	public static final int T__70=70;
	public static final int T__71=71;
	public static final int T__72=72;
	public static final int T__73=73;
	public static final int T__74=74;
	public static final int T__75=75;
	public static final int T__76=76;
	public static final int T__77=77;
	public static final int T__78=78;
	public static final int T__79=79;
	public static final int T__80=80;
	public static final int T__81=81;
	public static final int T__82=82;
	public static final int T__83=83;
	public static final int T__84=84;
	public static final int T__85=85;
	public static final int T__86=86;
	public static final int T__87=87;
	public static final int T__88=88;
	public static final int T__89=89;
	public static final int T__90=90;
	public static final int T__91=91;
	public static final int T__92=92;
	public static final int T__93=93;
	public static final int T__94=94;
	public static final int T__95=95;
	public static final int T__96=96;
	public static final int T__97=97;
	public static final int ActualParamList=4;
	public static final int ArrayType=5;
	public static final int Assignment=6;
	public static final int BooleanLiteral=7;
	public static final int BuiltInRead=8;
	public static final int BuiltInReadFloat=9;
	public static final int BuiltInWrite=10;
	public static final int BuiltInWriteFloat=11;
	public static final int BuiltInWriteln=12;
	public static final int COMMENT=13;
	public static final int ClassDecl=14;
	public static final int ClassName=15;
	public static final int DecimalNumber=16;
	public static final int DeclList=17;
	public static final int DigitNumber=18;
	public static final int ElemSelector=19;
	public static final int Expr=20;
	public static final int Factor=21;
	public static final int FieldSelector=22;
	public static final int FloatNumber=23;
	public static final int FormalParams=24;
	public static final int HexDigit=25;
	public static final int HexNumber=26;
	public static final int HexPrefix=27;
	public static final int IdentAccess=28;
	public static final int Identifier=29;
	public static final int IfElse=30;
	public static final int JavaIDDigit=31;
	public static final int LINE_COMMENT=32;
	public static final int Letter=33;
	public static final int MethodBody=34;
	public static final int MethodCall=35;
	public static final int MethodCallTail=36;
	public static final int MethodDecl=37;
	public static final int MethodHead=38;
	public static final int MethodName=39;
	public static final int NewExpr=40;
	public static final int NoSignFactor=41;
	public static final int Param=42;
	public static final int ReturnStmt=43;
	public static final int ReturnType=44;
	public static final int SelectorSeq=45;
	public static final int Seq=46;
	public static final int SimpleExpr=47;
	public static final int StmtBlock=48;
	public static final int SuperClassName=49;
	public static final int Term=50;
	public static final int Type=51;
	public static final int TypeVal=52;
	public static final int UNIT=53;
	public static final int WS=54;
	public static final int WhileStmt=55;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public JavaliParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public JavaliParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	protected TreeAdaptor adaptor = new CommonTreeAdaptor();

	public void setTreeAdaptor(TreeAdaptor adaptor) {
		this.adaptor = adaptor;
	}
	public TreeAdaptor getTreeAdaptor() {
		return adaptor;
	}
	@Override public String[] getTokenNames() { return JavaliParser.tokenNames; }
	@Override public String getGrammarFileName() { return "/Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g"; }


	protected void mismatch(TokenStream input, int ttype, BitSet follow) throws RecognitionException {
		throw new MismatchedTokenException(ttype, input);
	}

	public void recoverFromMismatchedSet(TokenStream input, RecognitionException e, BitSet follow) throws RecognitionException {
		throw e;
	}

	protected Object recoverFromMismatchedToken(IntStream input, int ttype, BitSet follow) throws RecognitionException {
		throw new MismatchedTokenException(ttype, input);
	}


	public static class unit_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "unit"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:247:1: unit : ( classDecl )+ EOF -> ^( UNIT ( classDecl )+ ) ;
	public final JavaliParser.unit_return unit() throws RecognitionException {
		JavaliParser.unit_return retval = new JavaliParser.unit_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token EOF2=null;
		ParserRuleReturnScope classDecl1 =null;

		Object EOF2_tree=null;
		RewriteRuleTokenStream stream_EOF=new RewriteRuleTokenStream(adaptor,"token EOF");
		RewriteRuleSubtreeStream stream_classDecl=new RewriteRuleSubtreeStream(adaptor,"rule classDecl");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:248:2: ( ( classDecl )+ EOF -> ^( UNIT ( classDecl )+ ) )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:248:4: ( classDecl )+ EOF
			{
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:248:4: ( classDecl )+
			int cnt1=0;
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( (LA1_0==78) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:248:4: classDecl
					{
					pushFollow(FOLLOW_classDecl_in_unit185);
					classDecl1=classDecl();
					state._fsp--;

					stream_classDecl.add(classDecl1.getTree());
					}
					break;

				default :
					if ( cnt1 >= 1 ) break loop1;
					EarlyExitException eee = new EarlyExitException(1, input);
					throw eee;
				}
				cnt1++;
			}

			EOF2=(Token)match(input,EOF,FOLLOW_EOF_in_unit188);  
			stream_EOF.add(EOF2);

			// AST REWRITE
			// elements: classDecl
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 248:19: -> ^( UNIT ( classDecl )+ )
			{
				// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:248:22: ^( UNIT ( classDecl )+ )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(UNIT, "UNIT"), root_1);
				if ( !(stream_classDecl.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_classDecl.hasNext() ) {
					adaptor.addChild(root_1, stream_classDecl.nextTree());
				}
				stream_classDecl.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "unit"


	public static class classDecl_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "classDecl"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:251:1: classDecl : ( 'class' cname= Identifier '{' ( declList )? '}' -> ^( ClassDecl ^( ClassName $cname) ( declList )? ) | 'class' clname= Identifier 'extends' sname= Identifier '{' ( declList )? '}' -> ^( ClassDecl ^( ClassName $clname) ^( SuperClassName $sname) ( declList )? ) );
	public final JavaliParser.classDecl_return classDecl() throws RecognitionException {
		JavaliParser.classDecl_return retval = new JavaliParser.classDecl_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token cname=null;
		Token clname=null;
		Token sname=null;
		Token string_literal3=null;
		Token char_literal4=null;
		Token char_literal6=null;
		Token string_literal7=null;
		Token string_literal8=null;
		Token char_literal9=null;
		Token char_literal11=null;
		ParserRuleReturnScope declList5 =null;
		ParserRuleReturnScope declList10 =null;

		Object cname_tree=null;
		Object clname_tree=null;
		Object sname_tree=null;
		Object string_literal3_tree=null;
		Object char_literal4_tree=null;
		Object char_literal6_tree=null;
		Object string_literal7_tree=null;
		Object string_literal8_tree=null;
		Object char_literal9_tree=null;
		Object char_literal11_tree=null;
		RewriteRuleTokenStream stream_97=new RewriteRuleTokenStream(adaptor,"token 97");
		RewriteRuleTokenStream stream_78=new RewriteRuleTokenStream(adaptor,"token 78");
		RewriteRuleTokenStream stream_95=new RewriteRuleTokenStream(adaptor,"token 95");
		RewriteRuleTokenStream stream_80=new RewriteRuleTokenStream(adaptor,"token 80");
		RewriteRuleTokenStream stream_Identifier=new RewriteRuleTokenStream(adaptor,"token Identifier");
		RewriteRuleSubtreeStream stream_declList=new RewriteRuleSubtreeStream(adaptor,"rule declList");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:252:2: ( 'class' cname= Identifier '{' ( declList )? '}' -> ^( ClassDecl ^( ClassName $cname) ( declList )? ) | 'class' clname= Identifier 'extends' sname= Identifier '{' ( declList )? '}' -> ^( ClassDecl ^( ClassName $clname) ^( SuperClassName $sname) ( declList )? ) )
			int alt4=2;
			int LA4_0 = input.LA(1);
			if ( (LA4_0==78) ) {
				int LA4_1 = input.LA(2);
				if ( (LA4_1==Identifier) ) {
					int LA4_2 = input.LA(3);
					if ( (LA4_2==95) ) {
						alt4=1;
					}
					else if ( (LA4_2==80) ) {
						alt4=2;
					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 4, 2, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 4, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 4, 0, input);
				throw nvae;
			}

			switch (alt4) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:252:4: 'class' cname= Identifier '{' ( declList )? '}'
					{
					string_literal3=(Token)match(input,78,FOLLOW_78_in_classDecl209);  
					stream_78.add(string_literal3);

					cname=(Token)match(input,Identifier,FOLLOW_Identifier_in_classDecl213);  
					stream_Identifier.add(cname);

					char_literal4=(Token)match(input,95,FOLLOW_95_in_classDecl215);  
					stream_95.add(char_literal4);

					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:252:33: ( declList )?
					int alt2=2;
					int LA2_0 = input.LA(1);
					if ( (LA2_0==Identifier||LA2_0==77||LA2_0==81||LA2_0==83||LA2_0==90) ) {
						alt2=1;
					}
					switch (alt2) {
						case 1 :
							// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:252:33: declList
							{
							pushFollow(FOLLOW_declList_in_classDecl217);
							declList5=declList();
							state._fsp--;

							stream_declList.add(declList5.getTree());
							}
							break;

					}

					char_literal6=(Token)match(input,97,FOLLOW_97_in_classDecl220);  
					stream_97.add(char_literal6);

					// AST REWRITE
					// elements: declList, cname
					// token labels: cname
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleTokenStream stream_cname=new RewriteRuleTokenStream(adaptor,"token cname",cname);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 252:47: -> ^( ClassDecl ^( ClassName $cname) ( declList )? )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:252:51: ^( ClassDecl ^( ClassName $cname) ( declList )? )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ClassDecl, "ClassDecl"), root_1);
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:252:63: ^( ClassName $cname)
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(ClassName, "ClassName"), root_2);
						adaptor.addChild(root_2, stream_cname.nextNode());
						adaptor.addChild(root_1, root_2);
						}

						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:252:83: ( declList )?
						if ( stream_declList.hasNext() ) {
							adaptor.addChild(root_1, stream_declList.nextTree());
						}
						stream_declList.reset();

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:253:4: 'class' clname= Identifier 'extends' sname= Identifier '{' ( declList )? '}'
					{
					string_literal7=(Token)match(input,78,FOLLOW_78_in_classDecl242);  
					stream_78.add(string_literal7);

					clname=(Token)match(input,Identifier,FOLLOW_Identifier_in_classDecl246);  
					stream_Identifier.add(clname);

					string_literal8=(Token)match(input,80,FOLLOW_80_in_classDecl248);  
					stream_80.add(string_literal8);

					sname=(Token)match(input,Identifier,FOLLOW_Identifier_in_classDecl252);  
					stream_Identifier.add(sname);

					char_literal9=(Token)match(input,95,FOLLOW_95_in_classDecl254);  
					stream_95.add(char_literal9);

					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:253:61: ( declList )?
					int alt3=2;
					int LA3_0 = input.LA(1);
					if ( (LA3_0==Identifier||LA3_0==77||LA3_0==81||LA3_0==83||LA3_0==90) ) {
						alt3=1;
					}
					switch (alt3) {
						case 1 :
							// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:253:61: declList
							{
							pushFollow(FOLLOW_declList_in_classDecl256);
							declList10=declList();
							state._fsp--;

							stream_declList.add(declList10.getTree());
							}
							break;

					}

					char_literal11=(Token)match(input,97,FOLLOW_97_in_classDecl259);  
					stream_97.add(char_literal11);

					// AST REWRITE
					// elements: clname, declList, sname
					// token labels: clname, sname
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleTokenStream stream_clname=new RewriteRuleTokenStream(adaptor,"token clname",clname);
					RewriteRuleTokenStream stream_sname=new RewriteRuleTokenStream(adaptor,"token sname",sname);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 253:75: -> ^( ClassDecl ^( ClassName $clname) ^( SuperClassName $sname) ( declList )? )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:253:78: ^( ClassDecl ^( ClassName $clname) ^( SuperClassName $sname) ( declList )? )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ClassDecl, "ClassDecl"), root_1);
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:253:90: ^( ClassName $clname)
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(ClassName, "ClassName"), root_2);
						adaptor.addChild(root_2, stream_clname.nextNode());
						adaptor.addChild(root_1, root_2);
						}

						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:253:111: ^( SuperClassName $sname)
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(SuperClassName, "SuperClassName"), root_2);
						adaptor.addChild(root_2, stream_sname.nextNode());
						adaptor.addChild(root_1, root_2);
						}

						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:253:136: ( declList )?
						if ( stream_declList.hasNext() ) {
							adaptor.addChild(root_1, stream_declList.nextTree());
						}
						stream_declList.reset();

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "classDecl"


	public static class declList_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "declList"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:256:1: declList : ( decl )+ -> ^( DeclList ( decl )+ ) ;
	public final JavaliParser.declList_return declList() throws RecognitionException {
		JavaliParser.declList_return retval = new JavaliParser.declList_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope decl12 =null;

		RewriteRuleSubtreeStream stream_decl=new RewriteRuleSubtreeStream(adaptor,"rule decl");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:257:2: ( ( decl )+ -> ^( DeclList ( decl )+ ) )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:257:4: ( decl )+
			{
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:257:4: ( decl )+
			int cnt5=0;
			loop5:
			while (true) {
				int alt5=2;
				int LA5_0 = input.LA(1);
				if ( (LA5_0==Identifier) ) {
					int LA5_2 = input.LA(2);
					if ( (LA5_2==75) ) {
						int LA5_4 = input.LA(3);
						if ( (LA5_4==76) ) {
							alt5=1;
						}

					}
					else if ( (LA5_2==Identifier) ) {
						alt5=1;
					}

				}
				else if ( (LA5_0==77||LA5_0==81||LA5_0==83||LA5_0==90) ) {
					alt5=1;
				}

				switch (alt5) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:257:5: decl
					{
					pushFollow(FOLLOW_decl_in_declList294);
					decl12=decl();
					state._fsp--;

					stream_decl.add(decl12.getTree());
					}
					break;

				default :
					if ( cnt5 >= 1 ) break loop5;
					EarlyExitException eee = new EarlyExitException(5, input);
					throw eee;
				}
				cnt5++;
			}

			// AST REWRITE
			// elements: decl
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 257:12: -> ^( DeclList ( decl )+ )
			{
				// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:257:15: ^( DeclList ( decl )+ )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(DeclList, "DeclList"), root_1);
				if ( !(stream_decl.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_decl.hasNext() ) {
					adaptor.addChild(root_1, stream_decl.nextTree());
				}
				stream_decl.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "declList"


	public static class decl_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "decl"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:259:1: decl : ( varDecl | methodDecl );
	public final JavaliParser.decl_return decl() throws RecognitionException {
		JavaliParser.decl_return retval = new JavaliParser.decl_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope varDecl13 =null;
		ParserRuleReturnScope methodDecl14 =null;


		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:260:2: ( varDecl | methodDecl )
			int alt6=2;
			switch ( input.LA(1) ) {
			case 77:
			case 81:
			case 83:
				{
				int LA6_1 = input.LA(2);
				if ( (LA6_1==Identifier) ) {
					int LA6_4 = input.LA(3);
					if ( (LA6_4==64||LA6_4==68) ) {
						alt6=1;
					}
					else if ( (LA6_4==60) ) {
						alt6=2;
					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 6, 4, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}
				else if ( (LA6_1==75) ) {
					int LA6_5 = input.LA(3);
					if ( (LA6_5==76) ) {
						int LA6_8 = input.LA(4);
						if ( (LA6_8==Identifier) ) {
							int LA6_4 = input.LA(5);
							if ( (LA6_4==64||LA6_4==68) ) {
								alt6=1;
							}
							else if ( (LA6_4==60) ) {
								alt6=2;
							}

							else {
								int nvaeMark = input.mark();
								try {
									for (int nvaeConsume = 0; nvaeConsume < 5 - 1; nvaeConsume++) {
										input.consume();
									}
									NoViableAltException nvae =
										new NoViableAltException("", 6, 4, input);
									throw nvae;
								} finally {
									input.rewind(nvaeMark);
								}
							}

						}

						else {
							int nvaeMark = input.mark();
							try {
								for (int nvaeConsume = 0; nvaeConsume < 4 - 1; nvaeConsume++) {
									input.consume();
								}
								NoViableAltException nvae =
									new NoViableAltException("", 6, 8, input);
								throw nvae;
							} finally {
								input.rewind(nvaeMark);
							}
						}

					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 6, 5, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 6, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case Identifier:
				{
				int LA6_2 = input.LA(2);
				if ( (LA6_2==75) ) {
					int LA6_6 = input.LA(3);
					if ( (LA6_6==76) ) {
						int LA6_9 = input.LA(4);
						if ( (LA6_9==Identifier) ) {
							int LA6_4 = input.LA(5);
							if ( (LA6_4==64||LA6_4==68) ) {
								alt6=1;
							}
							else if ( (LA6_4==60) ) {
								alt6=2;
							}

							else {
								int nvaeMark = input.mark();
								try {
									for (int nvaeConsume = 0; nvaeConsume < 5 - 1; nvaeConsume++) {
										input.consume();
									}
									NoViableAltException nvae =
										new NoViableAltException("", 6, 4, input);
									throw nvae;
								} finally {
									input.rewind(nvaeMark);
								}
							}

						}

						else {
							int nvaeMark = input.mark();
							try {
								for (int nvaeConsume = 0; nvaeConsume < 4 - 1; nvaeConsume++) {
									input.consume();
								}
								NoViableAltException nvae =
									new NoViableAltException("", 6, 9, input);
								throw nvae;
							} finally {
								input.rewind(nvaeMark);
							}
						}

					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 6, 6, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}
				else if ( (LA6_2==Identifier) ) {
					int LA6_4 = input.LA(3);
					if ( (LA6_4==64||LA6_4==68) ) {
						alt6=1;
					}
					else if ( (LA6_4==60) ) {
						alt6=2;
					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 6, 4, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 6, 2, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case 90:
				{
				alt6=2;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 6, 0, input);
				throw nvae;
			}
			switch (alt6) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:260:4: varDecl
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_varDecl_in_decl316);
					varDecl13=varDecl();
					state._fsp--;

					adaptor.addChild(root_0, varDecl13.getTree());

					}
					break;
				case 2 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:260:14: methodDecl
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_methodDecl_in_decl320);
					methodDecl14=methodDecl();
					state._fsp--;

					adaptor.addChild(root_0, methodDecl14.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "decl"


	public static class varDecl_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "varDecl"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:263:1: varDecl : ( type Identifier ';' -> ^( TypeVal ^( Type type ) Identifier ) | type Identifier ( ',' Identifier )+ ';' -> ( ^( TypeVal ^( Type type ) Identifier ) )+ );
	public final JavaliParser.varDecl_return varDecl() throws RecognitionException {
		JavaliParser.varDecl_return retval = new JavaliParser.varDecl_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token Identifier16=null;
		Token char_literal17=null;
		Token Identifier19=null;
		Token char_literal20=null;
		Token Identifier21=null;
		Token char_literal22=null;
		ParserRuleReturnScope type15 =null;
		ParserRuleReturnScope type18 =null;

		Object Identifier16_tree=null;
		Object char_literal17_tree=null;
		Object Identifier19_tree=null;
		Object char_literal20_tree=null;
		Object Identifier21_tree=null;
		Object char_literal22_tree=null;
		RewriteRuleTokenStream stream_68=new RewriteRuleTokenStream(adaptor,"token 68");
		RewriteRuleTokenStream stream_64=new RewriteRuleTokenStream(adaptor,"token 64");
		RewriteRuleTokenStream stream_Identifier=new RewriteRuleTokenStream(adaptor,"token Identifier");
		RewriteRuleSubtreeStream stream_type=new RewriteRuleSubtreeStream(adaptor,"rule type");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:264:2: ( type Identifier ';' -> ^( TypeVal ^( Type type ) Identifier ) | type Identifier ( ',' Identifier )+ ';' -> ( ^( TypeVal ^( Type type ) Identifier ) )+ )
			int alt8=2;
			int LA8_0 = input.LA(1);
			if ( (LA8_0==77||LA8_0==81||LA8_0==83) ) {
				int LA8_1 = input.LA(2);
				if ( (LA8_1==Identifier) ) {
					int LA8_3 = input.LA(3);
					if ( (LA8_3==68) ) {
						alt8=1;
					}
					else if ( (LA8_3==64) ) {
						alt8=2;
					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 8, 3, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}
				else if ( (LA8_1==75) ) {
					int LA8_4 = input.LA(3);
					if ( (LA8_4==76) ) {
						int LA8_8 = input.LA(4);
						if ( (LA8_8==Identifier) ) {
							int LA8_3 = input.LA(5);
							if ( (LA8_3==68) ) {
								alt8=1;
							}
							else if ( (LA8_3==64) ) {
								alt8=2;
							}

							else {
								int nvaeMark = input.mark();
								try {
									for (int nvaeConsume = 0; nvaeConsume < 5 - 1; nvaeConsume++) {
										input.consume();
									}
									NoViableAltException nvae =
										new NoViableAltException("", 8, 3, input);
									throw nvae;
								} finally {
									input.rewind(nvaeMark);
								}
							}

						}

						else {
							int nvaeMark = input.mark();
							try {
								for (int nvaeConsume = 0; nvaeConsume < 4 - 1; nvaeConsume++) {
									input.consume();
								}
								NoViableAltException nvae =
									new NoViableAltException("", 8, 8, input);
								throw nvae;
							} finally {
								input.rewind(nvaeMark);
							}
						}

					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 8, 4, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 8, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}
			else if ( (LA8_0==Identifier) ) {
				int LA8_2 = input.LA(2);
				if ( (LA8_2==75) ) {
					int LA8_5 = input.LA(3);
					if ( (LA8_5==76) ) {
						int LA8_9 = input.LA(4);
						if ( (LA8_9==Identifier) ) {
							int LA8_3 = input.LA(5);
							if ( (LA8_3==68) ) {
								alt8=1;
							}
							else if ( (LA8_3==64) ) {
								alt8=2;
							}

							else {
								int nvaeMark = input.mark();
								try {
									for (int nvaeConsume = 0; nvaeConsume < 5 - 1; nvaeConsume++) {
										input.consume();
									}
									NoViableAltException nvae =
										new NoViableAltException("", 8, 3, input);
									throw nvae;
								} finally {
									input.rewind(nvaeMark);
								}
							}

						}

						else {
							int nvaeMark = input.mark();
							try {
								for (int nvaeConsume = 0; nvaeConsume < 4 - 1; nvaeConsume++) {
									input.consume();
								}
								NoViableAltException nvae =
									new NoViableAltException("", 8, 9, input);
								throw nvae;
							} finally {
								input.rewind(nvaeMark);
							}
						}

					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 8, 5, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}
				else if ( (LA8_2==Identifier) ) {
					int LA8_3 = input.LA(3);
					if ( (LA8_3==68) ) {
						alt8=1;
					}
					else if ( (LA8_3==64) ) {
						alt8=2;
					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 8, 3, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 8, 2, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 8, 0, input);
				throw nvae;
			}

			switch (alt8) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:264:4: type Identifier ';'
					{
					pushFollow(FOLLOW_type_in_varDecl331);
					type15=type();
					state._fsp--;

					stream_type.add(type15.getTree());
					Identifier16=(Token)match(input,Identifier,FOLLOW_Identifier_in_varDecl333);  
					stream_Identifier.add(Identifier16);

					char_literal17=(Token)match(input,68,FOLLOW_68_in_varDecl335);  
					stream_68.add(char_literal17);

					// AST REWRITE
					// elements: type, Identifier
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 264:25: -> ^( TypeVal ^( Type type ) Identifier )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:264:28: ^( TypeVal ^( Type type ) Identifier )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(TypeVal, "TypeVal"), root_1);
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:264:38: ^( Type type )
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(Type, "Type"), root_2);
						adaptor.addChild(root_2, stream_type.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_1, stream_Identifier.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:265:4: type Identifier ( ',' Identifier )+ ';'
					{
					pushFollow(FOLLOW_type_in_varDecl355);
					type18=type();
					state._fsp--;

					stream_type.add(type18.getTree());
					Identifier19=(Token)match(input,Identifier,FOLLOW_Identifier_in_varDecl357);  
					stream_Identifier.add(Identifier19);

					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:265:20: ( ',' Identifier )+
					int cnt7=0;
					loop7:
					while (true) {
						int alt7=2;
						int LA7_0 = input.LA(1);
						if ( (LA7_0==64) ) {
							alt7=1;
						}

						switch (alt7) {
						case 1 :
							// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:265:22: ',' Identifier
							{
							char_literal20=(Token)match(input,64,FOLLOW_64_in_varDecl361);  
							stream_64.add(char_literal20);

							Identifier21=(Token)match(input,Identifier,FOLLOW_Identifier_in_varDecl363);  
							stream_Identifier.add(Identifier21);

							}
							break;

						default :
							if ( cnt7 >= 1 ) break loop7;
							EarlyExitException eee = new EarlyExitException(7, input);
							throw eee;
						}
						cnt7++;
					}

					char_literal22=(Token)match(input,68,FOLLOW_68_in_varDecl368);  
					stream_68.add(char_literal22);

					// AST REWRITE
					// elements: type, Identifier
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 265:45: -> ( ^( TypeVal ^( Type type ) Identifier ) )+
					{
						if ( !(stream_type.hasNext()||stream_Identifier.hasNext()) ) {
							throw new RewriteEarlyExitException();
						}
						while ( stream_type.hasNext()||stream_Identifier.hasNext() ) {
							// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:265:48: ^( TypeVal ^( Type type ) Identifier )
							{
							Object root_1 = (Object)adaptor.nil();
							root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(TypeVal, "TypeVal"), root_1);
							// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:265:58: ^( Type type )
							{
							Object root_2 = (Object)adaptor.nil();
							root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(Type, "Type"), root_2);
							adaptor.addChild(root_2, stream_type.nextTree());
							adaptor.addChild(root_1, root_2);
							}

							adaptor.addChild(root_1, stream_Identifier.nextNode());
							adaptor.addChild(root_0, root_1);
							}

						}
						stream_type.reset();
						stream_Identifier.reset();

					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "varDecl"


	public static class methodDecl_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "methodDecl"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:268:1: methodDecl : methodHeading methodBody -> ^( MethodDecl methodHeading methodBody ) ;
	public final JavaliParser.methodDecl_return methodDecl() throws RecognitionException {
		JavaliParser.methodDecl_return retval = new JavaliParser.methodDecl_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope methodHeading23 =null;
		ParserRuleReturnScope methodBody24 =null;

		RewriteRuleSubtreeStream stream_methodBody=new RewriteRuleSubtreeStream(adaptor,"rule methodBody");
		RewriteRuleSubtreeStream stream_methodHeading=new RewriteRuleSubtreeStream(adaptor,"rule methodHeading");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:269:2: ( methodHeading methodBody -> ^( MethodDecl methodHeading methodBody ) )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:269:4: methodHeading methodBody
			{
			pushFollow(FOLLOW_methodHeading_in_methodDecl395);
			methodHeading23=methodHeading();
			state._fsp--;

			stream_methodHeading.add(methodHeading23.getTree());
			pushFollow(FOLLOW_methodBody_in_methodDecl397);
			methodBody24=methodBody();
			state._fsp--;

			stream_methodBody.add(methodBody24.getTree());
			// AST REWRITE
			// elements: methodHeading, methodBody
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 269:29: -> ^( MethodDecl methodHeading methodBody )
			{
				// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:269:32: ^( MethodDecl methodHeading methodBody )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(MethodDecl, "MethodDecl"), root_1);
				adaptor.addChild(root_1, stream_methodHeading.nextTree());
				adaptor.addChild(root_1, stream_methodBody.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "methodDecl"


	public static class methodHeading_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "methodHeading"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:272:1: methodHeading : ( type mn= Identifier '(' ( formalParamList )? ')' -> ^( MethodHead ^( MethodName $mn) ^( ReturnType type ) ^( FormalParams ( formalParamList )? ) ) | 'void' mnn= Identifier '(' ( formalParamList )? ')' -> ^( MethodHead ^( MethodName $mnn) ^( ReturnType 'void' ) ^( FormalParams ( formalParamList )? ) ) );
	public final JavaliParser.methodHeading_return methodHeading() throws RecognitionException {
		JavaliParser.methodHeading_return retval = new JavaliParser.methodHeading_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token mn=null;
		Token mnn=null;
		Token char_literal26=null;
		Token char_literal28=null;
		Token string_literal29=null;
		Token char_literal30=null;
		Token char_literal32=null;
		ParserRuleReturnScope type25 =null;
		ParserRuleReturnScope formalParamList27 =null;
		ParserRuleReturnScope formalParamList31 =null;

		Object mn_tree=null;
		Object mnn_tree=null;
		Object char_literal26_tree=null;
		Object char_literal28_tree=null;
		Object string_literal29_tree=null;
		Object char_literal30_tree=null;
		Object char_literal32_tree=null;
		RewriteRuleTokenStream stream_90=new RewriteRuleTokenStream(adaptor,"token 90");
		RewriteRuleTokenStream stream_Identifier=new RewriteRuleTokenStream(adaptor,"token Identifier");
		RewriteRuleTokenStream stream_60=new RewriteRuleTokenStream(adaptor,"token 60");
		RewriteRuleTokenStream stream_61=new RewriteRuleTokenStream(adaptor,"token 61");
		RewriteRuleSubtreeStream stream_formalParamList=new RewriteRuleSubtreeStream(adaptor,"rule formalParamList");
		RewriteRuleSubtreeStream stream_type=new RewriteRuleSubtreeStream(adaptor,"rule type");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:273:4: ( type mn= Identifier '(' ( formalParamList )? ')' -> ^( MethodHead ^( MethodName $mn) ^( ReturnType type ) ^( FormalParams ( formalParamList )? ) ) | 'void' mnn= Identifier '(' ( formalParamList )? ')' -> ^( MethodHead ^( MethodName $mnn) ^( ReturnType 'void' ) ^( FormalParams ( formalParamList )? ) ) )
			int alt11=2;
			int LA11_0 = input.LA(1);
			if ( (LA11_0==Identifier||LA11_0==77||LA11_0==81||LA11_0==83) ) {
				alt11=1;
			}
			else if ( (LA11_0==90) ) {
				alt11=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 11, 0, input);
				throw nvae;
			}

			switch (alt11) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:273:7: type mn= Identifier '(' ( formalParamList )? ')'
					{
					pushFollow(FOLLOW_type_in_methodHeading421);
					type25=type();
					state._fsp--;

					stream_type.add(type25.getTree());
					mn=(Token)match(input,Identifier,FOLLOW_Identifier_in_methodHeading425);  
					stream_Identifier.add(mn);

					char_literal26=(Token)match(input,60,FOLLOW_60_in_methodHeading427);  
					stream_60.add(char_literal26);

					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:273:30: ( formalParamList )?
					int alt9=2;
					int LA9_0 = input.LA(1);
					if ( (LA9_0==Identifier||LA9_0==77||LA9_0==81||LA9_0==83) ) {
						alt9=1;
					}
					switch (alt9) {
						case 1 :
							// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:273:30: formalParamList
							{
							pushFollow(FOLLOW_formalParamList_in_methodHeading429);
							formalParamList27=formalParamList();
							state._fsp--;

							stream_formalParamList.add(formalParamList27.getTree());
							}
							break;

					}

					char_literal28=(Token)match(input,61,FOLLOW_61_in_methodHeading432);  
					stream_61.add(char_literal28);

					// AST REWRITE
					// elements: mn, type, formalParamList
					// token labels: mn
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleTokenStream stream_mn=new RewriteRuleTokenStream(adaptor,"token mn",mn);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 273:51: -> ^( MethodHead ^( MethodName $mn) ^( ReturnType type ) ^( FormalParams ( formalParamList )? ) )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:273:54: ^( MethodHead ^( MethodName $mn) ^( ReturnType type ) ^( FormalParams ( formalParamList )? ) )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(MethodHead, "MethodHead"), root_1);
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:273:67: ^( MethodName $mn)
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(MethodName, "MethodName"), root_2);
						adaptor.addChild(root_2, stream_mn.nextNode());
						adaptor.addChild(root_1, root_2);
						}

						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:273:85: ^( ReturnType type )
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(ReturnType, "ReturnType"), root_2);
						adaptor.addChild(root_2, stream_type.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:273:104: ^( FormalParams ( formalParamList )? )
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(FormalParams, "FormalParams"), root_2);
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:273:119: ( formalParamList )?
						if ( stream_formalParamList.hasNext() ) {
							adaptor.addChild(root_2, stream_formalParamList.nextTree());
						}
						stream_formalParamList.reset();

						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:274:6: 'void' mnn= Identifier '(' ( formalParamList )? ')'
					{
					string_literal29=(Token)match(input,90,FOLLOW_90_in_methodHeading468);  
					stream_90.add(string_literal29);

					mnn=(Token)match(input,Identifier,FOLLOW_Identifier_in_methodHeading472);  
					stream_Identifier.add(mnn);

					char_literal30=(Token)match(input,60,FOLLOW_60_in_methodHeading474);  
					stream_60.add(char_literal30);

					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:274:32: ( formalParamList )?
					int alt10=2;
					int LA10_0 = input.LA(1);
					if ( (LA10_0==Identifier||LA10_0==77||LA10_0==81||LA10_0==83) ) {
						alt10=1;
					}
					switch (alt10) {
						case 1 :
							// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:274:32: formalParamList
							{
							pushFollow(FOLLOW_formalParamList_in_methodHeading476);
							formalParamList31=formalParamList();
							state._fsp--;

							stream_formalParamList.add(formalParamList31.getTree());
							}
							break;

					}

					char_literal32=(Token)match(input,61,FOLLOW_61_in_methodHeading479);  
					stream_61.add(char_literal32);

					// AST REWRITE
					// elements: formalParamList, 90, mnn
					// token labels: mnn
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleTokenStream stream_mnn=new RewriteRuleTokenStream(adaptor,"token mnn",mnn);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 274:53: -> ^( MethodHead ^( MethodName $mnn) ^( ReturnType 'void' ) ^( FormalParams ( formalParamList )? ) )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:274:56: ^( MethodHead ^( MethodName $mnn) ^( ReturnType 'void' ) ^( FormalParams ( formalParamList )? ) )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(MethodHead, "MethodHead"), root_1);
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:274:69: ^( MethodName $mnn)
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(MethodName, "MethodName"), root_2);
						adaptor.addChild(root_2, stream_mnn.nextNode());
						adaptor.addChild(root_1, root_2);
						}

						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:274:88: ^( ReturnType 'void' )
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(ReturnType, "ReturnType"), root_2);
						adaptor.addChild(root_2, stream_90.nextNode());
						adaptor.addChild(root_1, root_2);
						}

						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:274:109: ^( FormalParams ( formalParamList )? )
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(FormalParams, "FormalParams"), root_2);
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:274:124: ( formalParamList )?
						if ( stream_formalParamList.hasNext() ) {
							adaptor.addChild(root_2, stream_formalParamList.nextTree());
						}
						stream_formalParamList.reset();

						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "methodHeading"


	public static class formalParamList_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "formalParamList"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:277:1: formalParamList : type Identifier ( ',' type Identifier )* -> ( ^( Param type Identifier ) )+ ;
	public final JavaliParser.formalParamList_return formalParamList() throws RecognitionException {
		JavaliParser.formalParamList_return retval = new JavaliParser.formalParamList_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token Identifier34=null;
		Token char_literal35=null;
		Token Identifier37=null;
		ParserRuleReturnScope type33 =null;
		ParserRuleReturnScope type36 =null;

		Object Identifier34_tree=null;
		Object char_literal35_tree=null;
		Object Identifier37_tree=null;
		RewriteRuleTokenStream stream_64=new RewriteRuleTokenStream(adaptor,"token 64");
		RewriteRuleTokenStream stream_Identifier=new RewriteRuleTokenStream(adaptor,"token Identifier");
		RewriteRuleSubtreeStream stream_type=new RewriteRuleSubtreeStream(adaptor,"rule type");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:278:2: ( type Identifier ( ',' type Identifier )* -> ( ^( Param type Identifier ) )+ )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:278:4: type Identifier ( ',' type Identifier )*
			{
			pushFollow(FOLLOW_type_in_formalParamList521);
			type33=type();
			state._fsp--;

			stream_type.add(type33.getTree());
			Identifier34=(Token)match(input,Identifier,FOLLOW_Identifier_in_formalParamList523);  
			stream_Identifier.add(Identifier34);

			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:278:20: ( ',' type Identifier )*
			loop12:
			while (true) {
				int alt12=2;
				int LA12_0 = input.LA(1);
				if ( (LA12_0==64) ) {
					alt12=1;
				}

				switch (alt12) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:278:22: ',' type Identifier
					{
					char_literal35=(Token)match(input,64,FOLLOW_64_in_formalParamList527);  
					stream_64.add(char_literal35);

					pushFollow(FOLLOW_type_in_formalParamList529);
					type36=type();
					state._fsp--;

					stream_type.add(type36.getTree());
					Identifier37=(Token)match(input,Identifier,FOLLOW_Identifier_in_formalParamList531);  
					stream_Identifier.add(Identifier37);

					}
					break;

				default :
					break loop12;
				}
			}

			// AST REWRITE
			// elements: type, Identifier
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 278:45: -> ( ^( Param type Identifier ) )+
			{
				if ( !(stream_type.hasNext()||stream_Identifier.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_type.hasNext()||stream_Identifier.hasNext() ) {
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:278:48: ^( Param type Identifier )
					{
					Object root_1 = (Object)adaptor.nil();
					root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(Param, "Param"), root_1);
					adaptor.addChild(root_1, stream_type.nextTree());
					adaptor.addChild(root_1, stream_Identifier.nextNode());
					adaptor.addChild(root_0, root_1);
					}

				}
				stream_type.reset();
				stream_Identifier.reset();

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "formalParamList"


	public static class methodBody_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "methodBody"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:281:1: methodBody : ( methodBodyWithDeclList ( stmtList )? '}' -> ^( MethodBody ^( Seq methodBodyWithDeclList ) ^( Seq ( stmtList )? ) ) | '{' stmtList '}' -> ^( MethodBody Seq ^( Seq stmtList ) ) | '{' '}' -> ^( MethodBody Seq Seq ) );
	public final JavaliParser.methodBody_return methodBody() throws RecognitionException {
		JavaliParser.methodBody_return retval = new JavaliParser.methodBody_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal40=null;
		Token char_literal41=null;
		Token char_literal43=null;
		Token char_literal44=null;
		Token char_literal45=null;
		ParserRuleReturnScope methodBodyWithDeclList38 =null;
		ParserRuleReturnScope stmtList39 =null;
		ParserRuleReturnScope stmtList42 =null;

		Object char_literal40_tree=null;
		Object char_literal41_tree=null;
		Object char_literal43_tree=null;
		Object char_literal44_tree=null;
		Object char_literal45_tree=null;
		RewriteRuleTokenStream stream_97=new RewriteRuleTokenStream(adaptor,"token 97");
		RewriteRuleTokenStream stream_95=new RewriteRuleTokenStream(adaptor,"token 95");
		RewriteRuleSubtreeStream stream_stmtList=new RewriteRuleSubtreeStream(adaptor,"rule stmtList");
		RewriteRuleSubtreeStream stream_methodBodyWithDeclList=new RewriteRuleSubtreeStream(adaptor,"rule methodBodyWithDeclList");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:282:2: ( methodBodyWithDeclList ( stmtList )? '}' -> ^( MethodBody ^( Seq methodBodyWithDeclList ) ^( Seq ( stmtList )? ) ) | '{' stmtList '}' -> ^( MethodBody Seq ^( Seq stmtList ) ) | '{' '}' -> ^( MethodBody Seq Seq ) )
			int alt14=3;
			int LA14_0 = input.LA(1);
			if ( (LA14_0==95) ) {
				switch ( input.LA(2) ) {
				case 97:
					{
					alt14=3;
					}
					break;
				case 77:
				case 81:
				case 83:
				case 90:
					{
					alt14=1;
					}
					break;
				case Identifier:
					{
					switch ( input.LA(3) ) {
					case 75:
						{
						int LA14_6 = input.LA(4);
						if ( (LA14_6==76) ) {
							alt14=1;
						}
						else if ( (LA14_6==BooleanLiteral||LA14_6==DecimalNumber||LA14_6==FloatNumber||LA14_6==HexNumber||LA14_6==Identifier||LA14_6==56||LA14_6==60||LA14_6==63||LA14_6==65||LA14_6==85||LA14_6==89) ) {
							alt14=2;
						}

						else {
							int nvaeMark = input.mark();
							try {
								for (int nvaeConsume = 0; nvaeConsume < 4 - 1; nvaeConsume++) {
									input.consume();
								}
								NoViableAltException nvae =
									new NoViableAltException("", 14, 6, input);
								throw nvae;
							} finally {
								input.rewind(nvaeMark);
							}
						}

						}
						break;
					case Identifier:
						{
						alt14=1;
						}
						break;
					case 60:
					case 66:
					case 68:
					case 71:
						{
						alt14=2;
						}
						break;
					default:
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 14, 4, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}
					}
					break;
				case 82:
				case 88:
				case 89:
				case 91:
				case 92:
				case 93:
				case 94:
					{
					alt14=2;
					}
					break;
				default:
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 14, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 14, 0, input);
				throw nvae;
			}

			switch (alt14) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:282:4: methodBodyWithDeclList ( stmtList )? '}'
					{
					pushFollow(FOLLOW_methodBodyWithDeclList_in_methodBody556);
					methodBodyWithDeclList38=methodBodyWithDeclList();
					state._fsp--;

					stream_methodBodyWithDeclList.add(methodBodyWithDeclList38.getTree());
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:282:27: ( stmtList )?
					int alt13=2;
					int LA13_0 = input.LA(1);
					if ( (LA13_0==Identifier||LA13_0==82||(LA13_0 >= 88 && LA13_0 <= 89)||(LA13_0 >= 91 && LA13_0 <= 94)) ) {
						alt13=1;
					}
					switch (alt13) {
						case 1 :
							// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:282:28: stmtList
							{
							pushFollow(FOLLOW_stmtList_in_methodBody559);
							stmtList39=stmtList();
							state._fsp--;

							stream_stmtList.add(stmtList39.getTree());
							}
							break;

					}

					char_literal40=(Token)match(input,97,FOLLOW_97_in_methodBody563);  
					stream_97.add(char_literal40);

					// AST REWRITE
					// elements: methodBodyWithDeclList, stmtList
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 282:44: -> ^( MethodBody ^( Seq methodBodyWithDeclList ) ^( Seq ( stmtList )? ) )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:282:47: ^( MethodBody ^( Seq methodBodyWithDeclList ) ^( Seq ( stmtList )? ) )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(MethodBody, "MethodBody"), root_1);
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:282:60: ^( Seq methodBodyWithDeclList )
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(Seq, "Seq"), root_2);
						adaptor.addChild(root_2, stream_methodBodyWithDeclList.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:282:90: ^( Seq ( stmtList )? )
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(Seq, "Seq"), root_2);
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:282:96: ( stmtList )?
						if ( stream_stmtList.hasNext() ) {
							adaptor.addChild(root_2, stream_stmtList.nextTree());
						}
						stream_stmtList.reset();

						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:283:4: '{' stmtList '}'
					{
					char_literal41=(Token)match(input,95,FOLLOW_95_in_methodBody591);  
					stream_95.add(char_literal41);

					pushFollow(FOLLOW_stmtList_in_methodBody593);
					stmtList42=stmtList();
					state._fsp--;

					stream_stmtList.add(stmtList42.getTree());
					char_literal43=(Token)match(input,97,FOLLOW_97_in_methodBody595);  
					stream_97.add(char_literal43);

					// AST REWRITE
					// elements: stmtList
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 283:21: -> ^( MethodBody Seq ^( Seq stmtList ) )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:283:24: ^( MethodBody Seq ^( Seq stmtList ) )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(MethodBody, "MethodBody"), root_1);
						adaptor.addChild(root_1, (Object)adaptor.create(Seq, "Seq"));
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:283:41: ^( Seq stmtList )
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(Seq, "Seq"), root_2);
						adaptor.addChild(root_2, stream_stmtList.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 3 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:284:4: '{' '}'
					{
					char_literal44=(Token)match(input,95,FOLLOW_95_in_methodBody615);  
					stream_95.add(char_literal44);

					char_literal45=(Token)match(input,97,FOLLOW_97_in_methodBody618);  
					stream_97.add(char_literal45);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 284:13: -> ^( MethodBody Seq Seq )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:284:16: ^( MethodBody Seq Seq )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(MethodBody, "MethodBody"), root_1);
						adaptor.addChild(root_1, (Object)adaptor.create(Seq, "Seq"));
						adaptor.addChild(root_1, (Object)adaptor.create(Seq, "Seq"));
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "methodBody"


	public static class methodBodyWithDeclList_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "methodBodyWithDeclList"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:287:1: methodBodyWithDeclList : '{' declList -> declList ;
	public final JavaliParser.methodBodyWithDeclList_return methodBodyWithDeclList() throws RecognitionException {
		JavaliParser.methodBodyWithDeclList_return retval = new JavaliParser.methodBodyWithDeclList_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal46=null;
		ParserRuleReturnScope declList47 =null;

		Object char_literal46_tree=null;
		RewriteRuleTokenStream stream_95=new RewriteRuleTokenStream(adaptor,"token 95");
		RewriteRuleSubtreeStream stream_declList=new RewriteRuleSubtreeStream(adaptor,"rule declList");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:288:2: ( '{' declList -> declList )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:288:4: '{' declList
			{
			char_literal46=(Token)match(input,95,FOLLOW_95_in_methodBodyWithDeclList640);  
			stream_95.add(char_literal46);

			pushFollow(FOLLOW_declList_in_methodBodyWithDeclList642);
			declList47=declList();
			state._fsp--;

			stream_declList.add(declList47.getTree());
			// AST REWRITE
			// elements: declList
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 288:17: -> declList
			{
				adaptor.addChild(root_0, stream_declList.nextTree());
			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "methodBodyWithDeclList"


	public static class stmtList_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "stmtList"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:293:1: stmtList : ( stmt )+ ;
	public final JavaliParser.stmtList_return stmtList() throws RecognitionException {
		JavaliParser.stmtList_return retval = new JavaliParser.stmtList_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope stmt48 =null;


		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:294:2: ( ( stmt )+ )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:294:4: ( stmt )+
			{
			root_0 = (Object)adaptor.nil();


			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:294:4: ( stmt )+
			int cnt15=0;
			loop15:
			while (true) {
				int alt15=2;
				int LA15_0 = input.LA(1);
				if ( (LA15_0==Identifier||LA15_0==82||(LA15_0 >= 88 && LA15_0 <= 89)||(LA15_0 >= 91 && LA15_0 <= 94)) ) {
					alt15=1;
				}

				switch (alt15) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:294:4: stmt
					{
					pushFollow(FOLLOW_stmt_in_stmtList659);
					stmt48=stmt();
					state._fsp--;

					adaptor.addChild(root_0, stmt48.getTree());

					}
					break;

				default :
					if ( cnt15 >= 1 ) break loop15;
					EarlyExitException eee = new EarlyExitException(15, input);
					throw eee;
				}
				cnt15++;
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "stmtList"


	public static class stmt_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "stmt"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:296:1: stmt : ( assignmentOrMethodCall ';' -> assignmentOrMethodCall | ioStmt -> ioStmt | ifStmt -> ifStmt | whileStmt -> whileStmt | returnStmt -> returnStmt );
	public final JavaliParser.stmt_return stmt() throws RecognitionException {
		JavaliParser.stmt_return retval = new JavaliParser.stmt_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal50=null;
		ParserRuleReturnScope assignmentOrMethodCall49 =null;
		ParserRuleReturnScope ioStmt51 =null;
		ParserRuleReturnScope ifStmt52 =null;
		ParserRuleReturnScope whileStmt53 =null;
		ParserRuleReturnScope returnStmt54 =null;

		Object char_literal50_tree=null;
		RewriteRuleTokenStream stream_68=new RewriteRuleTokenStream(adaptor,"token 68");
		RewriteRuleSubtreeStream stream_whileStmt=new RewriteRuleSubtreeStream(adaptor,"rule whileStmt");
		RewriteRuleSubtreeStream stream_assignmentOrMethodCall=new RewriteRuleSubtreeStream(adaptor,"rule assignmentOrMethodCall");
		RewriteRuleSubtreeStream stream_returnStmt=new RewriteRuleSubtreeStream(adaptor,"rule returnStmt");
		RewriteRuleSubtreeStream stream_ifStmt=new RewriteRuleSubtreeStream(adaptor,"rule ifStmt");
		RewriteRuleSubtreeStream stream_ioStmt=new RewriteRuleSubtreeStream(adaptor,"rule ioStmt");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:297:2: ( assignmentOrMethodCall ';' -> assignmentOrMethodCall | ioStmt -> ioStmt | ifStmt -> ifStmt | whileStmt -> whileStmt | returnStmt -> returnStmt )
			int alt16=5;
			switch ( input.LA(1) ) {
			case Identifier:
			case 89:
				{
				alt16=1;
				}
				break;
			case 92:
			case 93:
			case 94:
				{
				alt16=2;
				}
				break;
			case 82:
				{
				alt16=3;
				}
				break;
			case 91:
				{
				alt16=4;
				}
				break;
			case 88:
				{
				alt16=5;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 16, 0, input);
				throw nvae;
			}
			switch (alt16) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:297:4: assignmentOrMethodCall ';'
					{
					pushFollow(FOLLOW_assignmentOrMethodCall_in_stmt670);
					assignmentOrMethodCall49=assignmentOrMethodCall();
					state._fsp--;

					stream_assignmentOrMethodCall.add(assignmentOrMethodCall49.getTree());
					char_literal50=(Token)match(input,68,FOLLOW_68_in_stmt672);  
					stream_68.add(char_literal50);

					// AST REWRITE
					// elements: assignmentOrMethodCall
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 297:31: -> assignmentOrMethodCall
					{
						adaptor.addChild(root_0, stream_assignmentOrMethodCall.nextTree());
					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:298:7: ioStmt
					{
					pushFollow(FOLLOW_ioStmt_in_stmt684);
					ioStmt51=ioStmt();
					state._fsp--;

					stream_ioStmt.add(ioStmt51.getTree());
					// AST REWRITE
					// elements: ioStmt
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 298:14: -> ioStmt
					{
						adaptor.addChild(root_0, stream_ioStmt.nextTree());
					}


					retval.tree = root_0;

					}
					break;
				case 3 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:299:4: ifStmt
					{
					pushFollow(FOLLOW_ifStmt_in_stmt693);
					ifStmt52=ifStmt();
					state._fsp--;

					stream_ifStmt.add(ifStmt52.getTree());
					// AST REWRITE
					// elements: ifStmt
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 299:11: -> ifStmt
					{
						adaptor.addChild(root_0, stream_ifStmt.nextTree());
					}


					retval.tree = root_0;

					}
					break;
				case 4 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:300:4: whileStmt
					{
					pushFollow(FOLLOW_whileStmt_in_stmt702);
					whileStmt53=whileStmt();
					state._fsp--;

					stream_whileStmt.add(whileStmt53.getTree());
					// AST REWRITE
					// elements: whileStmt
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 300:14: -> whileStmt
					{
						adaptor.addChild(root_0, stream_whileStmt.nextTree());
					}


					retval.tree = root_0;

					}
					break;
				case 5 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:301:5: returnStmt
					{
					pushFollow(FOLLOW_returnStmt_in_stmt711);
					returnStmt54=returnStmt();
					state._fsp--;

					stream_returnStmt.add(returnStmt54.getTree());
					// AST REWRITE
					// elements: returnStmt
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 301:16: -> returnStmt
					{
						adaptor.addChild(root_0, stream_returnStmt.nextTree());
					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "stmt"


	public static class assignmentOrMethodCall_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "assignmentOrMethodCall"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:304:1: assignmentOrMethodCall : identAccess ( assignmentTail -> ^( Assignment identAccess assignmentTail ) | -> ^( MethodCall identAccess ) ) ;
	public final JavaliParser.assignmentOrMethodCall_return assignmentOrMethodCall() throws RecognitionException {
		JavaliParser.assignmentOrMethodCall_return retval = new JavaliParser.assignmentOrMethodCall_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope identAccess55 =null;
		ParserRuleReturnScope assignmentTail56 =null;

		RewriteRuleSubtreeStream stream_identAccess=new RewriteRuleSubtreeStream(adaptor,"rule identAccess");
		RewriteRuleSubtreeStream stream_assignmentTail=new RewriteRuleSubtreeStream(adaptor,"rule assignmentTail");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:305:4: ( identAccess ( assignmentTail -> ^( Assignment identAccess assignmentTail ) | -> ^( MethodCall identAccess ) ) )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:305:7: identAccess ( assignmentTail -> ^( Assignment identAccess assignmentTail ) | -> ^( MethodCall identAccess ) )
			{
			pushFollow(FOLLOW_identAccess_in_assignmentOrMethodCall729);
			identAccess55=identAccess();
			state._fsp--;

			stream_identAccess.add(identAccess55.getTree());
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:306:7: ( assignmentTail -> ^( Assignment identAccess assignmentTail ) | -> ^( MethodCall identAccess ) )
			int alt17=2;
			int LA17_0 = input.LA(1);
			if ( (LA17_0==71) ) {
				alt17=1;
			}
			else if ( (LA17_0==68) ) {
				alt17=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 17, 0, input);
				throw nvae;
			}

			switch (alt17) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:306:10: assignmentTail
					{
					pushFollow(FOLLOW_assignmentTail_in_assignmentOrMethodCall740);
					assignmentTail56=assignmentTail();
					state._fsp--;

					stream_assignmentTail.add(assignmentTail56.getTree());
					// AST REWRITE
					// elements: identAccess, assignmentTail
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 306:25: -> ^( Assignment identAccess assignmentTail )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:306:27: ^( Assignment identAccess assignmentTail )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(Assignment, "Assignment"), root_1);
						adaptor.addChild(root_1, stream_identAccess.nextTree());
						adaptor.addChild(root_1, stream_assignmentTail.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:307:8: 
					{
					// AST REWRITE
					// elements: identAccess
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 307:8: -> ^( MethodCall identAccess )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:307:10: ^( MethodCall identAccess )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(MethodCall, "MethodCall"), root_1);
						adaptor.addChild(root_1, stream_identAccess.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "assignmentOrMethodCall"


	public static class assignmentTail_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "assignmentTail"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:311:1: assignmentTail : '=' assignmentRHS -> assignmentRHS ;
	public final JavaliParser.assignmentTail_return assignmentTail() throws RecognitionException {
		JavaliParser.assignmentTail_return retval = new JavaliParser.assignmentTail_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal57=null;
		ParserRuleReturnScope assignmentRHS58 =null;

		Object char_literal57_tree=null;
		RewriteRuleTokenStream stream_71=new RewriteRuleTokenStream(adaptor,"token 71");
		RewriteRuleSubtreeStream stream_assignmentRHS=new RewriteRuleSubtreeStream(adaptor,"rule assignmentRHS");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:312:2: ( '=' assignmentRHS -> assignmentRHS )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:312:4: '=' assignmentRHS
			{
			char_literal57=(Token)match(input,71,FOLLOW_71_in_assignmentTail784);  
			stream_71.add(char_literal57);

			pushFollow(FOLLOW_assignmentRHS_in_assignmentTail786);
			assignmentRHS58=assignmentRHS();
			state._fsp--;

			stream_assignmentRHS.add(assignmentRHS58.getTree());
			// AST REWRITE
			// elements: assignmentRHS
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 312:22: -> assignmentRHS
			{
				adaptor.addChild(root_0, stream_assignmentRHS.nextTree());
			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "assignmentTail"


	public static class assignmentRHS_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "assignmentRHS"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:315:1: assignmentRHS : ( expr | newExpr | readExpr | readExprFloat ) ;
	public final JavaliParser.assignmentRHS_return assignmentRHS() throws RecognitionException {
		JavaliParser.assignmentRHS_return retval = new JavaliParser.assignmentRHS_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope expr59 =null;
		ParserRuleReturnScope newExpr60 =null;
		ParserRuleReturnScope readExpr61 =null;
		ParserRuleReturnScope readExprFloat62 =null;


		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:316:2: ( ( expr | newExpr | readExpr | readExprFloat ) )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:316:4: ( expr | newExpr | readExpr | readExprFloat )
			{
			root_0 = (Object)adaptor.nil();


			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:316:4: ( expr | newExpr | readExpr | readExprFloat )
			int alt18=4;
			switch ( input.LA(1) ) {
			case BooleanLiteral:
			case DecimalNumber:
			case FloatNumber:
			case HexNumber:
			case Identifier:
			case 56:
			case 60:
			case 63:
			case 65:
			case 85:
			case 89:
				{
				alt18=1;
				}
				break;
			case 84:
				{
				alt18=2;
				}
				break;
			case 86:
				{
				alt18=3;
				}
				break;
			case 87:
				{
				alt18=4;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 18, 0, input);
				throw nvae;
			}
			switch (alt18) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:316:6: expr
					{
					pushFollow(FOLLOW_expr_in_assignmentRHS802);
					expr59=expr();
					state._fsp--;

					adaptor.addChild(root_0, expr59.getTree());

					}
					break;
				case 2 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:316:13: newExpr
					{
					pushFollow(FOLLOW_newExpr_in_assignmentRHS806);
					newExpr60=newExpr();
					state._fsp--;

					adaptor.addChild(root_0, newExpr60.getTree());

					}
					break;
				case 3 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:316:23: readExpr
					{
					pushFollow(FOLLOW_readExpr_in_assignmentRHS810);
					readExpr61=readExpr();
					state._fsp--;

					adaptor.addChild(root_0, readExpr61.getTree());

					}
					break;
				case 4 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:316:34: readExprFloat
					{
					pushFollow(FOLLOW_readExprFloat_in_assignmentRHS814);
					readExprFloat62=readExprFloat();
					state._fsp--;

					adaptor.addChild(root_0, readExprFloat62.getTree());

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "assignmentRHS"


	public static class methodCallTail_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "methodCallTail"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:319:1: methodCallTail : '(' ( actualParamList )? ')' -> ^( MethodCallTail ( actualParamList )? ) ;
	public final JavaliParser.methodCallTail_return methodCallTail() throws RecognitionException {
		JavaliParser.methodCallTail_return retval = new JavaliParser.methodCallTail_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal63=null;
		Token char_literal65=null;
		ParserRuleReturnScope actualParamList64 =null;

		Object char_literal63_tree=null;
		Object char_literal65_tree=null;
		RewriteRuleTokenStream stream_60=new RewriteRuleTokenStream(adaptor,"token 60");
		RewriteRuleTokenStream stream_61=new RewriteRuleTokenStream(adaptor,"token 61");
		RewriteRuleSubtreeStream stream_actualParamList=new RewriteRuleSubtreeStream(adaptor,"rule actualParamList");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:320:2: ( '(' ( actualParamList )? ')' -> ^( MethodCallTail ( actualParamList )? ) )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:320:4: '(' ( actualParamList )? ')'
			{
			char_literal63=(Token)match(input,60,FOLLOW_60_in_methodCallTail827);  
			stream_60.add(char_literal63);

			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:320:8: ( actualParamList )?
			int alt19=2;
			int LA19_0 = input.LA(1);
			if ( (LA19_0==BooleanLiteral||LA19_0==DecimalNumber||LA19_0==FloatNumber||LA19_0==HexNumber||LA19_0==Identifier||LA19_0==56||LA19_0==60||LA19_0==63||LA19_0==65||LA19_0==85||LA19_0==89) ) {
				alt19=1;
			}
			switch (alt19) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:320:8: actualParamList
					{
					pushFollow(FOLLOW_actualParamList_in_methodCallTail829);
					actualParamList64=actualParamList();
					state._fsp--;

					stream_actualParamList.add(actualParamList64.getTree());
					}
					break;

			}

			char_literal65=(Token)match(input,61,FOLLOW_61_in_methodCallTail832);  
			stream_61.add(char_literal65);

			// AST REWRITE
			// elements: actualParamList
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 320:29: -> ^( MethodCallTail ( actualParamList )? )
			{
				// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:320:32: ^( MethodCallTail ( actualParamList )? )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(MethodCallTail, "MethodCallTail"), root_1);
				// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:320:49: ( actualParamList )?
				if ( stream_actualParamList.hasNext() ) {
					adaptor.addChild(root_1, stream_actualParamList.nextTree());
				}
				stream_actualParamList.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "methodCallTail"


	public static class actualParamList_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "actualParamList"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:323:1: actualParamList : expr ( ',' expr )* -> ^( ActualParamList ( expr )+ ) ;
	public final JavaliParser.actualParamList_return actualParamList() throws RecognitionException {
		JavaliParser.actualParamList_return retval = new JavaliParser.actualParamList_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal67=null;
		ParserRuleReturnScope expr66 =null;
		ParserRuleReturnScope expr68 =null;

		Object char_literal67_tree=null;
		RewriteRuleTokenStream stream_64=new RewriteRuleTokenStream(adaptor,"token 64");
		RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:324:2: ( expr ( ',' expr )* -> ^( ActualParamList ( expr )+ ) )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:324:4: expr ( ',' expr )*
			{
			pushFollow(FOLLOW_expr_in_actualParamList852);
			expr66=expr();
			state._fsp--;

			stream_expr.add(expr66.getTree());
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:324:9: ( ',' expr )*
			loop20:
			while (true) {
				int alt20=2;
				int LA20_0 = input.LA(1);
				if ( (LA20_0==64) ) {
					alt20=1;
				}

				switch (alt20) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:324:11: ',' expr
					{
					char_literal67=(Token)match(input,64,FOLLOW_64_in_actualParamList856);  
					stream_64.add(char_literal67);

					pushFollow(FOLLOW_expr_in_actualParamList858);
					expr68=expr();
					state._fsp--;

					stream_expr.add(expr68.getTree());
					}
					break;

				default :
					break loop20;
				}
			}

			// AST REWRITE
			// elements: expr
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 324:23: -> ^( ActualParamList ( expr )+ )
			{
				// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:324:26: ^( ActualParamList ( expr )+ )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ActualParamList, "ActualParamList"), root_1);
				if ( !(stream_expr.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_expr.hasNext() ) {
					adaptor.addChild(root_1, stream_expr.nextTree());
				}
				stream_expr.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "actualParamList"


	public static class ioStmt_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "ioStmt"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:327:1: ioStmt : ( 'write' '(' expr ')' ';' -> ^( BuiltInWrite expr ) | 'writef' '(' expr ')' ';' -> ^( BuiltInWriteFloat expr ) | 'writeln' '(' ')' ';' -> ^( BuiltInWriteln ) );
	public final JavaliParser.ioStmt_return ioStmt() throws RecognitionException {
		JavaliParser.ioStmt_return retval = new JavaliParser.ioStmt_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal69=null;
		Token char_literal70=null;
		Token char_literal72=null;
		Token char_literal73=null;
		Token string_literal74=null;
		Token char_literal75=null;
		Token char_literal77=null;
		Token char_literal78=null;
		Token string_literal79=null;
		Token char_literal80=null;
		Token char_literal81=null;
		Token char_literal82=null;
		ParserRuleReturnScope expr71 =null;
		ParserRuleReturnScope expr76 =null;

		Object string_literal69_tree=null;
		Object char_literal70_tree=null;
		Object char_literal72_tree=null;
		Object char_literal73_tree=null;
		Object string_literal74_tree=null;
		Object char_literal75_tree=null;
		Object char_literal77_tree=null;
		Object char_literal78_tree=null;
		Object string_literal79_tree=null;
		Object char_literal80_tree=null;
		Object char_literal81_tree=null;
		Object char_literal82_tree=null;
		RewriteRuleTokenStream stream_68=new RewriteRuleTokenStream(adaptor,"token 68");
		RewriteRuleTokenStream stream_94=new RewriteRuleTokenStream(adaptor,"token 94");
		RewriteRuleTokenStream stream_93=new RewriteRuleTokenStream(adaptor,"token 93");
		RewriteRuleTokenStream stream_92=new RewriteRuleTokenStream(adaptor,"token 92");
		RewriteRuleTokenStream stream_60=new RewriteRuleTokenStream(adaptor,"token 60");
		RewriteRuleTokenStream stream_61=new RewriteRuleTokenStream(adaptor,"token 61");
		RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:328:2: ( 'write' '(' expr ')' ';' -> ^( BuiltInWrite expr ) | 'writef' '(' expr ')' ';' -> ^( BuiltInWriteFloat expr ) | 'writeln' '(' ')' ';' -> ^( BuiltInWriteln ) )
			int alt21=3;
			switch ( input.LA(1) ) {
			case 92:
				{
				alt21=1;
				}
				break;
			case 93:
				{
				alt21=2;
				}
				break;
			case 94:
				{
				alt21=3;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 21, 0, input);
				throw nvae;
			}
			switch (alt21) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:328:4: 'write' '(' expr ')' ';'
					{
					string_literal69=(Token)match(input,92,FOLLOW_92_in_ioStmt881);  
					stream_92.add(string_literal69);

					char_literal70=(Token)match(input,60,FOLLOW_60_in_ioStmt883);  
					stream_60.add(char_literal70);

					pushFollow(FOLLOW_expr_in_ioStmt885);
					expr71=expr();
					state._fsp--;

					stream_expr.add(expr71.getTree());
					char_literal72=(Token)match(input,61,FOLLOW_61_in_ioStmt887);  
					stream_61.add(char_literal72);

					char_literal73=(Token)match(input,68,FOLLOW_68_in_ioStmt889);  
					stream_68.add(char_literal73);

					// AST REWRITE
					// elements: expr
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 328:29: -> ^( BuiltInWrite expr )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:328:32: ^( BuiltInWrite expr )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(BuiltInWrite, "BuiltInWrite"), root_1);
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:329:6: 'writef' '(' expr ')' ';'
					{
					string_literal74=(Token)match(input,93,FOLLOW_93_in_ioStmt904);  
					stream_93.add(string_literal74);

					char_literal75=(Token)match(input,60,FOLLOW_60_in_ioStmt906);  
					stream_60.add(char_literal75);

					pushFollow(FOLLOW_expr_in_ioStmt908);
					expr76=expr();
					state._fsp--;

					stream_expr.add(expr76.getTree());
					char_literal77=(Token)match(input,61,FOLLOW_61_in_ioStmt910);  
					stream_61.add(char_literal77);

					char_literal78=(Token)match(input,68,FOLLOW_68_in_ioStmt912);  
					stream_68.add(char_literal78);

					// AST REWRITE
					// elements: expr
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 329:32: -> ^( BuiltInWriteFloat expr )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:329:35: ^( BuiltInWriteFloat expr )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(BuiltInWriteFloat, "BuiltInWriteFloat"), root_1);
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 3 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:330:6: 'writeln' '(' ')' ';'
					{
					string_literal79=(Token)match(input,94,FOLLOW_94_in_ioStmt927);  
					stream_94.add(string_literal79);

					char_literal80=(Token)match(input,60,FOLLOW_60_in_ioStmt929);  
					stream_60.add(char_literal80);

					char_literal81=(Token)match(input,61,FOLLOW_61_in_ioStmt931);  
					stream_61.add(char_literal81);

					char_literal82=(Token)match(input,68,FOLLOW_68_in_ioStmt933);  
					stream_68.add(char_literal82);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 330:28: -> ^( BuiltInWriteln )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:330:31: ^( BuiltInWriteln )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(BuiltInWriteln, "BuiltInWriteln"), root_1);
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "ioStmt"


	public static class ifStmt_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "ifStmt"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:333:1: ifStmt : 'if' '(' ex= expr ')' s1= stmtBlock ( -> ^( IfElse $ex $s1) | 'else' s2= stmtBlock -> ^( IfElse $ex $s1 $s2) ) ;
	public final JavaliParser.ifStmt_return ifStmt() throws RecognitionException {
		JavaliParser.ifStmt_return retval = new JavaliParser.ifStmt_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal83=null;
		Token char_literal84=null;
		Token char_literal85=null;
		Token string_literal86=null;
		ParserRuleReturnScope ex =null;
		ParserRuleReturnScope s1 =null;
		ParserRuleReturnScope s2 =null;

		Object string_literal83_tree=null;
		Object char_literal84_tree=null;
		Object char_literal85_tree=null;
		Object string_literal86_tree=null;
		RewriteRuleTokenStream stream_79=new RewriteRuleTokenStream(adaptor,"token 79");
		RewriteRuleTokenStream stream_82=new RewriteRuleTokenStream(adaptor,"token 82");
		RewriteRuleTokenStream stream_60=new RewriteRuleTokenStream(adaptor,"token 60");
		RewriteRuleTokenStream stream_61=new RewriteRuleTokenStream(adaptor,"token 61");
		RewriteRuleSubtreeStream stream_stmtBlock=new RewriteRuleSubtreeStream(adaptor,"rule stmtBlock");
		RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:334:2: ( 'if' '(' ex= expr ')' s1= stmtBlock ( -> ^( IfElse $ex $s1) | 'else' s2= stmtBlock -> ^( IfElse $ex $s1 $s2) ) )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:334:4: 'if' '(' ex= expr ')' s1= stmtBlock ( -> ^( IfElse $ex $s1) | 'else' s2= stmtBlock -> ^( IfElse $ex $s1 $s2) )
			{
			string_literal83=(Token)match(input,82,FOLLOW_82_in_ifStmt953);  
			stream_82.add(string_literal83);

			char_literal84=(Token)match(input,60,FOLLOW_60_in_ifStmt955);  
			stream_60.add(char_literal84);

			pushFollow(FOLLOW_expr_in_ifStmt959);
			ex=expr();
			state._fsp--;

			stream_expr.add(ex.getTree());
			char_literal85=(Token)match(input,61,FOLLOW_61_in_ifStmt961);  
			stream_61.add(char_literal85);

			pushFollow(FOLLOW_stmtBlock_in_ifStmt965);
			s1=stmtBlock();
			state._fsp--;

			stream_stmtBlock.add(s1.getTree());
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:335:3: ( -> ^( IfElse $ex $s1) | 'else' s2= stmtBlock -> ^( IfElse $ex $s1 $s2) )
			int alt22=2;
			int LA22_0 = input.LA(1);
			if ( (LA22_0==Identifier||LA22_0==82||(LA22_0 >= 88 && LA22_0 <= 89)||(LA22_0 >= 91 && LA22_0 <= 94)||LA22_0==97) ) {
				alt22=1;
			}
			else if ( (LA22_0==79) ) {
				alt22=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 22, 0, input);
				throw nvae;
			}

			switch (alt22) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:335:5: 
					{
					// AST REWRITE
					// elements: ex, s1
					// token labels: 
					// rule labels: ex, retval, s1
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_ex=new RewriteRuleSubtreeStream(adaptor,"rule ex",ex!=null?ex.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_s1=new RewriteRuleSubtreeStream(adaptor,"rule s1",s1!=null?s1.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 335:5: -> ^( IfElse $ex $s1)
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:335:8: ^( IfElse $ex $s1)
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(IfElse, "IfElse"), root_1);
						adaptor.addChild(root_1, stream_ex.nextTree());
						adaptor.addChild(root_1, stream_s1.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:336:5: 'else' s2= stmtBlock
					{
					string_literal86=(Token)match(input,79,FOLLOW_79_in_ifStmt987);  
					stream_79.add(string_literal86);

					pushFollow(FOLLOW_stmtBlock_in_ifStmt991);
					s2=stmtBlock();
					state._fsp--;

					stream_stmtBlock.add(s2.getTree());
					// AST REWRITE
					// elements: s1, ex, s2
					// token labels: 
					// rule labels: ex, retval, s2, s1
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_ex=new RewriteRuleSubtreeStream(adaptor,"rule ex",ex!=null?ex.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_s2=new RewriteRuleSubtreeStream(adaptor,"rule s2",s2!=null?s2.getTree():null);
					RewriteRuleSubtreeStream stream_s1=new RewriteRuleSubtreeStream(adaptor,"rule s1",s1!=null?s1.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 336:25: -> ^( IfElse $ex $s1 $s2)
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:336:28: ^( IfElse $ex $s1 $s2)
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(IfElse, "IfElse"), root_1);
						adaptor.addChild(root_1, stream_ex.nextTree());
						adaptor.addChild(root_1, stream_s1.nextTree());
						adaptor.addChild(root_1, stream_s2.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "ifStmt"


	public static class whileStmt_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "whileStmt"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:340:1: whileStmt : 'while' '(' expr ')' stmtBlock -> ^( WhileStmt expr stmtBlock ) ;
	public final JavaliParser.whileStmt_return whileStmt() throws RecognitionException {
		JavaliParser.whileStmt_return retval = new JavaliParser.whileStmt_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal87=null;
		Token char_literal88=null;
		Token char_literal90=null;
		ParserRuleReturnScope expr89 =null;
		ParserRuleReturnScope stmtBlock91 =null;

		Object string_literal87_tree=null;
		Object char_literal88_tree=null;
		Object char_literal90_tree=null;
		RewriteRuleTokenStream stream_91=new RewriteRuleTokenStream(adaptor,"token 91");
		RewriteRuleTokenStream stream_60=new RewriteRuleTokenStream(adaptor,"token 60");
		RewriteRuleTokenStream stream_61=new RewriteRuleTokenStream(adaptor,"token 61");
		RewriteRuleSubtreeStream stream_stmtBlock=new RewriteRuleSubtreeStream(adaptor,"rule stmtBlock");
		RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:341:2: ( 'while' '(' expr ')' stmtBlock -> ^( WhileStmt expr stmtBlock ) )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:341:4: 'while' '(' expr ')' stmtBlock
			{
			string_literal87=(Token)match(input,91,FOLLOW_91_in_whileStmt1021);  
			stream_91.add(string_literal87);

			char_literal88=(Token)match(input,60,FOLLOW_60_in_whileStmt1023);  
			stream_60.add(char_literal88);

			pushFollow(FOLLOW_expr_in_whileStmt1025);
			expr89=expr();
			state._fsp--;

			stream_expr.add(expr89.getTree());
			char_literal90=(Token)match(input,61,FOLLOW_61_in_whileStmt1027);  
			stream_61.add(char_literal90);

			pushFollow(FOLLOW_stmtBlock_in_whileStmt1029);
			stmtBlock91=stmtBlock();
			state._fsp--;

			stream_stmtBlock.add(stmtBlock91.getTree());
			// AST REWRITE
			// elements: expr, stmtBlock
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 341:35: -> ^( WhileStmt expr stmtBlock )
			{
				// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:341:38: ^( WhileStmt expr stmtBlock )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(WhileStmt, "WhileStmt"), root_1);
				adaptor.addChild(root_1, stream_expr.nextTree());
				adaptor.addChild(root_1, stream_stmtBlock.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "whileStmt"


	public static class returnStmt_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "returnStmt"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:344:1: returnStmt : 'return' ( expr )? ';' -> ^( ReturnStmt ( expr )? ) ;
	public final JavaliParser.returnStmt_return returnStmt() throws RecognitionException {
		JavaliParser.returnStmt_return retval = new JavaliParser.returnStmt_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal92=null;
		Token char_literal94=null;
		ParserRuleReturnScope expr93 =null;

		Object string_literal92_tree=null;
		Object char_literal94_tree=null;
		RewriteRuleTokenStream stream_68=new RewriteRuleTokenStream(adaptor,"token 68");
		RewriteRuleTokenStream stream_88=new RewriteRuleTokenStream(adaptor,"token 88");
		RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:345:2: ( 'return' ( expr )? ';' -> ^( ReturnStmt ( expr )? ) )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:345:5: 'return' ( expr )? ';'
			{
			string_literal92=(Token)match(input,88,FOLLOW_88_in_returnStmt1051);  
			stream_88.add(string_literal92);

			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:345:14: ( expr )?
			int alt23=2;
			int LA23_0 = input.LA(1);
			if ( (LA23_0==BooleanLiteral||LA23_0==DecimalNumber||LA23_0==FloatNumber||LA23_0==HexNumber||LA23_0==Identifier||LA23_0==56||LA23_0==60||LA23_0==63||LA23_0==65||LA23_0==85||LA23_0==89) ) {
				alt23=1;
			}
			switch (alt23) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:345:14: expr
					{
					pushFollow(FOLLOW_expr_in_returnStmt1053);
					expr93=expr();
					state._fsp--;

					stream_expr.add(expr93.getTree());
					}
					break;

			}

			char_literal94=(Token)match(input,68,FOLLOW_68_in_returnStmt1056);  
			stream_68.add(char_literal94);

			// AST REWRITE
			// elements: expr
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 345:24: -> ^( ReturnStmt ( expr )? )
			{
				// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:345:27: ^( ReturnStmt ( expr )? )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ReturnStmt, "ReturnStmt"), root_1);
				// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:345:40: ( expr )?
				if ( stream_expr.hasNext() ) {
					adaptor.addChild(root_1, stream_expr.nextTree());
				}
				stream_expr.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "returnStmt"


	public static class stmtBlock_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "stmtBlock"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:348:1: stmtBlock : '{' ( stmtList )? '}' -> ^( StmtBlock ( stmtList )? ) ;
	public final JavaliParser.stmtBlock_return stmtBlock() throws RecognitionException {
		JavaliParser.stmtBlock_return retval = new JavaliParser.stmtBlock_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal95=null;
		Token char_literal97=null;
		ParserRuleReturnScope stmtList96 =null;

		Object char_literal95_tree=null;
		Object char_literal97_tree=null;
		RewriteRuleTokenStream stream_97=new RewriteRuleTokenStream(adaptor,"token 97");
		RewriteRuleTokenStream stream_95=new RewriteRuleTokenStream(adaptor,"token 95");
		RewriteRuleSubtreeStream stream_stmtList=new RewriteRuleSubtreeStream(adaptor,"rule stmtList");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:349:2: ( '{' ( stmtList )? '}' -> ^( StmtBlock ( stmtList )? ) )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:349:4: '{' ( stmtList )? '}'
			{
			char_literal95=(Token)match(input,95,FOLLOW_95_in_stmtBlock1078);  
			stream_95.add(char_literal95);

			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:349:8: ( stmtList )?
			int alt24=2;
			int LA24_0 = input.LA(1);
			if ( (LA24_0==Identifier||LA24_0==82||(LA24_0 >= 88 && LA24_0 <= 89)||(LA24_0 >= 91 && LA24_0 <= 94)) ) {
				alt24=1;
			}
			switch (alt24) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:349:8: stmtList
					{
					pushFollow(FOLLOW_stmtList_in_stmtBlock1080);
					stmtList96=stmtList();
					state._fsp--;

					stream_stmtList.add(stmtList96.getTree());
					}
					break;

			}

			char_literal97=(Token)match(input,97,FOLLOW_97_in_stmtBlock1083);  
			stream_97.add(char_literal97);

			// AST REWRITE
			// elements: stmtList
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 349:22: -> ^( StmtBlock ( stmtList )? )
			{
				// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:349:25: ^( StmtBlock ( stmtList )? )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(StmtBlock, "StmtBlock"), root_1);
				// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:349:37: ( stmtList )?
				if ( stream_stmtList.hasNext() ) {
					adaptor.addChild(root_1, stream_stmtList.nextTree());
				}
				stream_stmtList.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "stmtBlock"


	public static class newExpr_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "newExpr"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:354:1: newExpr : ( 'new' Identifier '(' ')' -> ^( NewExpr Identifier ) | 'new' Identifier '[' simpleExpr ']' -> ^( NewExpr Identifier simpleExpr ) | 'new' primitiveType '[' simpleExpr ']' -> ^( NewExpr primitiveType simpleExpr ) );
	public final JavaliParser.newExpr_return newExpr() throws RecognitionException {
		JavaliParser.newExpr_return retval = new JavaliParser.newExpr_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal98=null;
		Token Identifier99=null;
		Token char_literal100=null;
		Token char_literal101=null;
		Token string_literal102=null;
		Token Identifier103=null;
		Token char_literal104=null;
		Token char_literal106=null;
		Token string_literal107=null;
		Token char_literal109=null;
		Token char_literal111=null;
		ParserRuleReturnScope simpleExpr105 =null;
		ParserRuleReturnScope primitiveType108 =null;
		ParserRuleReturnScope simpleExpr110 =null;

		Object string_literal98_tree=null;
		Object Identifier99_tree=null;
		Object char_literal100_tree=null;
		Object char_literal101_tree=null;
		Object string_literal102_tree=null;
		Object Identifier103_tree=null;
		Object char_literal104_tree=null;
		Object char_literal106_tree=null;
		Object string_literal107_tree=null;
		Object char_literal109_tree=null;
		Object char_literal111_tree=null;
		RewriteRuleTokenStream stream_Identifier=new RewriteRuleTokenStream(adaptor,"token Identifier");
		RewriteRuleTokenStream stream_60=new RewriteRuleTokenStream(adaptor,"token 60");
		RewriteRuleTokenStream stream_61=new RewriteRuleTokenStream(adaptor,"token 61");
		RewriteRuleTokenStream stream_75=new RewriteRuleTokenStream(adaptor,"token 75");
		RewriteRuleTokenStream stream_84=new RewriteRuleTokenStream(adaptor,"token 84");
		RewriteRuleTokenStream stream_76=new RewriteRuleTokenStream(adaptor,"token 76");
		RewriteRuleSubtreeStream stream_simpleExpr=new RewriteRuleSubtreeStream(adaptor,"rule simpleExpr");
		RewriteRuleSubtreeStream stream_primitiveType=new RewriteRuleSubtreeStream(adaptor,"rule primitiveType");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:355:2: ( 'new' Identifier '(' ')' -> ^( NewExpr Identifier ) | 'new' Identifier '[' simpleExpr ']' -> ^( NewExpr Identifier simpleExpr ) | 'new' primitiveType '[' simpleExpr ']' -> ^( NewExpr primitiveType simpleExpr ) )
			int alt25=3;
			int LA25_0 = input.LA(1);
			if ( (LA25_0==84) ) {
				int LA25_1 = input.LA(2);
				if ( (LA25_1==Identifier) ) {
					int LA25_2 = input.LA(3);
					if ( (LA25_2==60) ) {
						alt25=1;
					}
					else if ( (LA25_2==75) ) {
						alt25=2;
					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 25, 2, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}
				else if ( (LA25_1==77||LA25_1==81||LA25_1==83) ) {
					alt25=3;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 25, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 25, 0, input);
				throw nvae;
			}

			switch (alt25) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:355:4: 'new' Identifier '(' ')'
					{
					string_literal98=(Token)match(input,84,FOLLOW_84_in_newExpr1105);  
					stream_84.add(string_literal98);

					Identifier99=(Token)match(input,Identifier,FOLLOW_Identifier_in_newExpr1107);  
					stream_Identifier.add(Identifier99);

					char_literal100=(Token)match(input,60,FOLLOW_60_in_newExpr1109);  
					stream_60.add(char_literal100);

					char_literal101=(Token)match(input,61,FOLLOW_61_in_newExpr1111);  
					stream_61.add(char_literal101);

					// AST REWRITE
					// elements: Identifier
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 355:29: -> ^( NewExpr Identifier )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:355:32: ^( NewExpr Identifier )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(NewExpr, "NewExpr"), root_1);
						adaptor.addChild(root_1, stream_Identifier.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:356:4: 'new' Identifier '[' simpleExpr ']'
					{
					string_literal102=(Token)match(input,84,FOLLOW_84_in_newExpr1124);  
					stream_84.add(string_literal102);

					Identifier103=(Token)match(input,Identifier,FOLLOW_Identifier_in_newExpr1126);  
					stream_Identifier.add(Identifier103);

					char_literal104=(Token)match(input,75,FOLLOW_75_in_newExpr1128);  
					stream_75.add(char_literal104);

					pushFollow(FOLLOW_simpleExpr_in_newExpr1130);
					simpleExpr105=simpleExpr();
					state._fsp--;

					stream_simpleExpr.add(simpleExpr105.getTree());
					char_literal106=(Token)match(input,76,FOLLOW_76_in_newExpr1132);  
					stream_76.add(char_literal106);

					// AST REWRITE
					// elements: simpleExpr, Identifier
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 356:40: -> ^( NewExpr Identifier simpleExpr )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:356:43: ^( NewExpr Identifier simpleExpr )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(NewExpr, "NewExpr"), root_1);
						adaptor.addChild(root_1, stream_Identifier.nextNode());
						adaptor.addChild(root_1, stream_simpleExpr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 3 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:357:4: 'new' primitiveType '[' simpleExpr ']'
					{
					string_literal107=(Token)match(input,84,FOLLOW_84_in_newExpr1147);  
					stream_84.add(string_literal107);

					pushFollow(FOLLOW_primitiveType_in_newExpr1149);
					primitiveType108=primitiveType();
					state._fsp--;

					stream_primitiveType.add(primitiveType108.getTree());
					char_literal109=(Token)match(input,75,FOLLOW_75_in_newExpr1151);  
					stream_75.add(char_literal109);

					pushFollow(FOLLOW_simpleExpr_in_newExpr1153);
					simpleExpr110=simpleExpr();
					state._fsp--;

					stream_simpleExpr.add(simpleExpr110.getTree());
					char_literal111=(Token)match(input,76,FOLLOW_76_in_newExpr1155);  
					stream_76.add(char_literal111);

					// AST REWRITE
					// elements: primitiveType, simpleExpr
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 357:43: -> ^( NewExpr primitiveType simpleExpr )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:357:46: ^( NewExpr primitiveType simpleExpr )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(NewExpr, "NewExpr"), root_1);
						adaptor.addChild(root_1, stream_primitiveType.nextTree());
						adaptor.addChild(root_1, stream_simpleExpr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "newExpr"


	public static class readExpr_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "readExpr"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:360:1: readExpr : 'read' '(' ')' -> ^( BuiltInRead ) ;
	public final JavaliParser.readExpr_return readExpr() throws RecognitionException {
		JavaliParser.readExpr_return retval = new JavaliParser.readExpr_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal112=null;
		Token char_literal113=null;
		Token char_literal114=null;

		Object string_literal112_tree=null;
		Object char_literal113_tree=null;
		Object char_literal114_tree=null;
		RewriteRuleTokenStream stream_86=new RewriteRuleTokenStream(adaptor,"token 86");
		RewriteRuleTokenStream stream_60=new RewriteRuleTokenStream(adaptor,"token 60");
		RewriteRuleTokenStream stream_61=new RewriteRuleTokenStream(adaptor,"token 61");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:361:2: ( 'read' '(' ')' -> ^( BuiltInRead ) )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:361:4: 'read' '(' ')'
			{
			string_literal112=(Token)match(input,86,FOLLOW_86_in_readExpr1176);  
			stream_86.add(string_literal112);

			char_literal113=(Token)match(input,60,FOLLOW_60_in_readExpr1178);  
			stream_60.add(char_literal113);

			char_literal114=(Token)match(input,61,FOLLOW_61_in_readExpr1180);  
			stream_61.add(char_literal114);

			// AST REWRITE
			// elements: 
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 361:19: -> ^( BuiltInRead )
			{
				// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:361:22: ^( BuiltInRead )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(BuiltInRead, "BuiltInRead"), root_1);
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "readExpr"


	public static class readExprFloat_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "readExprFloat"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:364:1: readExprFloat : 'readf' '(' ')' -> ^( BuiltInReadFloat ) ;
	public final JavaliParser.readExprFloat_return readExprFloat() throws RecognitionException {
		JavaliParser.readExprFloat_return retval = new JavaliParser.readExprFloat_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal115=null;
		Token char_literal116=null;
		Token char_literal117=null;

		Object string_literal115_tree=null;
		Object char_literal116_tree=null;
		Object char_literal117_tree=null;
		RewriteRuleTokenStream stream_60=new RewriteRuleTokenStream(adaptor,"token 60");
		RewriteRuleTokenStream stream_61=new RewriteRuleTokenStream(adaptor,"token 61");
		RewriteRuleTokenStream stream_87=new RewriteRuleTokenStream(adaptor,"token 87");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:365:4: ( 'readf' '(' ')' -> ^( BuiltInReadFloat ) )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:365:6: 'readf' '(' ')'
			{
			string_literal115=(Token)match(input,87,FOLLOW_87_in_readExprFloat1200);  
			stream_87.add(string_literal115);

			char_literal116=(Token)match(input,60,FOLLOW_60_in_readExprFloat1202);  
			stream_60.add(char_literal116);

			char_literal117=(Token)match(input,61,FOLLOW_61_in_readExprFloat1204);  
			stream_61.add(char_literal117);

			// AST REWRITE
			// elements: 
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 365:22: -> ^( BuiltInReadFloat )
			{
				// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:365:25: ^( BuiltInReadFloat )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(BuiltInReadFloat, "BuiltInReadFloat"), root_1);
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "readExprFloat"


	public static class expr_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "expr"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:368:1: expr : s1= simpleExpr ( -> ^( Expr $s1) |c2= compOp s2= simpleExpr -> ^( Expr $c2 $s1 $s2) ) ;
	public final JavaliParser.expr_return expr() throws RecognitionException {
		JavaliParser.expr_return retval = new JavaliParser.expr_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope s1 =null;
		ParserRuleReturnScope c2 =null;
		ParserRuleReturnScope s2 =null;

		RewriteRuleSubtreeStream stream_simpleExpr=new RewriteRuleSubtreeStream(adaptor,"rule simpleExpr");
		RewriteRuleSubtreeStream stream_compOp=new RewriteRuleSubtreeStream(adaptor,"rule compOp");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:369:2: (s1= simpleExpr ( -> ^( Expr $s1) |c2= compOp s2= simpleExpr -> ^( Expr $c2 $s1 $s2) ) )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:369:4: s1= simpleExpr ( -> ^( Expr $s1) |c2= compOp s2= simpleExpr -> ^( Expr $c2 $s1 $s2) )
			{
			pushFollow(FOLLOW_simpleExpr_in_expr1226);
			s1=simpleExpr();
			state._fsp--;

			stream_simpleExpr.add(s1.getTree());
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:370:3: ( -> ^( Expr $s1) |c2= compOp s2= simpleExpr -> ^( Expr $c2 $s1 $s2) )
			int alt26=2;
			int LA26_0 = input.LA(1);
			if ( (LA26_0==61||LA26_0==64||LA26_0==68) ) {
				alt26=1;
			}
			else if ( (LA26_0==57||(LA26_0 >= 69 && LA26_0 <= 70)||(LA26_0 >= 72 && LA26_0 <= 74)) ) {
				alt26=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 26, 0, input);
				throw nvae;
			}

			switch (alt26) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:370:5: 
					{
					// AST REWRITE
					// elements: s1
					// token labels: 
					// rule labels: retval, s1
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_s1=new RewriteRuleSubtreeStream(adaptor,"rule s1",s1!=null?s1.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 370:5: -> ^( Expr $s1)
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:370:7: ^( Expr $s1)
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(Expr, "Expr"), root_1);
						adaptor.addChild(root_1, stream_s1.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:371:5: c2= compOp s2= simpleExpr
					{
					pushFollow(FOLLOW_compOp_in_expr1246);
					c2=compOp();
					state._fsp--;

					stream_compOp.add(c2.getTree());
					pushFollow(FOLLOW_simpleExpr_in_expr1250);
					s2=simpleExpr();
					state._fsp--;

					stream_simpleExpr.add(s2.getTree());
					// AST REWRITE
					// elements: s1, c2, s2
					// token labels: 
					// rule labels: retval, s2, s1, c2
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_s2=new RewriteRuleSubtreeStream(adaptor,"rule s2",s2!=null?s2.getTree():null);
					RewriteRuleSubtreeStream stream_s1=new RewriteRuleSubtreeStream(adaptor,"rule s1",s1!=null?s1.getTree():null);
					RewriteRuleSubtreeStream stream_c2=new RewriteRuleSubtreeStream(adaptor,"rule c2",c2!=null?c2.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 371:29: -> ^( Expr $c2 $s1 $s2)
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:371:31: ^( Expr $c2 $s1 $s2)
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(Expr, "Expr"), root_1);
						adaptor.addChild(root_1, stream_c2.nextTree());
						adaptor.addChild(root_1, stream_s1.nextTree());
						adaptor.addChild(root_1, stream_s2.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "expr"


	public static class compOp_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "compOp"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:375:1: compOp : ( '==' | '!=' | '<' | '<=' | '>' | '>=' );
	public final JavaliParser.compOp_return compOp() throws RecognitionException {
		JavaliParser.compOp_return retval = new JavaliParser.compOp_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token set118=null;

		Object set118_tree=null;

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:376:2: ( '==' | '!=' | '<' | '<=' | '>' | '>=' )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:
			{
			root_0 = (Object)adaptor.nil();


			set118=input.LT(1);
			if ( input.LA(1)==57||(input.LA(1) >= 69 && input.LA(1) <= 70)||(input.LA(1) >= 72 && input.LA(1) <= 74) ) {
				input.consume();
				adaptor.addChild(root_0, (Object)adaptor.create(set118));
				state.errorRecovery=false;
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "compOp"


	public static class simpleExpr_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "simpleExpr"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:384:1: simpleExpr : (t2= term -> ^( SimpleExpr $t2) ) (o= weakOp t= term -> ^( SimpleExpr $o $simpleExpr $t) )* ;
	public final JavaliParser.simpleExpr_return simpleExpr() throws RecognitionException {
		JavaliParser.simpleExpr_return retval = new JavaliParser.simpleExpr_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope t2 =null;
		ParserRuleReturnScope o =null;
		ParserRuleReturnScope t =null;

		RewriteRuleSubtreeStream stream_weakOp=new RewriteRuleSubtreeStream(adaptor,"rule weakOp");
		RewriteRuleSubtreeStream stream_term=new RewriteRuleSubtreeStream(adaptor,"rule term");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:385:2: ( (t2= term -> ^( SimpleExpr $t2) ) (o= weakOp t= term -> ^( SimpleExpr $o $simpleExpr $t) )* )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:386:3: (t2= term -> ^( SimpleExpr $t2) ) (o= weakOp t= term -> ^( SimpleExpr $o $simpleExpr $t) )*
			{
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:386:3: (t2= term -> ^( SimpleExpr $t2) )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:386:4: t2= term
			{
			pushFollow(FOLLOW_term_in_simpleExpr1323);
			t2=term();
			state._fsp--;

			stream_term.add(t2.getTree());
			// AST REWRITE
			// elements: t2
			// token labels: 
			// rule labels: t2, retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_t2=new RewriteRuleSubtreeStream(adaptor,"rule t2",t2!=null?t2.getTree():null);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 386:12: -> ^( SimpleExpr $t2)
			{
				// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:386:15: ^( SimpleExpr $t2)
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(SimpleExpr, "SimpleExpr"), root_1);
				adaptor.addChild(root_1, stream_t2.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:387:3: (o= weakOp t= term -> ^( SimpleExpr $o $simpleExpr $t) )*
			loop27:
			while (true) {
				int alt27=2;
				int LA27_0 = input.LA(1);
				if ( (LA27_0==63||LA27_0==65||LA27_0==96) ) {
					alt27=1;
				}

				switch (alt27) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:387:5: o= weakOp t= term
					{
					pushFollow(FOLLOW_weakOp_in_simpleExpr1341);
					o=weakOp();
					state._fsp--;

					stream_weakOp.add(o.getTree());
					pushFollow(FOLLOW_term_in_simpleExpr1345);
					t=term();
					state._fsp--;

					stream_term.add(t.getTree());
					// AST REWRITE
					// elements: o, simpleExpr, t
					// token labels: 
					// rule labels: retval, t, o
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_t=new RewriteRuleSubtreeStream(adaptor,"rule t",t!=null?t.getTree():null);
					RewriteRuleSubtreeStream stream_o=new RewriteRuleSubtreeStream(adaptor,"rule o",o!=null?o.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 388:4: -> ^( SimpleExpr $o $simpleExpr $t)
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:388:6: ^( SimpleExpr $o $simpleExpr $t)
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(SimpleExpr, "SimpleExpr"), root_1);
						adaptor.addChild(root_1, stream_o.nextTree());
						adaptor.addChild(root_1, stream_retval.nextTree());
						adaptor.addChild(root_1, stream_t.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

				default :
					break loop27;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "simpleExpr"


	public static class weakOp_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "weakOp"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:391:1: weakOp : ( '+' | '-' | '||' );
	public final JavaliParser.weakOp_return weakOp() throws RecognitionException {
		JavaliParser.weakOp_return retval = new JavaliParser.weakOp_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token set119=null;

		Object set119_tree=null;

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:392:2: ( '+' | '-' | '||' )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:
			{
			root_0 = (Object)adaptor.nil();


			set119=input.LT(1);
			if ( input.LA(1)==63||input.LA(1)==65||input.LA(1)==96 ) {
				input.consume();
				adaptor.addChild(root_0, (Object)adaptor.create(set119));
				state.errorRecovery=false;
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "weakOp"


	public static class term_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "term"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:397:1: term : (f2= factor -> ^( Term $f2) ) (s= strongOp f= factor -> ^( Term $s $term $f) )* ;
	public final JavaliParser.term_return term() throws RecognitionException {
		JavaliParser.term_return retval = new JavaliParser.term_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope f2 =null;
		ParserRuleReturnScope s =null;
		ParserRuleReturnScope f =null;

		RewriteRuleSubtreeStream stream_strongOp=new RewriteRuleSubtreeStream(adaptor,"rule strongOp");
		RewriteRuleSubtreeStream stream_factor=new RewriteRuleSubtreeStream(adaptor,"rule factor");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:398:2: ( (f2= factor -> ^( Term $f2) ) (s= strongOp f= factor -> ^( Term $s $term $f) )* )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:399:3: (f2= factor -> ^( Term $f2) ) (s= strongOp f= factor -> ^( Term $s $term $f) )*
			{
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:399:3: (f2= factor -> ^( Term $f2) )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:399:4: f2= factor
			{
			pushFollow(FOLLOW_factor_in_term1405);
			f2=factor();
			state._fsp--;

			stream_factor.add(f2.getTree());
			// AST REWRITE
			// elements: f2
			// token labels: 
			// rule labels: retval, f2
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_f2=new RewriteRuleSubtreeStream(adaptor,"rule f2",f2!=null?f2.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 399:14: -> ^( Term $f2)
			{
				// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:399:17: ^( Term $f2)
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(Term, "Term"), root_1);
				adaptor.addChild(root_1, stream_f2.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:400:3: (s= strongOp f= factor -> ^( Term $s $term $f) )*
			loop28:
			while (true) {
				int alt28=2;
				int LA28_0 = input.LA(1);
				if ( ((LA28_0 >= 58 && LA28_0 <= 59)||LA28_0==62||LA28_0==67) ) {
					alt28=1;
				}

				switch (alt28) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:400:5: s= strongOp f= factor
					{
					pushFollow(FOLLOW_strongOp_in_term1423);
					s=strongOp();
					state._fsp--;

					stream_strongOp.add(s.getTree());
					pushFollow(FOLLOW_factor_in_term1427);
					f=factor();
					state._fsp--;

					stream_factor.add(f.getTree());
					// AST REWRITE
					// elements: f, s, term
					// token labels: 
					// rule labels: f, retval, s
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_f=new RewriteRuleSubtreeStream(adaptor,"rule f",f!=null?f.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_s=new RewriteRuleSubtreeStream(adaptor,"rule s",s!=null?s.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 401:4: -> ^( Term $s $term $f)
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:401:7: ^( Term $s $term $f)
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(Term, "Term"), root_1);
						adaptor.addChild(root_1, stream_s.nextTree());
						adaptor.addChild(root_1, stream_retval.nextTree());
						adaptor.addChild(root_1, stream_f.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

				default :
					break loop28;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "term"


	public static class strongOp_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "strongOp"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:404:1: strongOp : ( '*' | '/' | '%' | '&&' );
	public final JavaliParser.strongOp_return strongOp() throws RecognitionException {
		JavaliParser.strongOp_return retval = new JavaliParser.strongOp_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token set120=null;

		Object set120_tree=null;

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:405:2: ( '*' | '/' | '%' | '&&' )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:
			{
			root_0 = (Object)adaptor.nil();


			set120=input.LT(1);
			if ( (input.LA(1) >= 58 && input.LA(1) <= 59)||input.LA(1)==62||input.LA(1)==67 ) {
				input.consume();
				adaptor.addChild(root_0, (Object)adaptor.create(set120));
				state.errorRecovery=false;
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "strongOp"


	public static class factor_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "factor"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:411:1: factor : ( '+' noSignFactor -> ^( Factor '+' noSignFactor ) | '-' noSignFactor -> ^( Factor '-' noSignFactor ) | noSignFactor -> ^( Factor noSignFactor ) );
	public final JavaliParser.factor_return factor() throws RecognitionException {
		JavaliParser.factor_return retval = new JavaliParser.factor_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal121=null;
		Token char_literal123=null;
		ParserRuleReturnScope noSignFactor122 =null;
		ParserRuleReturnScope noSignFactor124 =null;
		ParserRuleReturnScope noSignFactor125 =null;

		Object char_literal121_tree=null;
		Object char_literal123_tree=null;
		RewriteRuleTokenStream stream_65=new RewriteRuleTokenStream(adaptor,"token 65");
		RewriteRuleTokenStream stream_63=new RewriteRuleTokenStream(adaptor,"token 63");
		RewriteRuleSubtreeStream stream_noSignFactor=new RewriteRuleSubtreeStream(adaptor,"rule noSignFactor");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:412:2: ( '+' noSignFactor -> ^( Factor '+' noSignFactor ) | '-' noSignFactor -> ^( Factor '-' noSignFactor ) | noSignFactor -> ^( Factor noSignFactor ) )
			int alt29=3;
			switch ( input.LA(1) ) {
			case 63:
				{
				alt29=1;
				}
				break;
			case 65:
				{
				alt29=2;
				}
				break;
			case BooleanLiteral:
			case DecimalNumber:
			case FloatNumber:
			case HexNumber:
			case Identifier:
			case 56:
			case 60:
			case 85:
			case 89:
				{
				alt29=3;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 29, 0, input);
				throw nvae;
			}
			switch (alt29) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:412:4: '+' noSignFactor
					{
					char_literal121=(Token)match(input,63,FOLLOW_63_in_factor1485);  
					stream_63.add(char_literal121);

					pushFollow(FOLLOW_noSignFactor_in_factor1487);
					noSignFactor122=noSignFactor();
					state._fsp--;

					stream_noSignFactor.add(noSignFactor122.getTree());
					// AST REWRITE
					// elements: noSignFactor, 63
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 412:21: -> ^( Factor '+' noSignFactor )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:412:23: ^( Factor '+' noSignFactor )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(Factor, "Factor"), root_1);
						adaptor.addChild(root_1, stream_63.nextNode());
						adaptor.addChild(root_1, stream_noSignFactor.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:413:4: '-' noSignFactor
					{
					char_literal123=(Token)match(input,65,FOLLOW_65_in_factor1501);  
					stream_65.add(char_literal123);

					pushFollow(FOLLOW_noSignFactor_in_factor1503);
					noSignFactor124=noSignFactor();
					state._fsp--;

					stream_noSignFactor.add(noSignFactor124.getTree());
					// AST REWRITE
					// elements: noSignFactor, 65
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 413:21: -> ^( Factor '-' noSignFactor )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:413:23: ^( Factor '-' noSignFactor )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(Factor, "Factor"), root_1);
						adaptor.addChild(root_1, stream_65.nextNode());
						adaptor.addChild(root_1, stream_noSignFactor.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 3 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:414:4: noSignFactor
					{
					pushFollow(FOLLOW_noSignFactor_in_factor1517);
					noSignFactor125=noSignFactor();
					state._fsp--;

					stream_noSignFactor.add(noSignFactor125.getTree());
					// AST REWRITE
					// elements: noSignFactor
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 414:17: -> ^( Factor noSignFactor )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:414:19: ^( Factor noSignFactor )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(Factor, "Factor"), root_1);
						adaptor.addChild(root_1, stream_noSignFactor.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "factor"


	public static class noSignFactor_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "noSignFactor"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:417:1: noSignFactor : ( '!' factor -> ^( NoSignFactor '!' factor ) | DecimalNumber -> ^( NoSignFactor DecimalNumber ) | HexNumber -> ^( NoSignFactor HexNumber ) | FloatNumber -> ^( NoSignFactor FloatNumber ) | BooleanLiteral -> ^( NoSignFactor BooleanLiteral ) | 'null' -> ^( NoSignFactor 'null' ) | identAccess -> ^( NoSignFactor identAccess ) | '(' expr ')' -> ^( NoSignFactor expr ) | '(' referenceType ')' noSignFactor -> ^( NoSignFactor referenceType noSignFactor ) );
	public final JavaliParser.noSignFactor_return noSignFactor() throws RecognitionException {
		JavaliParser.noSignFactor_return retval = new JavaliParser.noSignFactor_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal126=null;
		Token DecimalNumber128=null;
		Token HexNumber129=null;
		Token FloatNumber130=null;
		Token BooleanLiteral131=null;
		Token string_literal132=null;
		Token char_literal134=null;
		Token char_literal136=null;
		Token char_literal137=null;
		Token char_literal139=null;
		ParserRuleReturnScope factor127 =null;
		ParserRuleReturnScope identAccess133 =null;
		ParserRuleReturnScope expr135 =null;
		ParserRuleReturnScope referenceType138 =null;
		ParserRuleReturnScope noSignFactor140 =null;

		Object char_literal126_tree=null;
		Object DecimalNumber128_tree=null;
		Object HexNumber129_tree=null;
		Object FloatNumber130_tree=null;
		Object BooleanLiteral131_tree=null;
		Object string_literal132_tree=null;
		Object char_literal134_tree=null;
		Object char_literal136_tree=null;
		Object char_literal137_tree=null;
		Object char_literal139_tree=null;
		RewriteRuleTokenStream stream_HexNumber=new RewriteRuleTokenStream(adaptor,"token HexNumber");
		RewriteRuleTokenStream stream_DecimalNumber=new RewriteRuleTokenStream(adaptor,"token DecimalNumber");
		RewriteRuleTokenStream stream_56=new RewriteRuleTokenStream(adaptor,"token 56");
		RewriteRuleTokenStream stream_FloatNumber=new RewriteRuleTokenStream(adaptor,"token FloatNumber");
		RewriteRuleTokenStream stream_60=new RewriteRuleTokenStream(adaptor,"token 60");
		RewriteRuleTokenStream stream_61=new RewriteRuleTokenStream(adaptor,"token 61");
		RewriteRuleTokenStream stream_BooleanLiteral=new RewriteRuleTokenStream(adaptor,"token BooleanLiteral");
		RewriteRuleTokenStream stream_85=new RewriteRuleTokenStream(adaptor,"token 85");
		RewriteRuleSubtreeStream stream_noSignFactor=new RewriteRuleSubtreeStream(adaptor,"rule noSignFactor");
		RewriteRuleSubtreeStream stream_referenceType=new RewriteRuleSubtreeStream(adaptor,"rule referenceType");
		RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");
		RewriteRuleSubtreeStream stream_factor=new RewriteRuleSubtreeStream(adaptor,"rule factor");
		RewriteRuleSubtreeStream stream_identAccess=new RewriteRuleSubtreeStream(adaptor,"rule identAccess");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:418:2: ( '!' factor -> ^( NoSignFactor '!' factor ) | DecimalNumber -> ^( NoSignFactor DecimalNumber ) | HexNumber -> ^( NoSignFactor HexNumber ) | FloatNumber -> ^( NoSignFactor FloatNumber ) | BooleanLiteral -> ^( NoSignFactor BooleanLiteral ) | 'null' -> ^( NoSignFactor 'null' ) | identAccess -> ^( NoSignFactor identAccess ) | '(' expr ')' -> ^( NoSignFactor expr ) | '(' referenceType ')' noSignFactor -> ^( NoSignFactor referenceType noSignFactor ) )
			int alt30=9;
			switch ( input.LA(1) ) {
			case 56:
				{
				alt30=1;
				}
				break;
			case DecimalNumber:
				{
				alt30=2;
				}
				break;
			case HexNumber:
				{
				alt30=3;
				}
				break;
			case FloatNumber:
				{
				alt30=4;
				}
				break;
			case BooleanLiteral:
				{
				alt30=5;
				}
				break;
			case 85:
				{
				alt30=6;
				}
				break;
			case Identifier:
			case 89:
				{
				alt30=7;
				}
				break;
			case 60:
				{
				switch ( input.LA(2) ) {
				case BooleanLiteral:
				case DecimalNumber:
				case FloatNumber:
				case HexNumber:
				case 56:
				case 60:
				case 63:
				case 65:
				case 85:
				case 89:
					{
					alt30=8;
					}
					break;
				case Identifier:
					{
					switch ( input.LA(3) ) {
					case 75:
						{
						int LA30_12 = input.LA(4);
						if ( (LA30_12==76) ) {
							alt30=9;
						}
						else if ( (LA30_12==BooleanLiteral||LA30_12==DecimalNumber||LA30_12==FloatNumber||LA30_12==HexNumber||LA30_12==Identifier||LA30_12==56||LA30_12==60||LA30_12==63||LA30_12==65||LA30_12==85||LA30_12==89) ) {
							alt30=8;
						}

						else {
							int nvaeMark = input.mark();
							try {
								for (int nvaeConsume = 0; nvaeConsume < 4 - 1; nvaeConsume++) {
									input.consume();
								}
								NoViableAltException nvae =
									new NoViableAltException("", 30, 12, input);
								throw nvae;
							} finally {
								input.rewind(nvaeMark);
							}
						}

						}
						break;
					case 57:
					case 58:
					case 59:
					case 60:
					case 62:
					case 63:
					case 65:
					case 66:
					case 67:
					case 69:
					case 70:
					case 72:
					case 73:
					case 74:
					case 96:
						{
						alt30=8;
						}
						break;
					case 61:
						{
						int LA30_13 = input.LA(4);
						if ( ((LA30_13 >= 57 && LA30_13 <= 59)||(LA30_13 >= 61 && LA30_13 <= 65)||(LA30_13 >= 67 && LA30_13 <= 70)||(LA30_13 >= 72 && LA30_13 <= 74)||LA30_13==76||LA30_13==96) ) {
							alt30=8;
						}
						else if ( (LA30_13==BooleanLiteral||LA30_13==DecimalNumber||LA30_13==FloatNumber||LA30_13==HexNumber||LA30_13==Identifier||LA30_13==56||LA30_13==60||LA30_13==85||LA30_13==89) ) {
							alt30=9;
						}

						else {
							int nvaeMark = input.mark();
							try {
								for (int nvaeConsume = 0; nvaeConsume < 4 - 1; nvaeConsume++) {
									input.consume();
								}
								NoViableAltException nvae =
									new NoViableAltException("", 30, 13, input);
								throw nvae;
							} finally {
								input.rewind(nvaeMark);
							}
						}

						}
						break;
					default:
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 30, 10, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}
					}
					break;
				case 77:
				case 81:
				case 83:
					{
					alt30=9;
					}
					break;
				default:
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 30, 8, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 30, 0, input);
				throw nvae;
			}
			switch (alt30) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:418:4: '!' factor
					{
					char_literal126=(Token)match(input,56,FOLLOW_56_in_noSignFactor1535);  
					stream_56.add(char_literal126);

					pushFollow(FOLLOW_factor_in_noSignFactor1537);
					factor127=factor();
					state._fsp--;

					stream_factor.add(factor127.getTree());
					// AST REWRITE
					// elements: factor, 56
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 418:15: -> ^( NoSignFactor '!' factor )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:418:18: ^( NoSignFactor '!' factor )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(NoSignFactor, "NoSignFactor"), root_1);
						adaptor.addChild(root_1, stream_56.nextNode());
						adaptor.addChild(root_1, stream_factor.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:419:4: DecimalNumber
					{
					DecimalNumber128=(Token)match(input,DecimalNumber,FOLLOW_DecimalNumber_in_noSignFactor1552);  
					stream_DecimalNumber.add(DecimalNumber128);

					// AST REWRITE
					// elements: DecimalNumber
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 419:18: -> ^( NoSignFactor DecimalNumber )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:419:21: ^( NoSignFactor DecimalNumber )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(NoSignFactor, "NoSignFactor"), root_1);
						adaptor.addChild(root_1, stream_DecimalNumber.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 3 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:420:4: HexNumber
					{
					HexNumber129=(Token)match(input,HexNumber,FOLLOW_HexNumber_in_noSignFactor1565);  
					stream_HexNumber.add(HexNumber129);

					// AST REWRITE
					// elements: HexNumber
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 420:14: -> ^( NoSignFactor HexNumber )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:420:17: ^( NoSignFactor HexNumber )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(NoSignFactor, "NoSignFactor"), root_1);
						adaptor.addChild(root_1, stream_HexNumber.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 4 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:421:4: FloatNumber
					{
					FloatNumber130=(Token)match(input,FloatNumber,FOLLOW_FloatNumber_in_noSignFactor1578);  
					stream_FloatNumber.add(FloatNumber130);

					// AST REWRITE
					// elements: FloatNumber
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 421:16: -> ^( NoSignFactor FloatNumber )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:421:19: ^( NoSignFactor FloatNumber )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(NoSignFactor, "NoSignFactor"), root_1);
						adaptor.addChild(root_1, stream_FloatNumber.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 5 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:422:4: BooleanLiteral
					{
					BooleanLiteral131=(Token)match(input,BooleanLiteral,FOLLOW_BooleanLiteral_in_noSignFactor1591);  
					stream_BooleanLiteral.add(BooleanLiteral131);

					// AST REWRITE
					// elements: BooleanLiteral
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 422:19: -> ^( NoSignFactor BooleanLiteral )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:422:22: ^( NoSignFactor BooleanLiteral )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(NoSignFactor, "NoSignFactor"), root_1);
						adaptor.addChild(root_1, stream_BooleanLiteral.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 6 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:423:4: 'null'
					{
					string_literal132=(Token)match(input,85,FOLLOW_85_in_noSignFactor1604);  
					stream_85.add(string_literal132);

					// AST REWRITE
					// elements: 85
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 423:11: -> ^( NoSignFactor 'null' )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:423:13: ^( NoSignFactor 'null' )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(NoSignFactor, "NoSignFactor"), root_1);
						adaptor.addChild(root_1, stream_85.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 7 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:424:6: identAccess
					{
					pushFollow(FOLLOW_identAccess_in_noSignFactor1618);
					identAccess133=identAccess();
					state._fsp--;

					stream_identAccess.add(identAccess133.getTree());
					// AST REWRITE
					// elements: identAccess
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 424:18: -> ^( NoSignFactor identAccess )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:424:21: ^( NoSignFactor identAccess )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(NoSignFactor, "NoSignFactor"), root_1);
						adaptor.addChild(root_1, stream_identAccess.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 8 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:425:7: '(' expr ')'
					{
					char_literal134=(Token)match(input,60,FOLLOW_60_in_noSignFactor1634);  
					stream_60.add(char_literal134);

					pushFollow(FOLLOW_expr_in_noSignFactor1636);
					expr135=expr();
					state._fsp--;

					stream_expr.add(expr135.getTree());
					char_literal136=(Token)match(input,61,FOLLOW_61_in_noSignFactor1638);  
					stream_61.add(char_literal136);

					// AST REWRITE
					// elements: expr
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 425:20: -> ^( NoSignFactor expr )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:425:23: ^( NoSignFactor expr )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(NoSignFactor, "NoSignFactor"), root_1);
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 9 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:426:4: '(' referenceType ')' noSignFactor
					{
					char_literal137=(Token)match(input,60,FOLLOW_60_in_noSignFactor1651);  
					stream_60.add(char_literal137);

					pushFollow(FOLLOW_referenceType_in_noSignFactor1653);
					referenceType138=referenceType();
					state._fsp--;

					stream_referenceType.add(referenceType138.getTree());
					char_literal139=(Token)match(input,61,FOLLOW_61_in_noSignFactor1655);  
					stream_61.add(char_literal139);

					pushFollow(FOLLOW_noSignFactor_in_noSignFactor1657);
					noSignFactor140=noSignFactor();
					state._fsp--;

					stream_noSignFactor.add(noSignFactor140.getTree());
					// AST REWRITE
					// elements: noSignFactor, referenceType
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 426:39: -> ^( NoSignFactor referenceType noSignFactor )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:426:42: ^( NoSignFactor referenceType noSignFactor )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(NoSignFactor, "NoSignFactor"), root_1);
						adaptor.addChild(root_1, stream_referenceType.nextTree());
						adaptor.addChild(root_1, stream_noSignFactor.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "noSignFactor"


	public static class identAccess_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "identAccess"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:429:1: identAccess : ( Identifier -> Identifier | Identifier methodCallTail -> Identifier methodCallTail | 'this' -> 'this' ) ( selectorSeq )? -> ^( IdentAccess $identAccess ( selectorSeq )? ) ;
	public final JavaliParser.identAccess_return identAccess() throws RecognitionException {
		JavaliParser.identAccess_return retval = new JavaliParser.identAccess_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token Identifier141=null;
		Token Identifier142=null;
		Token string_literal144=null;
		ParserRuleReturnScope methodCallTail143 =null;
		ParserRuleReturnScope selectorSeq145 =null;

		Object Identifier141_tree=null;
		Object Identifier142_tree=null;
		Object string_literal144_tree=null;
		RewriteRuleTokenStream stream_Identifier=new RewriteRuleTokenStream(adaptor,"token Identifier");
		RewriteRuleTokenStream stream_89=new RewriteRuleTokenStream(adaptor,"token 89");
		RewriteRuleSubtreeStream stream_selectorSeq=new RewriteRuleSubtreeStream(adaptor,"rule selectorSeq");
		RewriteRuleSubtreeStream stream_methodCallTail=new RewriteRuleSubtreeStream(adaptor,"rule methodCallTail");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:430:4: ( ( Identifier -> Identifier | Identifier methodCallTail -> Identifier methodCallTail | 'this' -> 'this' ) ( selectorSeq )? -> ^( IdentAccess $identAccess ( selectorSeq )? ) )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:430:6: ( Identifier -> Identifier | Identifier methodCallTail -> Identifier methodCallTail | 'this' -> 'this' ) ( selectorSeq )?
			{
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:430:6: ( Identifier -> Identifier | Identifier methodCallTail -> Identifier methodCallTail | 'this' -> 'this' )
			int alt31=3;
			int LA31_0 = input.LA(1);
			if ( (LA31_0==Identifier) ) {
				int LA31_1 = input.LA(2);
				if ( ((LA31_1 >= 57 && LA31_1 <= 59)||(LA31_1 >= 61 && LA31_1 <= 76)||LA31_1==96) ) {
					alt31=1;
				}
				else if ( (LA31_1==60) ) {
					alt31=2;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 31, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}
			else if ( (LA31_0==89) ) {
				alt31=3;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 31, 0, input);
				throw nvae;
			}

			switch (alt31) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:430:8: Identifier
					{
					Identifier141=(Token)match(input,Identifier,FOLLOW_Identifier_in_identAccess1682);  
					stream_Identifier.add(Identifier141);

					// AST REWRITE
					// elements: Identifier
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 430:19: -> Identifier
					{
						adaptor.addChild(root_0, stream_Identifier.nextNode());
					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:431:9: Identifier methodCallTail
					{
					Identifier142=(Token)match(input,Identifier,FOLLOW_Identifier_in_identAccess1696);  
					stream_Identifier.add(Identifier142);

					pushFollow(FOLLOW_methodCallTail_in_identAccess1698);
					methodCallTail143=methodCallTail();
					state._fsp--;

					stream_methodCallTail.add(methodCallTail143.getTree());
					// AST REWRITE
					// elements: Identifier, methodCallTail
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 431:35: -> Identifier methodCallTail
					{
						adaptor.addChild(root_0, stream_Identifier.nextNode());
						adaptor.addChild(root_0, stream_methodCallTail.nextTree());
					}


					retval.tree = root_0;

					}
					break;
				case 3 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:432:9: 'this'
					{
					string_literal144=(Token)match(input,89,FOLLOW_89_in_identAccess1713);  
					stream_89.add(string_literal144);

					// AST REWRITE
					// elements: 89
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 432:16: -> 'this'
					{
						adaptor.addChild(root_0, stream_89.nextNode());
					}


					retval.tree = root_0;

					}
					break;

			}

			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:434:7: ( selectorSeq )?
			int alt32=2;
			int LA32_0 = input.LA(1);
			if ( (LA32_0==66||LA32_0==75) ) {
				alt32=1;
			}
			switch (alt32) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:434:10: selectorSeq
					{
					pushFollow(FOLLOW_selectorSeq_in_identAccess1735);
					selectorSeq145=selectorSeq();
					state._fsp--;

					stream_selectorSeq.add(selectorSeq145.getTree());
					}
					break;

			}

			// AST REWRITE
			// elements: selectorSeq, identAccess
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 434:25: -> ^( IdentAccess $identAccess ( selectorSeq )? )
			{
				// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:434:28: ^( IdentAccess $identAccess ( selectorSeq )? )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(IdentAccess, "IdentAccess"), root_1);
				adaptor.addChild(root_1, stream_retval.nextTree());
				// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:434:55: ( selectorSeq )?
				if ( stream_selectorSeq.hasNext() ) {
					adaptor.addChild(root_1, stream_selectorSeq.nextTree());
				}
				stream_selectorSeq.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "identAccess"


	public static class selectorSeq_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "selectorSeq"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:437:1: selectorSeq : ( (fs= fieldSelector -> ^( SelectorSeq $fs) |es= elemSelector -> ^( SelectorSeq $es) ) ) (f= fieldSelector -> ^( SelectorSeq $selectorSeq $f) |e= elemSelector -> ^( SelectorSeq $selectorSeq $e) )* ;
	public final JavaliParser.selectorSeq_return selectorSeq() throws RecognitionException {
		JavaliParser.selectorSeq_return retval = new JavaliParser.selectorSeq_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope fs =null;
		ParserRuleReturnScope es =null;
		ParserRuleReturnScope f =null;
		ParserRuleReturnScope e =null;

		RewriteRuleSubtreeStream stream_elemSelector=new RewriteRuleSubtreeStream(adaptor,"rule elemSelector");
		RewriteRuleSubtreeStream stream_fieldSelector=new RewriteRuleSubtreeStream(adaptor,"rule fieldSelector");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:438:4: ( ( (fs= fieldSelector -> ^( SelectorSeq $fs) |es= elemSelector -> ^( SelectorSeq $es) ) ) (f= fieldSelector -> ^( SelectorSeq $selectorSeq $f) |e= elemSelector -> ^( SelectorSeq $selectorSeq $e) )* )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:439:7: ( (fs= fieldSelector -> ^( SelectorSeq $fs) |es= elemSelector -> ^( SelectorSeq $es) ) ) (f= fieldSelector -> ^( SelectorSeq $selectorSeq $f) |e= elemSelector -> ^( SelectorSeq $selectorSeq $e) )*
			{
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:439:7: ( (fs= fieldSelector -> ^( SelectorSeq $fs) |es= elemSelector -> ^( SelectorSeq $es) ) )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:439:9: (fs= fieldSelector -> ^( SelectorSeq $fs) |es= elemSelector -> ^( SelectorSeq $es) )
			{
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:439:9: (fs= fieldSelector -> ^( SelectorSeq $fs) |es= elemSelector -> ^( SelectorSeq $es) )
			int alt33=2;
			int LA33_0 = input.LA(1);
			if ( (LA33_0==66) ) {
				alt33=1;
			}
			else if ( (LA33_0==75) ) {
				alt33=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 33, 0, input);
				throw nvae;
			}

			switch (alt33) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:439:11: fs= fieldSelector
					{
					pushFollow(FOLLOW_fieldSelector_in_selectorSeq1779);
					fs=fieldSelector();
					state._fsp--;

					stream_fieldSelector.add(fs.getTree());
					// AST REWRITE
					// elements: fs
					// token labels: 
					// rule labels: retval, fs
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_fs=new RewriteRuleSubtreeStream(adaptor,"rule fs",fs!=null?fs.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 439:28: -> ^( SelectorSeq $fs)
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:439:30: ^( SelectorSeq $fs)
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(SelectorSeq, "SelectorSeq"), root_1);
						adaptor.addChild(root_1, stream_fs.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:439:51: es= elemSelector
					{
					pushFollow(FOLLOW_elemSelector_in_selectorSeq1793);
					es=elemSelector();
					state._fsp--;

					stream_elemSelector.add(es.getTree());
					// AST REWRITE
					// elements: es
					// token labels: 
					// rule labels: retval, es
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_es=new RewriteRuleSubtreeStream(adaptor,"rule es",es!=null?es.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 439:67: -> ^( SelectorSeq $es)
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:439:69: ^( SelectorSeq $es)
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(SelectorSeq, "SelectorSeq"), root_1);
						adaptor.addChild(root_1, stream_es.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}

			}

			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:440:7: (f= fieldSelector -> ^( SelectorSeq $selectorSeq $f) |e= elemSelector -> ^( SelectorSeq $selectorSeq $e) )*
			loop34:
			while (true) {
				int alt34=3;
				int LA34_0 = input.LA(1);
				if ( (LA34_0==66) ) {
					alt34=1;
				}
				else if ( (LA34_0==75) ) {
					alt34=2;
				}

				switch (alt34) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:440:10: f= fieldSelector
					{
					pushFollow(FOLLOW_fieldSelector_in_selectorSeq1817);
					f=fieldSelector();
					state._fsp--;

					stream_fieldSelector.add(f.getTree());
					// AST REWRITE
					// elements: f, selectorSeq
					// token labels: 
					// rule labels: f, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_f=new RewriteRuleSubtreeStream(adaptor,"rule f",f!=null?f.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 440:26: -> ^( SelectorSeq $selectorSeq $f)
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:440:29: ^( SelectorSeq $selectorSeq $f)
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(SelectorSeq, "SelectorSeq"), root_1);
						adaptor.addChild(root_1, stream_retval.nextTree());
						adaptor.addChild(root_1, stream_f.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:441:10: e= elemSelector
					{
					pushFollow(FOLLOW_elemSelector_in_selectorSeq1842);
					e=elemSelector();
					state._fsp--;

					stream_elemSelector.add(e.getTree());
					// AST REWRITE
					// elements: selectorSeq, e
					// token labels: 
					// rule labels: retval, e
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_e=new RewriteRuleSubtreeStream(adaptor,"rule e",e!=null?e.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 441:25: -> ^( SelectorSeq $selectorSeq $e)
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:441:28: ^( SelectorSeq $selectorSeq $e)
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(SelectorSeq, "SelectorSeq"), root_1);
						adaptor.addChild(root_1, stream_retval.nextTree());
						adaptor.addChild(root_1, stream_e.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

				default :
					break loop34;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "selectorSeq"


	public static class fieldSelector_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "fieldSelector"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:445:1: fieldSelector : ( '.' Identifier methodCallTail -> ^( FieldSelector Identifier methodCallTail ) | '.' Identifier -> ^( FieldSelector Identifier ) );
	public final JavaliParser.fieldSelector_return fieldSelector() throws RecognitionException {
		JavaliParser.fieldSelector_return retval = new JavaliParser.fieldSelector_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal146=null;
		Token Identifier147=null;
		Token char_literal149=null;
		Token Identifier150=null;
		ParserRuleReturnScope methodCallTail148 =null;

		Object char_literal146_tree=null;
		Object Identifier147_tree=null;
		Object char_literal149_tree=null;
		Object Identifier150_tree=null;
		RewriteRuleTokenStream stream_66=new RewriteRuleTokenStream(adaptor,"token 66");
		RewriteRuleTokenStream stream_Identifier=new RewriteRuleTokenStream(adaptor,"token Identifier");
		RewriteRuleSubtreeStream stream_methodCallTail=new RewriteRuleSubtreeStream(adaptor,"rule methodCallTail");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:446:4: ( '.' Identifier methodCallTail -> ^( FieldSelector Identifier methodCallTail ) | '.' Identifier -> ^( FieldSelector Identifier ) )
			int alt35=2;
			int LA35_0 = input.LA(1);
			if ( (LA35_0==66) ) {
				int LA35_1 = input.LA(2);
				if ( (LA35_1==Identifier) ) {
					int LA35_2 = input.LA(3);
					if ( (LA35_2==60) ) {
						alt35=1;
					}
					else if ( ((LA35_2 >= 57 && LA35_2 <= 59)||(LA35_2 >= 61 && LA35_2 <= 76)||LA35_2==96) ) {
						alt35=2;
					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 35, 2, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 35, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 35, 0, input);
				throw nvae;
			}

			switch (alt35) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:446:6: '.' Identifier methodCallTail
					{
					char_literal146=(Token)match(input,66,FOLLOW_66_in_fieldSelector1878);  
					stream_66.add(char_literal146);

					Identifier147=(Token)match(input,Identifier,FOLLOW_Identifier_in_fieldSelector1880);  
					stream_Identifier.add(Identifier147);

					pushFollow(FOLLOW_methodCallTail_in_fieldSelector1882);
					methodCallTail148=methodCallTail();
					state._fsp--;

					stream_methodCallTail.add(methodCallTail148.getTree());
					// AST REWRITE
					// elements: Identifier, methodCallTail
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 446:36: -> ^( FieldSelector Identifier methodCallTail )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:446:39: ^( FieldSelector Identifier methodCallTail )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(FieldSelector, "FieldSelector"), root_1);
						adaptor.addChild(root_1, stream_Identifier.nextNode());
						adaptor.addChild(root_1, stream_methodCallTail.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:447:6: '.' Identifier
					{
					char_literal149=(Token)match(input,66,FOLLOW_66_in_fieldSelector1899);  
					stream_66.add(char_literal149);

					Identifier150=(Token)match(input,Identifier,FOLLOW_Identifier_in_fieldSelector1901);  
					stream_Identifier.add(Identifier150);

					// AST REWRITE
					// elements: Identifier
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 447:21: -> ^( FieldSelector Identifier )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:447:24: ^( FieldSelector Identifier )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(FieldSelector, "FieldSelector"), root_1);
						adaptor.addChild(root_1, stream_Identifier.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "fieldSelector"


	public static class elemSelector_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "elemSelector"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:450:1: elemSelector : '[' simpleExpr ']' -> ^( ElemSelector simpleExpr ) ;
	public final JavaliParser.elemSelector_return elemSelector() throws RecognitionException {
		JavaliParser.elemSelector_return retval = new JavaliParser.elemSelector_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal151=null;
		Token char_literal153=null;
		ParserRuleReturnScope simpleExpr152 =null;

		Object char_literal151_tree=null;
		Object char_literal153_tree=null;
		RewriteRuleTokenStream stream_75=new RewriteRuleTokenStream(adaptor,"token 75");
		RewriteRuleTokenStream stream_76=new RewriteRuleTokenStream(adaptor,"token 76");
		RewriteRuleSubtreeStream stream_simpleExpr=new RewriteRuleSubtreeStream(adaptor,"rule simpleExpr");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:451:4: ( '[' simpleExpr ']' -> ^( ElemSelector simpleExpr ) )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:451:7: '[' simpleExpr ']'
			{
			char_literal151=(Token)match(input,75,FOLLOW_75_in_elemSelector1925);  
			stream_75.add(char_literal151);

			pushFollow(FOLLOW_simpleExpr_in_elemSelector1927);
			simpleExpr152=simpleExpr();
			state._fsp--;

			stream_simpleExpr.add(simpleExpr152.getTree());
			char_literal153=(Token)match(input,76,FOLLOW_76_in_elemSelector1929);  
			stream_76.add(char_literal153);

			// AST REWRITE
			// elements: simpleExpr
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 451:26: -> ^( ElemSelector simpleExpr )
			{
				// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:451:29: ^( ElemSelector simpleExpr )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ElemSelector, "ElemSelector"), root_1);
				adaptor.addChild(root_1, stream_simpleExpr.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "elemSelector"


	public static class type_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "type"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:456:1: type : ( primitiveType | referenceType );
	public final JavaliParser.type_return type() throws RecognitionException {
		JavaliParser.type_return retval = new JavaliParser.type_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope primitiveType154 =null;
		ParserRuleReturnScope referenceType155 =null;


		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:457:2: ( primitiveType | referenceType )
			int alt36=2;
			int LA36_0 = input.LA(1);
			if ( (LA36_0==77||LA36_0==81||LA36_0==83) ) {
				int LA36_1 = input.LA(2);
				if ( (LA36_1==Identifier) ) {
					alt36=1;
				}
				else if ( (LA36_1==75) ) {
					alt36=2;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 36, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}
			else if ( (LA36_0==Identifier) ) {
				alt36=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 36, 0, input);
				throw nvae;
			}

			switch (alt36) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:457:4: primitiveType
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_primitiveType_in_type1952);
					primitiveType154=primitiveType();
					state._fsp--;

					adaptor.addChild(root_0, primitiveType154.getTree());

					}
					break;
				case 2 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:458:4: referenceType
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_referenceType_in_type1958);
					referenceType155=referenceType();
					state._fsp--;

					adaptor.addChild(root_0, referenceType155.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "type"


	public static class referenceType_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "referenceType"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:461:1: referenceType : ( Identifier | arrayType );
	public final JavaliParser.referenceType_return referenceType() throws RecognitionException {
		JavaliParser.referenceType_return retval = new JavaliParser.referenceType_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token Identifier156=null;
		ParserRuleReturnScope arrayType157 =null;

		Object Identifier156_tree=null;

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:462:2: ( Identifier | arrayType )
			int alt37=2;
			int LA37_0 = input.LA(1);
			if ( (LA37_0==Identifier) ) {
				int LA37_1 = input.LA(2);
				if ( (LA37_1==75) ) {
					alt37=2;
				}
				else if ( (LA37_1==Identifier||LA37_1==61) ) {
					alt37=1;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 37, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}
			else if ( (LA37_0==77||LA37_0==81||LA37_0==83) ) {
				alt37=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 37, 0, input);
				throw nvae;
			}

			switch (alt37) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:462:4: Identifier
					{
					root_0 = (Object)adaptor.nil();


					Identifier156=(Token)match(input,Identifier,FOLLOW_Identifier_in_referenceType1970); 
					Identifier156_tree = (Object)adaptor.create(Identifier156);
					adaptor.addChild(root_0, Identifier156_tree);

					}
					break;
				case 2 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:463:4: arrayType
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_arrayType_in_referenceType1975);
					arrayType157=arrayType();
					state._fsp--;

					adaptor.addChild(root_0, arrayType157.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "referenceType"


	public static class primitiveType_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "primitiveType"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:466:1: primitiveType : ( 'int' | 'float' | 'boolean' );
	public final JavaliParser.primitiveType_return primitiveType() throws RecognitionException {
		JavaliParser.primitiveType_return retval = new JavaliParser.primitiveType_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token set158=null;

		Object set158_tree=null;

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:467:2: ( 'int' | 'float' | 'boolean' )
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:
			{
			root_0 = (Object)adaptor.nil();


			set158=input.LT(1);
			if ( input.LA(1)==77||input.LA(1)==81||input.LA(1)==83 ) {
				input.consume();
				adaptor.addChild(root_0, (Object)adaptor.create(set158));
				state.errorRecovery=false;
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "primitiveType"


	public static class arrayType_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "arrayType"
	// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:472:1: arrayType : ( Identifier '[' ']' -> ^( ArrayType Identifier ) | primitiveType '[' ']' -> ^( ArrayType primitiveType ) );
	public final JavaliParser.arrayType_return arrayType() throws RecognitionException {
		JavaliParser.arrayType_return retval = new JavaliParser.arrayType_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token Identifier159=null;
		Token char_literal160=null;
		Token char_literal161=null;
		Token char_literal163=null;
		Token char_literal164=null;
		ParserRuleReturnScope primitiveType162 =null;

		Object Identifier159_tree=null;
		Object char_literal160_tree=null;
		Object char_literal161_tree=null;
		Object char_literal163_tree=null;
		Object char_literal164_tree=null;
		RewriteRuleTokenStream stream_Identifier=new RewriteRuleTokenStream(adaptor,"token Identifier");
		RewriteRuleTokenStream stream_75=new RewriteRuleTokenStream(adaptor,"token 75");
		RewriteRuleTokenStream stream_76=new RewriteRuleTokenStream(adaptor,"token 76");
		RewriteRuleSubtreeStream stream_primitiveType=new RewriteRuleSubtreeStream(adaptor,"rule primitiveType");

		try {
			// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:473:2: ( Identifier '[' ']' -> ^( ArrayType Identifier ) | primitiveType '[' ']' -> ^( ArrayType primitiveType ) )
			int alt38=2;
			int LA38_0 = input.LA(1);
			if ( (LA38_0==Identifier) ) {
				alt38=1;
			}
			else if ( (LA38_0==77||LA38_0==81||LA38_0==83) ) {
				alt38=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 38, 0, input);
				throw nvae;
			}

			switch (alt38) {
				case 1 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:473:4: Identifier '[' ']'
					{
					Identifier159=(Token)match(input,Identifier,FOLLOW_Identifier_in_arrayType2008);  
					stream_Identifier.add(Identifier159);

					char_literal160=(Token)match(input,75,FOLLOW_75_in_arrayType2010);  
					stream_75.add(char_literal160);

					char_literal161=(Token)match(input,76,FOLLOW_76_in_arrayType2012);  
					stream_76.add(char_literal161);

					// AST REWRITE
					// elements: Identifier
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 473:23: -> ^( ArrayType Identifier )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:473:26: ^( ArrayType Identifier )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ArrayType, "ArrayType"), root_1);
						adaptor.addChild(root_1, stream_Identifier.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:474:4: primitiveType '[' ']'
					{
					pushFollow(FOLLOW_primitiveType_in_arrayType2025);
					primitiveType162=primitiveType();
					state._fsp--;

					stream_primitiveType.add(primitiveType162.getTree());
					char_literal163=(Token)match(input,75,FOLLOW_75_in_arrayType2027);  
					stream_75.add(char_literal163);

					char_literal164=(Token)match(input,76,FOLLOW_76_in_arrayType2029);  
					stream_76.add(char_literal164);

					// AST REWRITE
					// elements: primitiveType
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 474:26: -> ^( ArrayType primitiveType )
					{
						// /Users/lukas/Documents/GitRepos/CD1_A2/src/cd/parser/Javali.g:474:29: ^( ArrayType primitiveType )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ArrayType, "ArrayType"), root_1);
						adaptor.addChild(root_1, stream_primitiveType.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		catch (RecognitionException re) {
			reportError(re);
			throw re;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "arrayType"

	// Delegated rules



	public static final BitSet FOLLOW_classDecl_in_unit185 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_EOF_in_unit188 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_78_in_classDecl209 = new BitSet(new long[]{0x0000000020000000L});
	public static final BitSet FOLLOW_Identifier_in_classDecl213 = new BitSet(new long[]{0x0000000000000000L,0x0000000080000000L});
	public static final BitSet FOLLOW_95_in_classDecl215 = new BitSet(new long[]{0x0000000020000000L,0x00000002040A2000L});
	public static final BitSet FOLLOW_declList_in_classDecl217 = new BitSet(new long[]{0x0000000000000000L,0x0000000200000000L});
	public static final BitSet FOLLOW_97_in_classDecl220 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_78_in_classDecl242 = new BitSet(new long[]{0x0000000020000000L});
	public static final BitSet FOLLOW_Identifier_in_classDecl246 = new BitSet(new long[]{0x0000000000000000L,0x0000000000010000L});
	public static final BitSet FOLLOW_80_in_classDecl248 = new BitSet(new long[]{0x0000000020000000L});
	public static final BitSet FOLLOW_Identifier_in_classDecl252 = new BitSet(new long[]{0x0000000000000000L,0x0000000080000000L});
	public static final BitSet FOLLOW_95_in_classDecl254 = new BitSet(new long[]{0x0000000020000000L,0x00000002040A2000L});
	public static final BitSet FOLLOW_declList_in_classDecl256 = new BitSet(new long[]{0x0000000000000000L,0x0000000200000000L});
	public static final BitSet FOLLOW_97_in_classDecl259 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_decl_in_declList294 = new BitSet(new long[]{0x0000000020000002L,0x00000000040A2000L});
	public static final BitSet FOLLOW_varDecl_in_decl316 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_methodDecl_in_decl320 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_type_in_varDecl331 = new BitSet(new long[]{0x0000000020000000L});
	public static final BitSet FOLLOW_Identifier_in_varDecl333 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_varDecl335 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_type_in_varDecl355 = new BitSet(new long[]{0x0000000020000000L});
	public static final BitSet FOLLOW_Identifier_in_varDecl357 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
	public static final BitSet FOLLOW_64_in_varDecl361 = new BitSet(new long[]{0x0000000020000000L});
	public static final BitSet FOLLOW_Identifier_in_varDecl363 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000011L});
	public static final BitSet FOLLOW_68_in_varDecl368 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_methodHeading_in_methodDecl395 = new BitSet(new long[]{0x0000000000000000L,0x0000000080000000L});
	public static final BitSet FOLLOW_methodBody_in_methodDecl397 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_type_in_methodHeading421 = new BitSet(new long[]{0x0000000020000000L});
	public static final BitSet FOLLOW_Identifier_in_methodHeading425 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_60_in_methodHeading427 = new BitSet(new long[]{0x2000000020000000L,0x00000000000A2000L});
	public static final BitSet FOLLOW_formalParamList_in_methodHeading429 = new BitSet(new long[]{0x2000000000000000L});
	public static final BitSet FOLLOW_61_in_methodHeading432 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_90_in_methodHeading468 = new BitSet(new long[]{0x0000000020000000L});
	public static final BitSet FOLLOW_Identifier_in_methodHeading472 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_60_in_methodHeading474 = new BitSet(new long[]{0x2000000020000000L,0x00000000000A2000L});
	public static final BitSet FOLLOW_formalParamList_in_methodHeading476 = new BitSet(new long[]{0x2000000000000000L});
	public static final BitSet FOLLOW_61_in_methodHeading479 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_type_in_formalParamList521 = new BitSet(new long[]{0x0000000020000000L});
	public static final BitSet FOLLOW_Identifier_in_formalParamList523 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000001L});
	public static final BitSet FOLLOW_64_in_formalParamList527 = new BitSet(new long[]{0x0000000020000000L,0x00000000000A2000L});
	public static final BitSet FOLLOW_type_in_formalParamList529 = new BitSet(new long[]{0x0000000020000000L});
	public static final BitSet FOLLOW_Identifier_in_formalParamList531 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000001L});
	public static final BitSet FOLLOW_methodBodyWithDeclList_in_methodBody556 = new BitSet(new long[]{0x0000000020000000L,0x000000027B040000L});
	public static final BitSet FOLLOW_stmtList_in_methodBody559 = new BitSet(new long[]{0x0000000000000000L,0x0000000200000000L});
	public static final BitSet FOLLOW_97_in_methodBody563 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_95_in_methodBody591 = new BitSet(new long[]{0x0000000020000000L,0x000000007B040000L});
	public static final BitSet FOLLOW_stmtList_in_methodBody593 = new BitSet(new long[]{0x0000000000000000L,0x0000000200000000L});
	public static final BitSet FOLLOW_97_in_methodBody595 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_95_in_methodBody615 = new BitSet(new long[]{0x0000000000000000L,0x0000000200000000L});
	public static final BitSet FOLLOW_97_in_methodBody618 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_95_in_methodBodyWithDeclList640 = new BitSet(new long[]{0x0000000020000000L,0x00000000040A2000L});
	public static final BitSet FOLLOW_declList_in_methodBodyWithDeclList642 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_stmt_in_stmtList659 = new BitSet(new long[]{0x0000000020000002L,0x000000007B040000L});
	public static final BitSet FOLLOW_assignmentOrMethodCall_in_stmt670 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_stmt672 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ioStmt_in_stmt684 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ifStmt_in_stmt693 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_whileStmt_in_stmt702 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_returnStmt_in_stmt711 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_identAccess_in_assignmentOrMethodCall729 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000080L});
	public static final BitSet FOLLOW_assignmentTail_in_assignmentOrMethodCall740 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_71_in_assignmentTail784 = new BitSet(new long[]{0x9100000024810080L,0x0000000002F00002L});
	public static final BitSet FOLLOW_assignmentRHS_in_assignmentTail786 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expr_in_assignmentRHS802 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_newExpr_in_assignmentRHS806 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_readExpr_in_assignmentRHS810 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_readExprFloat_in_assignmentRHS814 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_60_in_methodCallTail827 = new BitSet(new long[]{0xB100000024810080L,0x0000000002200002L});
	public static final BitSet FOLLOW_actualParamList_in_methodCallTail829 = new BitSet(new long[]{0x2000000000000000L});
	public static final BitSet FOLLOW_61_in_methodCallTail832 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expr_in_actualParamList852 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000001L});
	public static final BitSet FOLLOW_64_in_actualParamList856 = new BitSet(new long[]{0x9100000024810080L,0x0000000002200002L});
	public static final BitSet FOLLOW_expr_in_actualParamList858 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000001L});
	public static final BitSet FOLLOW_92_in_ioStmt881 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_60_in_ioStmt883 = new BitSet(new long[]{0x9100000024810080L,0x0000000002200002L});
	public static final BitSet FOLLOW_expr_in_ioStmt885 = new BitSet(new long[]{0x2000000000000000L});
	public static final BitSet FOLLOW_61_in_ioStmt887 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_ioStmt889 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_93_in_ioStmt904 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_60_in_ioStmt906 = new BitSet(new long[]{0x9100000024810080L,0x0000000002200002L});
	public static final BitSet FOLLOW_expr_in_ioStmt908 = new BitSet(new long[]{0x2000000000000000L});
	public static final BitSet FOLLOW_61_in_ioStmt910 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_ioStmt912 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_94_in_ioStmt927 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_60_in_ioStmt929 = new BitSet(new long[]{0x2000000000000000L});
	public static final BitSet FOLLOW_61_in_ioStmt931 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_ioStmt933 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_82_in_ifStmt953 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_60_in_ifStmt955 = new BitSet(new long[]{0x9100000024810080L,0x0000000002200002L});
	public static final BitSet FOLLOW_expr_in_ifStmt959 = new BitSet(new long[]{0x2000000000000000L});
	public static final BitSet FOLLOW_61_in_ifStmt961 = new BitSet(new long[]{0x0000000000000000L,0x0000000080000000L});
	public static final BitSet FOLLOW_stmtBlock_in_ifStmt965 = new BitSet(new long[]{0x0000000000000002L,0x0000000000008000L});
	public static final BitSet FOLLOW_79_in_ifStmt987 = new BitSet(new long[]{0x0000000000000000L,0x0000000080000000L});
	public static final BitSet FOLLOW_stmtBlock_in_ifStmt991 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_91_in_whileStmt1021 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_60_in_whileStmt1023 = new BitSet(new long[]{0x9100000024810080L,0x0000000002200002L});
	public static final BitSet FOLLOW_expr_in_whileStmt1025 = new BitSet(new long[]{0x2000000000000000L});
	public static final BitSet FOLLOW_61_in_whileStmt1027 = new BitSet(new long[]{0x0000000000000000L,0x0000000080000000L});
	public static final BitSet FOLLOW_stmtBlock_in_whileStmt1029 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_88_in_returnStmt1051 = new BitSet(new long[]{0x9100000024810080L,0x0000000002200012L});
	public static final BitSet FOLLOW_expr_in_returnStmt1053 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_returnStmt1056 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_95_in_stmtBlock1078 = new BitSet(new long[]{0x0000000020000000L,0x000000027B040000L});
	public static final BitSet FOLLOW_stmtList_in_stmtBlock1080 = new BitSet(new long[]{0x0000000000000000L,0x0000000200000000L});
	public static final BitSet FOLLOW_97_in_stmtBlock1083 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_84_in_newExpr1105 = new BitSet(new long[]{0x0000000020000000L});
	public static final BitSet FOLLOW_Identifier_in_newExpr1107 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_60_in_newExpr1109 = new BitSet(new long[]{0x2000000000000000L});
	public static final BitSet FOLLOW_61_in_newExpr1111 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_84_in_newExpr1124 = new BitSet(new long[]{0x0000000020000000L});
	public static final BitSet FOLLOW_Identifier_in_newExpr1126 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000800L});
	public static final BitSet FOLLOW_75_in_newExpr1128 = new BitSet(new long[]{0x9100000024810080L,0x0000000002200002L});
	public static final BitSet FOLLOW_simpleExpr_in_newExpr1130 = new BitSet(new long[]{0x0000000000000000L,0x0000000000001000L});
	public static final BitSet FOLLOW_76_in_newExpr1132 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_84_in_newExpr1147 = new BitSet(new long[]{0x0000000000000000L,0x00000000000A2000L});
	public static final BitSet FOLLOW_primitiveType_in_newExpr1149 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000800L});
	public static final BitSet FOLLOW_75_in_newExpr1151 = new BitSet(new long[]{0x9100000024810080L,0x0000000002200002L});
	public static final BitSet FOLLOW_simpleExpr_in_newExpr1153 = new BitSet(new long[]{0x0000000000000000L,0x0000000000001000L});
	public static final BitSet FOLLOW_76_in_newExpr1155 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_86_in_readExpr1176 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_60_in_readExpr1178 = new BitSet(new long[]{0x2000000000000000L});
	public static final BitSet FOLLOW_61_in_readExpr1180 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_87_in_readExprFloat1200 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_60_in_readExprFloat1202 = new BitSet(new long[]{0x2000000000000000L});
	public static final BitSet FOLLOW_61_in_readExprFloat1204 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_simpleExpr_in_expr1226 = new BitSet(new long[]{0x0200000000000002L,0x0000000000000760L});
	public static final BitSet FOLLOW_compOp_in_expr1246 = new BitSet(new long[]{0x9100000024810080L,0x0000000002200002L});
	public static final BitSet FOLLOW_simpleExpr_in_expr1250 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_term_in_simpleExpr1323 = new BitSet(new long[]{0x8000000000000002L,0x0000000100000002L});
	public static final BitSet FOLLOW_weakOp_in_simpleExpr1341 = new BitSet(new long[]{0x9100000024810080L,0x0000000002200002L});
	public static final BitSet FOLLOW_term_in_simpleExpr1345 = new BitSet(new long[]{0x8000000000000002L,0x0000000100000002L});
	public static final BitSet FOLLOW_factor_in_term1405 = new BitSet(new long[]{0x4C00000000000002L,0x0000000000000008L});
	public static final BitSet FOLLOW_strongOp_in_term1423 = new BitSet(new long[]{0x9100000024810080L,0x0000000002200002L});
	public static final BitSet FOLLOW_factor_in_term1427 = new BitSet(new long[]{0x4C00000000000002L,0x0000000000000008L});
	public static final BitSet FOLLOW_63_in_factor1485 = new BitSet(new long[]{0x1100000024810080L,0x0000000002200000L});
	public static final BitSet FOLLOW_noSignFactor_in_factor1487 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_65_in_factor1501 = new BitSet(new long[]{0x1100000024810080L,0x0000000002200000L});
	public static final BitSet FOLLOW_noSignFactor_in_factor1503 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_noSignFactor_in_factor1517 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_56_in_noSignFactor1535 = new BitSet(new long[]{0x9100000024810080L,0x0000000002200002L});
	public static final BitSet FOLLOW_factor_in_noSignFactor1537 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DecimalNumber_in_noSignFactor1552 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_HexNumber_in_noSignFactor1565 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FloatNumber_in_noSignFactor1578 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_BooleanLiteral_in_noSignFactor1591 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_85_in_noSignFactor1604 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_identAccess_in_noSignFactor1618 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_60_in_noSignFactor1634 = new BitSet(new long[]{0x9100000024810080L,0x0000000002200002L});
	public static final BitSet FOLLOW_expr_in_noSignFactor1636 = new BitSet(new long[]{0x2000000000000000L});
	public static final BitSet FOLLOW_61_in_noSignFactor1638 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_60_in_noSignFactor1651 = new BitSet(new long[]{0x0000000020000000L,0x00000000000A2000L});
	public static final BitSet FOLLOW_referenceType_in_noSignFactor1653 = new BitSet(new long[]{0x2000000000000000L});
	public static final BitSet FOLLOW_61_in_noSignFactor1655 = new BitSet(new long[]{0x1100000024810080L,0x0000000002200000L});
	public static final BitSet FOLLOW_noSignFactor_in_noSignFactor1657 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_Identifier_in_identAccess1682 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000804L});
	public static final BitSet FOLLOW_Identifier_in_identAccess1696 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_methodCallTail_in_identAccess1698 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000804L});
	public static final BitSet FOLLOW_89_in_identAccess1713 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000804L});
	public static final BitSet FOLLOW_selectorSeq_in_identAccess1735 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_fieldSelector_in_selectorSeq1779 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000804L});
	public static final BitSet FOLLOW_elemSelector_in_selectorSeq1793 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000804L});
	public static final BitSet FOLLOW_fieldSelector_in_selectorSeq1817 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000804L});
	public static final BitSet FOLLOW_elemSelector_in_selectorSeq1842 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000804L});
	public static final BitSet FOLLOW_66_in_fieldSelector1878 = new BitSet(new long[]{0x0000000020000000L});
	public static final BitSet FOLLOW_Identifier_in_fieldSelector1880 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_methodCallTail_in_fieldSelector1882 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_66_in_fieldSelector1899 = new BitSet(new long[]{0x0000000020000000L});
	public static final BitSet FOLLOW_Identifier_in_fieldSelector1901 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_75_in_elemSelector1925 = new BitSet(new long[]{0x9100000024810080L,0x0000000002200002L});
	public static final BitSet FOLLOW_simpleExpr_in_elemSelector1927 = new BitSet(new long[]{0x0000000000000000L,0x0000000000001000L});
	public static final BitSet FOLLOW_76_in_elemSelector1929 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_primitiveType_in_type1952 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_referenceType_in_type1958 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_Identifier_in_referenceType1970 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_arrayType_in_referenceType1975 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_Identifier_in_arrayType2008 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000800L});
	public static final BitSet FOLLOW_75_in_arrayType2010 = new BitSet(new long[]{0x0000000000000000L,0x0000000000001000L});
	public static final BitSet FOLLOW_76_in_arrayType2012 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_primitiveType_in_arrayType2025 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000800L});
	public static final BitSet FOLLOW_75_in_arrayType2027 = new BitSet(new long[]{0x0000000000000000L,0x0000000000001000L});
	public static final BitSet FOLLOW_76_in_arrayType2029 = new BitSet(new long[]{0x0000000000000002L});
}
