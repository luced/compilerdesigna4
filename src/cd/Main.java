package cd;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.CommonTreeNodeStream;

import cd.codegen.AstCodeGenerator;
import cd.debug.AstDump;
import cd.exceptions.ParseFailure;
import cd.ir.Ast.ClassDecl;
import cd.parser.JavaliLexer;
import cd.parser.JavaliParser;
import cd.parser.JavaliWalker;
import cd.semantic.SemanticAnalyzer;

/** 
 * The main entrypoint for the compiler.  Consists of a series
 * of routines which must be invoked in order.  The main()
 * routine here invokes these routines, as does the unit testing
 * code. This is not the <b>best</b> programming practice, as the
 * series of calls to be invoked is duplicated in two places in the
 * code, but it will do for now. */
public class Main {
	
	// Set to non-null to write debug info out
	public Writer debug = null;
	
	public void debug(String format, Object... args) {
		if (debug != null) {
			String result = String.format(format, args);
			try {
				debug.write(result);
				debug.write('\n');
				debug.flush();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	/** Parse command line, invoke compile() routine */
	public static void main(String args[]) throws IOException {
		
		Main m = new Main();
		
		for (String file : args) {
			
			if (file.equals("-d"))
				m.debug = new OutputStreamWriter(System.err);
			else {
				FileInputStream fin = new FileInputStream(file);

				// Parse:
				List<ClassDecl> astRoots = m.parse(file, fin, false);
				
				// Run the semantic check:
				m.semanticCheck(astRoots);
				
				// Generate code:
				String sFile = file + Config.ASMEXT;
				FileWriter fout = new FileWriter(sFile);
				m.generateCode(astRoots, fout);
				fout.close();
			}
		}
	}
	
	public Main() {
	}

	public List<ClassDecl> parse(InputStream file, boolean debugParser)  throws IOException {
		return parse(null, file, debugParser);
	}
	
	/** Parses an input stream into an AST 
	 * @throws IOException */
	public List<ClassDecl> parse(String fileName, InputStream file, boolean debugParser)  throws IOException {
		List<ClassDecl> result = new ArrayList<ClassDecl>();
		//throw new ToDoException("Required for 2bc, copy from examples/part2/tree/...");
		try { 
			ANTLRInputStream input = new ANTLRInputStream(file);
			   
			JavaliLexer lexer = new JavaliLexer(input);
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			            
			JavaliParser parser= new JavaliParser(tokens);
			JavaliParser.unit_return r;

			r = parser.unit();
			CommonTree t = (CommonTree) r.getTree();
			CommonTreeNodeStream nodes = new CommonTreeNodeStream(t);
 
			JavaliWalker walker = new JavaliWalker(nodes);

			result = walker.unit();
		} catch (RecognitionException e) {
				ParseFailure pf = new ParseFailure(e.charPositionInLine, e.getLocalizedMessage());
				pf.initCause(e);
				throw pf;
		} catch (RuntimeException re) {
				throw re;
		}

		return result;
	}
	
	public void semanticCheck(List<ClassDecl> astRoots) {
		{
			SemanticAnalyzer semAn = new SemanticAnalyzer(this);
			semAn.check(astRoots);
		}
		
		// Before generating code, optimize!

	}
	
	public void generateCode(List<ClassDecl> astRoots, Writer out) {
		{
			AstCodeGenerator cg = new AstCodeGenerator(this, out);
			cg.go(astRoots);
		}
	}

	/** Dumps the AST to the debug stream */
	private void dumpAst(List<ClassDecl> astRoots) throws IOException {
		if (this.debug == null) return;
		this.debug.write(AstDump.toString(astRoots));
	}
}
